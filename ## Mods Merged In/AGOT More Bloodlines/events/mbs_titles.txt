namespace = mbs_titles

################################
character_event = {
	id = mbs_titles.1
	hide_window = yes
	

	is_triggered_only = yes

	trigger = {
		has_landed_title = c_mbs_maris_motherhouse
		NOT = {
			is_female = yes
		}
	}


	immediate = {
		create_character = {
			female = yes
			dynasty = none
			random_traits = yes
			trait = zealous
			trait = septa
			trait = kind
			religion = the_seven
			culture = valeman
		}
		new_character = {
			b_mbs_maris_motherhouse = { gain_title = PREV }
			c_mbs_maris_motherhouse = { gain_title = PREV }
		}
	}
	
	option = {
		name = OK
	}
}
character_event = {
	id = mbs_titles.2
	hide_window = yes
	

	is_triggered_only = yes

	trigger = {
		has_landed_title = b_mbs_oldtown_motherhouse
		NOT = {
			is_female = yes
		}
	}


	immediate = {
		create_character = {
			female = yes
			dynasty = none
			random_traits = yes
			trait = zealous
			trait = septa
			trait = kind
			religion = the_seven
			culture = reachman
		}
		new_character = {
			b_mbs_oldtown_motherhouse = { gain_title = PREV }
		}
	}
	
	option = {
		name = OK
	}
}
character_event = {
	id = mbs_titles.3
	hide_window = yes
	

	is_triggered_only = yes

	trigger = {
		has_landed_title = c_mbs_maidenpool_motherhouse
		NOT = {
			is_female = yes
		}
	}


	immediate = {
		create_character = {
			female = yes
			dynasty = none
			random_traits = yes
			trait = zealous
			trait = septa
			trait = kind
			religion = the_seven
			culture = riverlander
		}
		new_character = {
			c_mbs_maidenpool_motherhouse = { gain_title = PREV }
			b_mbs_maidenpool_motherhouse = { gain_title = PREV }
		}
	}
	
	option = {
		name = OK
	}
}
character_event = {
	id = mbs_titles.4
	hide_window = yes
	

	is_triggered_only = yes

	trigger = {
		has_landed_title = c_mbs_lannisport_motherhouse
		NOT = {
			is_female = yes
		}
	}


	immediate = {
		create_character = {
			female = yes
			dynasty = none
			random_traits = yes
			trait = zealous
			trait = septa
			trait = kind
			religion = the_seven
			culture = westerman
		}
		new_character = {
			c_mbs_lannisport_motherhouse = { gain_title = PREV }
			b_mbs_lannisport_motherhouse = { gain_title = PREV }
		}
	}
	
	option = {
		name = OK
	}
}
character_event = {
	id = mbs_titles.5
	hide_window = yes
	

	is_triggered_only = yes

	trigger = {
		is_theocracy = yes
		OR = {
			has_landed_title = c_mbs_holy_house
			has_landed_title = c_mbs_highgarden_sept
			has_landed_title = c_mbs_gulltown_sept
			has_landed_title = c_mbs_lannisport_sept
			has_landed_title = c_mbs_lords_sept
			has_landed_title = c_mbs_starry_sept
			has_landed_title = c_mbs_quiet_isle
		}
		OR = {
			primary_title = { title = c_mbs_holy_house }
			primary_title = { title = c_mbs_highgarden_sept }
			primary_title = { title = c_mbs_gulltown_sept }
			primary_title = { title = c_mbs_lannisport_sept }
			primary_title = { title = c_mbs_lords_sept }
			primary_title = { title = c_mbs_starry_sept }
			primary_title = { title = c_mbs_quiet_isle }
		}
		NOT = { primary_title = { title = k_the_most_devout } }
		religion = the_seven
		NOT = {
			is_female = yes
		}
	}


	immediate = {
		add_trait = septon
		set_special_character_title = title_septon		
	}
	
	option = {
		name = OK
	}
}