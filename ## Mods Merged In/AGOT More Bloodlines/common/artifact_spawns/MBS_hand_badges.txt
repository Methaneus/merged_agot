hand_ofthe_king_pin_ned = {
    max_amount = 1
    spawn_date = 8297.1.1
	
	spawn_chance = {
		value = 100
		modifier = {
			factor = 0
			NOT = {
				year = 8297
			}
		}
	}
    
    weight = {
		value = 0
		additive_modifier = {
			value = 100
			has_landed_title = d_winterfell
		}
	}
    artifacts = {
        hand_ofthe_king_pin_ned = {
            value = 1
        }
    }
}
hand_ofthe_king_pin_jon_arryn = {
    max_amount = 1
    spawn_date = 8284.1.1
	
	spawn_chance = {
		value = 100
		modifier = {
			factor = 0
			NOT = {
				year = 8284
			}
		}
	}
    
    weight = {
		value = 0
		additive_modifier = {
			value = 100
			has_landed_title = k_vale
		}
	}
    artifacts = {
        hand_ofthe_king_pin_jon = {
            value = 1
        }
    }
}
hand_ofthe_king_pin_tywin = {
    max_amount = 1
    spawn_date = 8262.1.1
	
	spawn_chance = {
		value = 100
		modifier = {
			factor = 0
			NOT = {
				year = 8267
			}
		}
	}
    
    weight = {
		value = 0
		additive_modifier = {
			value = 100
			has_landed_title = k_westerlands
		}
	}
    artifacts = {
        hand_ofthe_king_pin_tywin = {
            value = 1
        }
    }
}
hand_ofthe_king_pin_chelsted = {
    max_amount = 1
    spawn_date = 8283.1.1
	
	spawn_chance = {
		value = 100
		modifier = {
			factor = 0
			NOT = {
				year = 8283
			}
		}
	}
    
    weight = {
		value = 0
		additive_modifier = {
			value = 100
			has_landed_title = c_bramsfort
		}
	}
    artifacts = {
        hand_ofthe_king_pin_qarlton = {
            value = 1
        }
    }
}
hand_ofthe_king_pin_orys = {
    max_amount = 1
    spawn_date = 7999.1.1
	
	spawn_chance = {
		value = 100
		modifier = {
			factor = 0
			NOT = {
				year = 7999
			}
		}
	}
    
    weight = {
		value = 0
		additive_modifier = {
			value = 100
			has_landed_title = k_stormlands
		}
	}
    artifacts = {
        hand_ofthe_king_pin_orys = {
            value = 1
        }
    }
}
hand_ofthe_king_pin_edmyn = {
    max_amount = 1
    spawn_date = 8007.1.1
	
	spawn_chance = {
		value = 100
		modifier = {
			factor = 0
			NOT = {
				year = 8007
			}
		}
	}
    
    weight = {
		value = 0
		additive_modifier = {
			value = 100
			has_landed_title = k_riverlands
		}
	}
    artifacts = {
        hand_ofthe_king_pin_edmyn = {
            value = 1
        }
    }
}
hand_ofthe_king_pin_rogar = {
    max_amount = 1
    spawn_date = 8048.3.7
	
	spawn_chance = {
		value = 100
		modifier = {
			factor = 0
			NOT = {
				year = 8048
			}
		}
	}
    
    weight = {
		value = 0
		additive_modifier = {
			value = 100
			has_landed_title = k_stormlands
		}
	}
    artifacts = {
        hand_ofthe_king_pin_rogar = {
            value = 1
        }
    }
}
hand_ofthe_king_pin_cregan = {
    max_amount = 1
    spawn_date = 8131.1.8
	
	spawn_chance = {
		value = 100
		modifier = {
			factor = 0
			NOT = {
				year = 8131
			}
		}
	}
    
    weight = {
		value = 0
		additive_modifier = {
			value = 100
			has_landed_title = d_winterfell
		}
	}
    artifacts = {
        hand_ofthe_king_pin_cregan = {
            value = 1
        }
    }
}
hand_ofthe_king_pin_viserys = {
    max_amount = 1
    spawn_date = 8140.1.1
	
	spawn_chance = {
		value = 100
		modifier = {
			factor = 0
			NOT = {
				year = 8172
			}
		}
	}
    
    weight = {
		value = 0
		additive_modifier = {
			value = 100
			has_landed_title = e_iron_throne
		}
	}
    artifacts = {
        hand_ofthe_king_pin_viserys = {
            value = 1
        }
    }
}
hand_ofthe_king_pin_bloodraven = {
    max_amount = 1
    spawn_date = 8209.6.1
	
	spawn_chance = {
		value = 100
		modifier = {
			factor = 0
			NOT = {
				year = 8252
			}
		}
	}
    
    weight = {
		value = 0
		additive_modifier = {
			value = 100
			has_landed_title = e_iron_throne
		}
	}
    artifacts = {
        hand_ofthe_king_pin_bloodraven = {
            value = 1
        }
    }
}
hand_ofthe_king_pin_ormund = {
    max_amount = 1
    spawn_date = 8259.11.1
	
	spawn_chance = {
		value = 100
		modifier = {
			factor = 0
			NOT = {
				year = 8259
			}
		}
	}
    
    weight = {
		value = 0
		additive_modifier = {
			value = 100
			has_landed_title = k_stormlands
		}
	}
    artifacts = {
        hand_ofthe_king_pin_ormund = {
            value = 1
        }
    }
}
