

# County Title
title = c_oldtown

# Settlements
max_settlements = 7
b_the_hightower = castle
b_oldtown = castle
b_mbs_oldtown_new = city
b_the_citadel = city
b_oldtown_seven_shrines = temple
#b_sailors_sept = temple
b_oakmere = castle
b_hapsford = castle

# Misc
culture = old_first_man
religion = old_gods

# History
1.1.1 = {
	b_the_hightower = ca_asoiaf_reach_basevalue_1
	b_the_hightower = ca_asoiaf_reach_basevalue_2
	b_the_hightower = ca_asoiaf_reach_basevalue_3
	b_the_hightower = ca_asoiaf_reach_basevalue_4
	b_the_hightower = ca_asoiaf_reach_basevalue_5
	#b_the_hightower = ca_cellar_1
	
	b_mbs_oldtown_new = ct_asoiaf_reach_basevalue_1
	b_mbs_oldtown_new = ct_asoiaf_reach_basevalue_2
	b_mbs_oldtown_new = ct_asoiaf_oldtownshipyard

	b_oldtown_seven_shrines = tp_monastery_1
}
1.1.1 = {
	b_oldtown = ca_asoiaf_reach_basevalue_1
	b_oldtown = ca_asoiaf_reach_basevalue_2
	b_oldtown = ca_asoiaf_reach_basevalue_3
	b_oldtown = ca_asoiaf_reach_basevalue_4
	b_oldtown = ca_asoiaf_reach_basevalue_5
	b_oldtown = ca_asoiaf_oldtownshipyard
}
1.1.1 = {
	b_the_citadel = ct_asoiaf_reach_basevalue_1
	b_the_citadel = ct_asoiaf_reach_basevalue_2
	b_the_citadel = ct_asoiaf_oldtownshipyard
}
1.1.1 = {
	b_oakmere = ca_asoiaf_reach_basevalue_1
	b_oakmere = ca_asoiaf_reach_basevalue_2
	b_oakmere = ca_asoiaf_reach_basevalue_3
	b_oakmere = ca_asoiaf_reach_basevalue_4
	b_oakmere = ca_asoiaf_oldtownshipyard
}
1.1.1 = {
	b_hapsford = ca_asoiaf_reach_basevalue_1
	b_hapsford = ca_asoiaf_reach_basevalue_2
	b_hapsford = ca_asoiaf_reach_basevalue_3
	b_hapsford = ca_asoiaf_reach_basevalue_4
	b_hapsford = ca_asoiaf_oldtownshipyard
}
6700.1.1 = {
	culture = reachman
	religion = the_seven
}
8070.1.1 = { #Jaehaerys constructs road network
	#b_the_hightower = ca_roseroad
}

