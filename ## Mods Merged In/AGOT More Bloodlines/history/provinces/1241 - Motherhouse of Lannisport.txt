

# County Title
title = c_mbs_lannisport_motherhouse

# Settlements
max_settlements = 2
b_mbs_lannisport_motherhouse = temple
b_mbs_lannisport_motherhouse2 = city

# Misc
culture = old_first_man
religion = old_gods

# History
1.1.1 = {
	
	b_mbs_lannisport_motherhouse = tp_monastery_1

}
6700.1.1 = {
	culture = westerman
	religion = the_seven
}