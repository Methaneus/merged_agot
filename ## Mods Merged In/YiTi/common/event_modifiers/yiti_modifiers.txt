yiti_road1 = {
	local_movement_speed = 0.1
	supply_limit = 2
	local_tax_modifier = 0.01
	icon = 24
}
yiti_road2 = {
	local_movement_speed = 0.2
	supply_limit = 5
	local_tax_modifier = 0.03
	icon = 29
}
yiti_road3 = {
	local_movement_speed = 0.4
	supply_limit = 10
	local_tax_modifier = 0.075
	icon = 190
}