# 1213  - Jumen

# County Title
title = c_jumen

# Settlements
max_settlements = 4

b_jumen_castle = castle
b_jumen_city1 = city
b_jumen_temple = temple
b_jumen_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_jumen_castle = ca_asoiaf_yiti_basevalue_1
	b_jumen_castle = ca_asoiaf_yiti_basevalue_2

	b_jumen_city1 = ct_asoiaf_yiti_basevalue_1
	b_jumen_city1 = ct_asoiaf_yiti_basevalue_2

}
	