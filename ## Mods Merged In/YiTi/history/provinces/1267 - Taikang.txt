# 1170  - Taikang

# County Title
title = c_taikang

# Settlements
max_settlements = 4

b_taikang_castle = castle
b_taikang_city1 = city
b_taikang_temple = temple
b_taikang_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_taikang_castle = ca_asoiaf_yiti_basevalue_1
	b_taikang_castle = ca_asoiaf_yiti_basevalue_2

	b_taikang_city1 = ct_asoiaf_yiti_basevalue_1

}
	