# 783  - Nanqi

# County Title
title = c_nanqi

# Settlements
max_settlements = 6

b_nanqi_castle1 = castle
b_nanqi_city1 = city
b_nanqi_temple = temple
b_nanqi_city2 = city
b_nanqi_castle2 = city
b_nanqi_castle3 = castle

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_nanqi_castle1 = ca_asoiaf_yiti_basevalue_1
	b_nanqi_castle1 = ca_asoiaf_yiti_basevalue_2
	b_nanqi_castle1 = ca_asoiaf_yiti_basevalue_3

	b_nanqi_city1 = ct_asoiaf_yiti_basevalue_1
	b_nanqi_city1 = ct_asoiaf_yiti_basevalue_2
	b_nanqi_city1 = ct_asoiaf_yiti_basevalue_3

	b_nanqi_city2 = ct_asoiaf_yiti_basevalue_1
	b_nanqi_city2 = ct_asoiaf_yiti_basevalue_2
	b_nanqi_city2 = ct_asoiaf_yiti_basevalue_3

}
	