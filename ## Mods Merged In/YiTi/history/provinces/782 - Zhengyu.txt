# 782  - Zhengyu

# County Title
title = c_zhengyu

# Settlements
max_settlements = 6

b_zhengyu_castle1 = castle
b_zhengyu_city1 = city
b_zhengyu_temple = temple
b_zhengyu_city2 = city
b_zhengyu_castle2 = city
b_zhengyu_castle3 = castle

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_zhengyu_castle1 = ca_asoiaf_yiti_basevalue_1
	b_zhengyu_castle1 = ca_asoiaf_yiti_basevalue_2
	b_zhengyu_castle1 = ca_asoiaf_yiti_basevalue_3

	b_zhengyu_city1 = ct_asoiaf_yiti_basevalue_1
	b_zhengyu_city1 = ct_asoiaf_yiti_basevalue_2
	b_zhengyu_city1 = ct_asoiaf_yiti_basevalue_3

	b_zhengyu_city2 = ct_asoiaf_yiti_basevalue_1
	b_zhengyu_city2 = ct_asoiaf_yiti_basevalue_2
	b_zhengyu_city2 = ct_asoiaf_yiti_basevalue_3

}
	