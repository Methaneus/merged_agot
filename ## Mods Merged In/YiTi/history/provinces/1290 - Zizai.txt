# 1193  - Zizai

# County Title
title = c_zizai

# Settlements
max_settlements = 4

b_zizai_castle = castle
b_zizai_city1 = city
b_zizai_temple = temple
b_zizai_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_zizai_castle = ca_asoiaf_yiti_basevalue_1
	b_zizai_castle = ca_asoiaf_yiti_basevalue_2

	b_zizai_city1 = ct_asoiaf_yiti_basevalue_1
	b_zizai_city1 = ct_asoiaf_yiti_basevalue_2

}
	