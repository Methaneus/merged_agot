# 1225  - Puwei

# County Title
title = c_puwei

# Settlements
max_settlements = 4

b_puwei_castle = castle
b_puwei_city1 = city
b_puwei_temple = temple
b_puwei_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_puwei_castle = ca_asoiaf_yiti_basevalue_1
	b_puwei_castle = ca_asoiaf_yiti_basevalue_2

	b_puwei_city1 = ct_asoiaf_yiti_basevalue_1
	b_puwei_city1 = ct_asoiaf_yiti_basevalue_2

}
	