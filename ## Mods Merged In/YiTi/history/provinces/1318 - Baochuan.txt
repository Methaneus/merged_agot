# 1221  - Baochuan

# County Title
title = c_baochuan

# Settlements
max_settlements = 4

b_baochuan_castle = castle
b_baochuan_city1 = city
b_baochuan_temple = temple
b_baochuan_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_baochuan_castle = ca_asoiaf_yiti_basevalue_1
	b_baochuan_castle = ca_asoiaf_yiti_basevalue_2

	b_baochuan_city1 = ct_asoiaf_yiti_basevalue_1
	b_baochuan_city1 = ct_asoiaf_yiti_basevalue_2

}
	