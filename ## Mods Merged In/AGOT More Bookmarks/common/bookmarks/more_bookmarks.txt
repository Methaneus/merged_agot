bm_CROWNED_DRAGON = {
	name = "BM_CROWNED_DRAGON"
	desc = "BM_CROWNED_DRAGON_DESC"
	date = 8001.1.1
	
	selectable_character = { id = 77000 }	# Aegon Targaryen
	selectable_character = { id = 1317 }	# Orys Baratheon
	selectable_character = { id = 16253 }	# Harlan Tyrell
	selectable_character = { id = 85190 }	# Loren Lannister
	selectable_character = { id = 17159 }	# Edmyn Tully
	selectable_character = { id = 26107 }	# Vickon Greyjoy
	selectable_character = { id = 210178 }	# Ronnel Arryn
	selectable_character = { id = 20059 }	# Torrhen Stark
	selectable_character = { id = 22001 }	# Mariya Martell
}
bm_TARGARYEN_RULE = {
	name = "BM_TARGARYEN_RULE"
	desc = "BM_TARGARYEN_RULE_DESC"
	date = 8011.1.1
	
	selectable_character = { id = 77000 }	# Aegon Targaryen
	selectable_character = { id = 1317 }	# Orys Baratheon
	selectable_character = { id = 15253 }	# Theo Tyrell
	selectable_character = { id = 85190 }	# Loren Lannister
	selectable_character = { id = 17159 }	# Edmyn Tully
	selectable_character = { id = 26107 }	# Vickon Greyjoy
	selectable_character = { id = 210178 }	# Ronnel Arryn
	selectable_character = { id = 20059 }	# Torrhen Stark
	selectable_character = { id = 22001 }	# Mariya Martell
}
bm_DEATH_OF_THE_CONQUEROR = {
	name = "BM_DEATH_OF_THE_CONQUEROR"
	desc = "BM_DEATH_OF_THE_CONQUEROR_DESC"
	date = 8038.1.1
	
	selectable_character = { id = 77004 }	# Aenys Targaryen
	selectable_character = { id = 1317 }	# Orys Baratheon
	selectable_character = { id = 15253 }	# Theo Tyrell
	selectable_character = { id = 86190 }	# Lyman Lannister
	selectable_character = { id = 16159 }	# Melwys Tully
	selectable_character = { id = 25107 }	# Goren Greyjoy
	selectable_character = { id = 210178 }	# Ronnel Arryn
	selectable_character = { id = 20159 }	# Brandon Stark
	selectable_character = { id = 19001 }	# Deria Martell
}
bm_RISE_OF_THREE_DAUGHTERS = {
	name = "BM_RISE_OF_THREE_DAUGHTERS"
	desc = "BM_RISE_OF_THREE_DAUGHTERS_DESC"
	date = 8096.1.1
	
	selectable_character = { id = 1003300700 }	# Myr
	selectable_character = { id = 1003300543 }	# Tyrosh
	selectable_character = { id = 1007300540 }	# Lys
	selectable_character = { id = 115174371 }	# Volantis
	selectable_character = { id = 110174360 }	# Pentos
	selectable_character = { id = 77006 }		# Jaehaerys Targaryen
	selectable_character = { id = 17001 }		# Mors Martell
}
bm_FALL_OF_THREE_DAUGHTERS = {
	name = "BM_FALL_OF_THREE_DAUGHTERS"
	desc = "BM_FALL_OF_THREE_DAUGHTERS_DESC"
	date = 8130.1.1
	
	selectable_character = { id = 77013 }		# Aegon III Targaryen
	selectable_character = { id = 14001 }		# Aliandra Martell
	selectable_character = { id = 113300544 }	# Three Daughters
	selectable_character = { id = 108300533 }	# Tyrosh
	selectable_character = { id = 106174355 }	# Myr
	selectable_character = { id = 108300539 }	# Lys
	selectable_character = { id = 115174373 }	# Volantis
	selectable_character = { id = 119300526 }	# Pentos
	selectable_character = { id = 600512 }		# Braavos
	selectable_character = { id = 112300548 }	# Lorath
}
bm_HOLY_KING = {
	name = "BM_HOLY_KING"
	desc = "BM_HOLY_KING_DESC"
	date = 8161.1.1
	
	selectable_character = { id = 77017 }	# Baelor Targaryen
	selectable_character = { id = 15317 }	# Corwen Baratheon
	selectable_character = { id = 13001 }	# Marence Martell
	selectable_character = { id = 88067 }	# Leo Tyrell
	selectable_character = { id = 89190 }	# Loreon Lannister
	selectable_character = { id = 13159 }	# Kermit Tully
	selectable_character = { id = 204178 }	# Jonothor Arryn
	selectable_character = { id = 36107 }	# Veron Greyjoy
	selectable_character = { id = 20859 }	# Cregan Stark
}
bm_THIRD_BLACKFYRE_REBELLION = {
	name = "BM_THIRD_BLACKFYRE_REBELLION"
	desc = "BM_THIRD_BLACKFYRE_REBELLION_DESC"
	date = 8219.1.1
	
	selectable_character = { id = 77030 }	# Aerys Targaryen
	selectable_character = { id = 10357 }	# Aerys Blackfyre
	selectable_character = { id = 17317 }	# Axel Baratheon
	selectable_character = { id = 11001 }	# Maron Martell
	selectable_character = { id = 88000 }	# Denys Tyrell
	selectable_character = { id = 102190 }	# Gerold Lannister
	selectable_character = { id = 10159 }	# Hosteen Tully
	selectable_character = { id = 200178 }	# Othor Arryn
	selectable_character = { id = 299107 }	# Loron Greyjoy
	selectable_character = { id = 20959 }	# Willam Stark
}
bm_BM_RAYMUN_REDBEARD_INVASION = {
	name = "BM_RAYMUN_REDBEARD_INVASION"
	desc = "BM_RAYMUN_REDBEARD_INVASION_DESC"
	date = 8226.1.1
	
	selectable_character = { id = 77031 }	# Maekar Targaryen
	selectable_character = { id = 21359 }	# Raymun Redbeard
	selectable_character = { id = 20317 }	# Lyonel Baratheon
	selectable_character = { id = 99002 }	# Olyvar Martell
	selectable_character = { id = 88000 }	# Denys Tyrell
	selectable_character = { id = 102190 }	# Gerold Lannister
	selectable_character = { id = 10159 }	# Hosteen Tully
	selectable_character = { id = 94001 }	# Jasper Arryn
	selectable_character = { id = 17107 }	# Torwyn Greyjoy
	selectable_character = { id = 20959 }	# Willam Stark
}
bm_FOURTH_BLACKFYRE_REBELLION = {
	name = "BM_FOURTH_BLACKFYRE_REBELLION"
	desc = "BM_FOURTH_BLACKFYRE_REBELLION_DESC"
	date = 8236.1.1
	
	selectable_character = { id = 77034 }	# Aegon V Targaryen
	selectable_character = { id = 60000057 }# Daemon Blackfyre
	selectable_character = { id = 20317 }	# Lyonel Baratheon
	selectable_character = { id = 2001 }	# Obera Martell
	selectable_character = { id = 88000 }	# Denys Tyrell
	selectable_character = { id = 102190 }	# Gerold Lannister
	selectable_character = { id = 10159 }	# Hosteen Tully
	selectable_character = { id = 94001 }	# Jasper Arryn
	selectable_character = { id = 17107 }	# Torwyn Greyjoy
	selectable_character = { id = 21169 }	# Edwyle Stark
}
bm_LAUGHING_STORM_REBELLION = {
	name = "BM_LAUGHING_STORM_REBELLION"
	desc = "BM_LAUGHING_STORM_REBELLION_DESC"
	date = 8239.1.1
	
	selectable_character = { id = 20317 }	# Lyonel "The Laughing Storm" Baratheon
	selectable_character = { id = 77034 }	# Aegon V Targaryen
	selectable_character = { id = 2001 }	# Obera Martell
	selectable_character = { id = 102190 }	# Gerold Lannister
	selectable_character = { id = 10159 }	# Hosteen Tully
	selectable_character = { id = 94001 }	# Jasper Arryn
	selectable_character = { id = 17107 }	# Torwyn Greyjoy
	selectable_character = { id = 21169 }	# Edwyle Stark
}
bm_DEFIANCE_OF_DUSKENDALE = {
	name = "BM_DEFIANCE_OF_DUSKENDALE"
	desc = "BM_DEFIANCE_OF_DUSKENDALE_DESC"
	date = 8276.1.1
	
	selectable_character = { id = 77037 }	# Aerys II Targaryen
	selectable_character = { id = 6049364 } # Denys Darklyn
	selectable_character = { id = 3317 }	# Steffon Baratheon
	selectable_character = { id = 1 }	 	# Doran Martell
	selectable_character = { id = 1253 }	# Mace Tyrell
	selectable_character = { id = 190 }		# Tywin Lannister
	selectable_character = { id = 159 }		# Hoster Tully
	selectable_character = { id = 94002 }	# Jon Arryn
	selectable_character = { id = 107 }		# Quellon Greyjoy
	selectable_character = { id = 2058 }	# Rickard Stark
}
bm_YEAR_OF_THE_FALSE_SPRING = {
	name = "BM_YEAR_OF_THE_FALSE_SPRING"
	desc = "BM_YEAR_OF_THE_FALSE_SPRING_DESC"
	date = 8280.1.1
	
	selectable_character = { id = 77037 } 	# Aerys II Targaryen
	selectable_character = { id = 77039 }	# Rhaegar Targaryen
	selectable_character = { id = 317 }		# Robert Baratheon
	selectable_character = { id = 1 }	 	# Doran Martell
	selectable_character = { id = 1253 }	# Mace Tyrell
	selectable_character = { id = 190 }		# Tywin Lannister
	selectable_character = { id = 159 }		# Hoster Tully
	selectable_character = { id = 94002 }	# Jon Arryn
	selectable_character = { id = 107 }		# Quellon Greyjoy
	selectable_character = { id = 2058 }	# Rickard Stark
	selectable_character = { id = 5056 }	# Walter Whent
}
bm_PARLEY_AT_STORMS_END = {
	name = "BM_PARLEY_AT_STORMS_END"
	desc = "BM_PARLEY_AT_STORMS_END_DESC"
	date = 8299.2.11

	selectable_character = { id = 5317 }	# Renly Baratheon
	selectable_character = { id = 4317 }	# Stannis Baratheon
	selectable_character = { id = 77041 }	# Daenerys Targaryen
	selectable_character = { id = 2059 }	# Robb Stark
	selectable_character = { id = 9316 }	# Joffrey Lannister
	selectable_character = { id = 1107 } 	# Balon Greyjoy
	selectable_character = { id = 190 } 	# Tywin Lanister
	selectable_character = { id = 1253 } 	# Mace Tyrell
	selectable_character = { id = 174347 }	# Mance Rayder
	selectable_character = { id = 1303 }	# Beric Dondarrion
	selectable_character = { id = 127 }		# Walder Frey
	selectable_character = { id = 87 }		# Roose Bolton
	selectable_character = { id = 1 }		# Doran Martell
	selectable_character = { id = 94003 }	# Robert Arryn
	selectable_character = { id = 159 }		# Hoster Tully
	selectable_character = { id = 94043 }	# Littlefinger
	selectable_character = { id = 281 }		# Alester Florent
	selectable_character = { id = 1019110 }	# Davos
	selectable_character = { id = 600000040 } # Illyrio Mopatis
	selectable_character = { id = 119982370 } # Sallodhor Saan
	selectable_character = { id = 6571003 }	# Yunkai
	selectable_character = { id = 3300101 }	# Khal Pono
}