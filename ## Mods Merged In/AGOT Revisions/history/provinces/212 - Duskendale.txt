# 212 - Duskendale

# County Title
title = c_duskendale

# Settlements
max_settlements = 7
b_dun_fort = castle
b_duskendale = city
b_dusk_sept = temple
b_woodhollow = castle
b_hammerhold = castle
b_highley = city

#b_newedge = castle
#b_highley = city
#b_woodhollow = castle

# Misc
culture = old_first_man
religion = old_gods

# History
1066.1.1 = {
	b_dun_fort = ca_asoiaf_crown_basevalue_1
	b_dun_fort = ca_asoiaf_crown_basevalue_2
	b_dun_fort = ca_asoiaf_crown_basevalue_3
	b_dun_fort = ca_asoiaf_crown_basevalue_4

	b_duskendale = ct_asoiaf_crown_basevalue_1
	b_duskendale = ct_asoiaf_crown_basevalue_2
	b_duskendale = ct_asoiaf_crown_basevalue_3
	b_duskendale = ct_asoiaf_crown_basevalue_4
	b_duskendale = ct_asoiaf_crown_basevalue_5
	b_duskendale = ct_asoiaf_crown_basevalue_6
	
	b_woodhollow = ca_asoiaf_crown_basevalue_1
	
	b_hammerhold = ca_asoiaf_crown_basevalue_1
	
	b_highley = ct_asoiaf_crown_basevalue_1
	b_highley = ct_asoiaf_crown_basevalue_2
	b_highley = ct_asoiaf_crown_basevalue_3
}

6700.1.1 = {
	culture = crownlander
	religion = the_seven
}

8036.1.1 = { #kings landing expansion
	remove_settlement = b_highley
}