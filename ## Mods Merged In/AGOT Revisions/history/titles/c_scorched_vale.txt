900.1.1={
	liege="d_the_bite"
}
###House Strickland
7969.1.1 = { holder=30001428 } # Robert (nc)
8006.1.1 = { holder=30011428 } # Anslem (nc)
8020.1.1 = { holder=30041428 } # Elwood (nc)
8049.1.1 = { holder=30081428 } # Amelryc (nc)
8073.1.1 = { holder=30091428 } # Edwyn (nc)
8093.1.1 = { holder=30101428 } # Titus (nc)
8123.1.1 = { holder=30151428 } # Hectyr (nc)
8176.1.1 = { holder=30231428 } # Pelleas (nc)
8192.1.1 = { holder=30241428 } # Quentyn (nc)

#House Arryn
8196.1.1 = { 
	holder=202178 	#Donnel Arryn
	liege="k_vale"
}
8218.1.1 = { holder=200178 }	#Othor Arryn
8220.1.1 = { holder=94001 }		#Jesper Arryn

#House Saul
8242.1.1={
	liege="d_the_bite"
	holder=110001
}
8282.1.1={
	holder=210001
}