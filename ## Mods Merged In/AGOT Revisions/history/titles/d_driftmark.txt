7300.1.1={
	liege="k_dragonstone"
	de_jure_liege = k_dragonstone
	law = succ_primogeniture
}

7968.1.1={
    holder = 601052 # Aegor Velaryon (nc)
} 

7990.1.1={
	holder = 38052 #Daemon Velaryon (C)
}
7998.5.9={
	holder = 34052
}
8035.1.1={
	holder = 33052
}
8086.1.1={
	holder = 26052 # Corlys Velaryon (C)
}
8112.1.1={ #Fix for Rhaenyra succession
	#law = succ_feudal_elective
	law = succ_seniority
}
8129.2.12 = {
	law = succ_primogeniture
}
8132.3.6 = {
	holder = 24052 # Alyn Velaryon (C)
}
8176.1.1={
    holder = 23052
}	
8197.1.1={
	holder = 22052
}
8235.1.1={
	holder = 21052
}
8258.1.1={
	holder = 20052
}
8285.1.1={
	holder = 52 	#Monford Velaryon (C)
}
8299.7.1 = {
	holder = 4052   #Monterys Velaryon (C)
}