###Izven of Lhazosh###
10055916 = {
	name="Jommo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	2965.1.1 = {birth="2965.1.1"}
	2981.1.1 = {add_trait=poor_warrior}
	2983.1.1 = {add_spouse=40055916}
	3018.1.1 = {death="3018.1.1"}
}
40055916 = {
	name="Jhiqui"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	2965.1.1 = {birth="2965.1.1"}
	3018.1.1 = {death="3018.1.1"}
}
10155916 = {
	name="Jhigi"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10055916
	mother=40055916

	2984.1.1 = {birth="2984.1.1"}
	3048.1.1 = {death="3048.1.1"}
}
10255916 = {
	name="Zikki"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10055916
	mother=40055916

	2986.1.1 = {birth="2986.1.1"}
	3029.1.1 = {death="3029.1.1"}
}
10355916 = {
	name="Fogo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10055916
	mother=40055916

	2987.1.1 = {birth="2987.1.1"}
	3003.1.1 = {add_trait=poor_warrior}
	3005.1.1 = {add_spouse=40355916}
	3035.1.1 = {death="3035.1.1"}
}
40355916 = {
	name="Zikki"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	2987.1.1 = {birth="2987.1.1"}
	3035.1.1 = {death="3035.1.1"}
}
10455916 = {
	name="Rommo"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10055916
	mother=40055916

	2988.1.1 = {birth="2988.1.1"}
	3004.1.1 = {add_trait=poor_warrior}
	3028.1.1 = {death="3028.1.1"}
}
10555916 = {
	name="Rommo"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10055916
	mother=40055916

	2990.1.1 = {birth="2990.1.1"}
	3006.1.1 = {add_trait=poor_warrior}
	3066.1.1 = {death="3066.1.1"}
}
10655916 = {
	name="Zollo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10355916
	mother=40355916

	3016.1.1 = {birth="3016.1.1"}
	3032.1.1 = {add_trait=trained_warrior}
	3034.1.1 = {add_spouse=40655916}
	3087.1.1 = {death="3087.1.1"}
}
40655916 = {
	name="Zikki"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	3016.1.1 = {birth="3016.1.1"}
	3087.1.1 = {death="3087.1.1"}
}
10755916 = {
	name="Mirri"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10355916
	mother=40355916

	3017.1.1 = {birth="3017.1.1"}
	3054.1.1 = {death="3054.1.1"}
}
10855916 = {
	name="Aggo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10655916
	mother=40655916

	3037.1.1 = {birth="3037.1.1"}
	3053.1.1 = {add_trait=poor_warrior}
	3055.1.1 = {add_spouse=40855916}
	3094.1.1 = {death="3094.1.1"}
}
40855916 = {
	name="Jhigi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	3037.1.1 = {birth="3037.1.1"}
	3094.1.1 = {death="3094.1.1"}
}
10955916 = {
	name="Zolli"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10655916
	mother=40655916

	3040.1.1 = {birth="3040.1.1"}
	3109.1.1 = {death="3109.1.1"}
}
11055916 = {
	name="Rhogoro"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10655916
	mother=40655916

	3041.1.1 = {birth="3041.1.1"}
	3057.1.1 = {add_trait=trained_warrior}
	3098.1.1 = {death="3098.1.1"}
}
11155916 = {
	name="Iggo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10855916
	mother=40855916

	3062.1.1 = {birth="3062.1.1"}
	3078.1.1 = {add_trait=trained_warrior}
	3080.1.1 = {add_spouse=41155916}
	3138.1.1 = {death="3138.1.1"}
}
41155916 = {
	name="Rimmi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	3062.1.1 = {birth="3062.1.1"}
	3138.1.1 = {death="3138.1.1"}
}
11255916 = {
	name="Arakh"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=10855916
	mother=40855916

	3064.1.1 = {birth="3064.1.1"}
	3080.1.1 = {add_trait=poor_warrior}
	3082.1.1 = {add_spouse=41255916}
	3125.1.1 = {death="3125.1.1"}
}
41255916 = {
	name="Coholli"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	3064.1.1 = {birth="3064.1.1"}
	3125.1.1 = {death="3125.1.1"}
}
11355916 = {
	name="Quiri"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11155916
	mother=41155916

	3084.1.1 = {birth="3084.1.1"}
	3163.1.1 = {death="3163.1.1"}
}
11455916 = {
	name="Mago"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11255916
	mother=41255916

	3089.1.1 = {birth="3089.1.1"}
	3105.1.1 = {add_trait=trained_warrior}
	3107.1.1 = {add_spouse=41455916}
	3131.1.1 = {death="3131.1.1"}
}
41455916 = {
	name="Coholli"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	3089.1.1 = {birth="3089.1.1"}
	3131.1.1 = {death="3131.1.1"}
}
11555916 = {
	name="Fogo"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11255916
	mother=41255916

	3091.1.1 = {birth="3091.1.1"}
	3107.1.1 = {add_trait=poor_warrior}
	3144.1.1 = {death="3144.1.1"}
}
11655916 = {
	name="Zolli"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11255916
	mother=41255916

	3092.1.1 = {birth="3092.1.1"}
	3157.1.1 = {death="3157.1.1"}
}
11755916 = {
	name="Ogo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11455916
	mother=41455916

	3108.1.1 = {birth="3108.1.1"}
	3124.1.1 = {add_trait=poor_warrior}
	3126.1.1 = {add_spouse=41755916}
	3176.1.1 = {death="3176.1.1"}
}
41755916 = {
	name="Mirri"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	3108.1.1 = {birth="3108.1.1"}
	3176.1.1 = {death="3176.1.1"}
}
11855916 = {
	name="Jhigi"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11455916
	mother=41455916

	3109.1.1 = {birth="3109.1.1"}
	3167.1.1 = {death="3167.1.1"}
}
11955916 = {
	name="Mago"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11455916
	mother=41455916

	3111.1.1 = {birth="3111.1.1"}
	3127.1.1 = {add_trait=poor_warrior}
	3158.1.1 = {death="3158.1.1"}
}
12055916 = {
	name="Qothi"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11455916
	mother=41455916

	3113.1.1 = {birth="3113.1.1"}
	3142.1.1 = {death = {death_reason = death_murder}}
}
12155916 = {
	name="Zollo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11755916
	mother=41755916

	3127.1.1 = {birth="3127.1.1"}
	3143.1.1 = {add_trait=trained_warrior}
	3145.1.1 = {add_spouse=42155916}
	3185.1.1 = {death="3185.1.1"}
}
42155916 = {
	name="Zikki"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	3127.1.1 = {birth="3127.1.1"}
	3185.1.1 = {death="3185.1.1"}
}
12255916 = {
	name="Rakharo"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11755916
	mother=41755916

	3129.1.1 = {birth="3129.1.1"}
	3145.1.1 = {add_trait=trained_warrior}
	3160.1.1 = {death="3160.1.1"}
}
12355916 = {
	name="Mirri"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11755916
	mother=41755916

	3130.1.1 = {birth="3130.1.1"}
	3178.1.1 = {death="3178.1.1"}
}
12455916 = {
	name="Mago"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11755916
	mother=41755916

	3132.1.1 = {birth="3132.1.1"}
	3148.1.1 = {add_trait=poor_warrior}
	3204.1.1 = {death="3204.1.1"}
}
12555916 = {
	name="Rimmi"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=11755916
	mother=41755916

	3133.1.1 = {birth="3133.1.1"}
	3177.1.1 = {death="3177.1.1"}
}
12655916 = {
	name="Bharbo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=12155916
	mother=42155916

	3153.1.1 = {birth="3153.1.1"}
	3169.1.1 = {add_trait=poor_warrior}
	3171.1.1 = {add_spouse=42655916}
	3198.1.1 = {death="3198.1.1"}
}
42655916 = {
	name="Zikki"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	3153.1.1 = {birth="3153.1.1"}
	3198.1.1 = {death="3198.1.1"}
}
12755916 = {
	name="Rimmi"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=12155916
	mother=42155916

	3154.1.1 = {birth="3154.1.1"}
	3203.1.1 = {death="3203.1.1"}
}
12855916 = {
	name="Zolli"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=12155916
	mother=42155916

	3155.1.1 = {birth="3155.1.1"}
	3203.1.1 = {death = {death_reason = death_accident}}
}
12955916 = {
	name="Haggo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=12655916
	mother=42655916

	3174.1.1 = {birth="3174.1.1"}
	3190.1.1 = {add_trait=trained_warrior}
	3192.1.1 = {add_spouse=42955916}
	3239.1.1 = {death="3239.1.1"}
}
42955916 = {
	name="Jhigi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	3174.1.1 = {birth="3174.1.1"}
	3239.1.1 = {death="3239.1.1"}
}
13055916 = {
	name="Rakharo"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=12655916
	mother=42655916

	3175.1.1 = {birth="3175.1.1"}
	3191.1.1 = {add_trait=trained_warrior}
	3193.1.1 = {add_spouse=43055916}
	3225.1.1 = {death="3225.1.1"}
}
43055916 = {
	name="Irri"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	3175.1.1 = {birth="3175.1.1"}
	3225.1.1 = {death="3225.1.1"}
}
13155916 = {
	name="Mothi"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=12655916
	mother=42655916

	3177.1.1 = {birth="3177.1.1"}
	3227.1.1 = {death="3227.1.1"}
}
13255916 = {
	name="Mirri"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=12955916
	mother=42955916

	3201.1.1 = {birth="3201.1.1"}
	3245.1.1 = {death="3245.1.1"}
}
13355916 = {
	name="Rakhiri"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=13055916
	mother=43055916

	3200.1.1 = {birth="3200.1.1"}
	3276.1.1 = {death="3276.1.1"}
}
13455916 = {
	name="Cohollo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=13055916
	mother=43055916

	3201.1.1 = {birth="3201.1.1"}
	3217.1.1 = {add_trait=poor_warrior}
	3219.1.1 = {add_spouse=43455916}
	3266.1.1 = {death="3266.1.1"}
}
43455916 = {
	name="Coholli"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	3201.1.1 = {birth="3201.1.1"}
	3266.1.1 = {death="3266.1.1"}
}
13555916 = {
	name="Rakharo"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=13055916
	mother=43055916

	3202.1.1 = {birth="3202.1.1"}
	3218.1.1 = {add_trait=poor_warrior}
	3255.1.1 = {death="3255.1.1"}
}
13655916 = {
	name="Pono"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=13455916
	mother=43455916

	3234.1.1 = {birth="3234.1.1"}
	3250.1.1 = {add_trait=trained_warrior}
	3252.1.1 = {add_spouse=43655916}
	3287.1.1 = {death="3287.1.1"}
}
43655916 = {
	name="Jhiqui"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	3234.1.1 = {birth="3234.1.1"}
	3287.1.1 = {death="3287.1.1"}
}
13755916 = {
	name="Rhogoro"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=13455916
	mother=43455916

	3237.1.1 = {birth="3237.1.1"}
	3253.1.1 = {add_trait=trained_warrior}
	3255.1.1 = {add_spouse=43755916}
	3269.1.1 = {death = {death_reason = death_murder}}
}
43755916 = {
	name="Kovarri"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	3237.1.1 = {birth="3237.1.1"}
	3269.1.1 = {death="3269.1.1"}
}
13855916 = {
	name="Qotho"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=13455916
	mother=43455916

	3240.1.1 = {birth="3240.1.1"}
	3256.1.1 = {add_trait=poor_warrior}
	3290.1.1 = {death="3290.1.1"}
}
13955916 = {
	name="Mirri"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=13655916
	mother=43655916

	3258.1.1 = {birth="3258.1.1"}
}
14055916 = {
	name="Rakharo"	# a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=13755916
	mother=43755916

	3257.1.1 = {birth="3257.1.1"}
	3273.1.1 = {add_trait=poor_warrior}
	3286.1.1 = {add_spouse=44055916}
}
44055916 = {
	name="Jhigi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	3268.1.1 = {birth="3268.1.1"}
}
14155916 = {
	name="Coholli"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=13755916
	mother=43755916

	3260.1.1 = {birth="3260.1.1"}
}
14255916 = {
	name="Mirri"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=14055916
	mother=44055916

	3294.1.1 = {birth="3294.1.1"}
}
14355916 = {
	name="Motho"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=14055916
	mother=44055916

	3297.1.1 = {birth="3297.1.1"}
	3313.1.1 = {add_trait=poor_warrior}
}
14455916 = {
	name="Quiri"	# not a lord
	female=yes
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=14055916
	mother=44055916

	3298.1.1 = {birth="3298.1.1"}
}
14555916 = {
	name="Ogo"	# not a lord
	dynasty=55916

	religion="great_shepherd"
	culture="lhazareen"

	father=14055916
	mother=44055916

	3299.1.1 = {birth="3299.1.1"}
	3315.1.1 = {add_trait=poor_warrior}
}
###Sheqethi of Hesh###
10055923 = {
	name="Pono"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	2953.1.1 = {birth="2953.1.1"}
	2969.1.1 = {add_trait=poor_warrior}
	2971.1.1 = {add_spouse=40055923}
	3010.1.1 = {death="3010.1.1"}
}
40055923 = {
	name="Rimmi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	2953.1.1 = {birth="2953.1.1"}
	3010.1.1 = {death="3010.1.1"}
}
10155923 = {
	name="Rhogoro"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10055923
	mother=40055923

	2982.1.1 = {birth="2982.1.1"}
	2998.1.1 = {add_trait=trained_warrior}
	3053.1.1 = {death="3053.1.1"}
}
10255923 = {
	name="Rhogiri"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10055923
	mother=40055923

	2984.1.1 = {birth="2984.1.1"}
	3038.1.1 = {death="3038.1.1"}
}
10355923 = {
	name="Motho"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10055923
	mother=40055923

	2985.1.1 = {birth="2985.1.1"}
	3001.1.1 = {add_trait=trained_warrior}
	3003.1.1 = {add_spouse=40355923}
	3057.1.1 = {death="3057.1.1"}
}
40355923 = {
	name="Mothi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	2985.1.1 = {birth="2985.1.1"}
	3057.1.1 = {death="3057.1.1"}
}
10455923 = {
	name="Rakharo"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10355923
	mother=40355923

	3003.1.1 = {birth="3003.1.1"}
	3019.1.1 = {add_trait=poor_warrior}
	3080.1.1 = {death="3080.1.1"}
}
10555923 = {
	name="Jhogo"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10355923
	mother=40355923

	3004.1.1 = {birth="3004.1.1"}
	3020.1.1 = {add_trait=trained_warrior}
	3022.1.1 = {add_spouse=40555923}
	3064.1.1 = {death = {death_reason = death_accident}}
}
40555923 = {
	name="Aggi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	3004.1.1 = {birth="3004.1.1"}
	3064.1.1 = {death="3064.1.1"}
}
10655923 = {
	name="Rhogiri"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10555923
	mother=40555923

	3023.1.1 = {birth="3023.1.1"}
	3066.1.1 = {death="3066.1.1"}
}
10755923 = {
	name="Pono"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10555923
	mother=40555923

	3026.1.1 = {birth="3026.1.1"}
	3042.1.1 = {add_trait=poor_warrior}
	3044.1.1 = {add_spouse=40755923}
	3082.1.1 = {death="3082.1.1"}
}
40755923 = {
	name="Rhogiri"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	3026.1.1 = {birth="3026.1.1"}
	3082.1.1 = {death="3082.1.1"}
}
10855923 = {
	name="Coholli"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10555923
	mother=40555923

	3027.1.1 = {birth="3027.1.1"}
	3073.1.1 = {death="3073.1.1"}
}
10955923 = {
	name="Malakho"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10555923
	mother=40555923

	3029.1.1 = {birth="3029.1.1"}
	3045.1.1 = {add_trait=poor_warrior}
	3047.1.1 = {add_spouse=40955923}
	3076.1.1 = {death="3076.1.1"}
}
40955923 = {
	name="Aggi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	3029.1.1 = {birth="3029.1.1"}
	3076.1.1 = {death="3076.1.1"}
}
11055923 = {
	name="Jhigi"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10555923
	mother=40555923

	3032.1.1 = {birth="3032.1.1"}
	3070.1.1 = {death="3070.1.1"}
}
11155923 = {
	name="Zikki"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10955923
	mother=40955923

	3049.1.1 = {birth="3049.1.1"}
	3115.1.1 = {death="3115.1.1"}
}
11255923 = {
	name="Aggi"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10955923
	mother=40955923

	3051.1.1 = {birth="3051.1.1"}
	3096.1.1 = {death="3096.1.1"}
}
11355923 = {
	name="Zollo"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10755923
	mother=40755923

	3047.1.1 = {birth="3047.1.1"}
	3063.1.1 = {add_trait=trained_warrior}
	3105.1.1 = {death="3105.1.1"}
}
11455923 = {
	name="Fogo"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10755923
	mother=40755923

	3048.1.1 = {birth="3048.1.1"}
	3064.1.1 = {add_trait=trained_warrior}
	3115.1.1 = {death="3115.1.1"}
}
11555923 = {
	name="Jommo"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10755923
	mother=40755923

	3049.1.1 = {birth="3049.1.1"}
	3065.1.1 = {add_trait=poor_warrior}
	3067.1.1 = {add_spouse=41555923}
	3124.1.1 = {death = {death_reason = death_accident}}
}
41555923 = {
	name="Aggi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	3049.1.1 = {birth="3049.1.1"}
	3124.1.1 = {death="3124.1.1"}
}
11655923 = {
	name="Kovarri"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=10755923
	mother=40755923

	3052.1.1 = {birth="3052.1.1"}
	3101.1.1 = {death="3101.1.1"}
}
11755923 = {
	name="Rimmi"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=11555923
	mother=41555923

	3074.1.1 = {birth="3074.1.1"}
	3143.1.1 = {death="3143.1.1"}
}
11855923 = {
	name="Rakharo"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=11555923
	mother=41555923

	3075.1.1 = {birth="3075.1.1"}
	3091.1.1 = {add_trait=poor_warrior}
	3093.1.1 = {add_spouse=41855923}
	3124.1.1 = {death="3124.1.1"}
}
41855923 = {
	name="Zolli"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	3075.1.1 = {birth="3075.1.1"}
	3124.1.1 = {death="3124.1.1"}
}
11955923 = {
	name="Pono"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=11855923
	mother=41855923

	3096.1.1 = {birth="3096.1.1"}
	3106.1.1 = {death = {death_reason = death_accident}}
}
12055923 = {
	name="Motho"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=11855923
	mother=41855923

	3098.1.1 = {birth="3098.1.1"}
	3114.1.1 = {add_trait=trained_warrior}
	3172.1.1 = {death="3172.1.1"}
}
12155923 = {
	name="Zikki"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=11855923
	mother=41855923

	3099.1.1 = {birth="3099.1.1"}
	3144.1.1 = {death="3144.1.1"}
}
12255923 = {
	name="Jhigi"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=11855923
	mother=41855923

	3101.1.1 = {birth="3101.1.1"}
	3110.1.1 = {death = {death_reason = death_accident}}
}
12355923 = {
	name="Drogo"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=11855923
	mother=41855923

	3104.1.1 = {birth="3104.1.1"}
	3120.1.1 = {add_trait=trained_warrior}
	3122.1.1 = {add_spouse=42355923}
	3160.1.1 = {death="3160.1.1"}
}
42355923 = {
	name="Mothi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	3104.1.1 = {birth="3104.1.1"}
	3160.1.1 = {death="3160.1.1"}
}
12455923 = {
	name="Jhiqui"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=12355923
	mother=42355923

	3137.1.1 = {birth="3137.1.1"}
	3190.1.1 = {death="3190.1.1"}
}
12555923 = {
	name="Moro"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=12355923
	mother=42355923

	3140.1.1 = {birth="3140.1.1"}
	3156.1.1 = {add_trait=trained_warrior}
	3158.1.1 = {add_spouse=42555923}
	3173.1.1 = {death="3173.1.1"}
}
42555923 = {
	name="Zolli"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	3140.1.1 = {birth="3140.1.1"}
	3173.1.1 = {death="3173.1.1"}
}
12655923 = {
	name="Rommo"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=12555923
	mother=42555923

	3164.1.1 = {birth="3164.1.1"}
	3180.1.1 = {add_trait=trained_warrior}
	3198.1.1 = {death="3198.1.1"}
}
12755923 = {
	name="Rakharo"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=12555923
	mother=42555923

	3165.1.1 = {birth="3165.1.1"}
	3181.1.1 = {add_trait=trained_warrior}
	3208.1.1 = {death="3208.1.1"}
}
12855923 = {
	name="Zekko"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=12555923
	mother=42555923

	3166.1.1 = {birth="3166.1.1"}
	3182.1.1 = {add_trait=poor_warrior}
	3184.1.1 = {add_spouse=42855923}
	3220.1.1 = {death="3220.1.1"}
}
42855923 = {
	name="Jhiqui"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	3166.1.1 = {birth="3166.1.1"}
	3220.1.1 = {death="3220.1.1"}
}
12955923 = {
	name="Jommo"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=12855923
	mother=42855923

	3191.1.1 = {birth="3191.1.1"}
	3207.1.1 = {add_trait=trained_warrior}
	3209.1.1 = {add_spouse=42955923}
	3232.1.1 = {death="3232.1.1"}
}
42955923 = {
	name="Qothi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	3191.1.1 = {birth="3191.1.1"}
	3232.1.1 = {death="3232.1.1"}
}
13055923 = {
	name="Rommo"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=12955923
	mother=42955923

	3215.1.1 = {birth="3215.1.1"}
	3231.1.1 = {add_trait=trained_warrior}
	3262.1.1 = {death = {death_reason = death_accident}}
}
13155923 = {
	name="Iggi"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=12955923
	mother=42955923

	3216.1.1 = {birth="3216.1.1"}
	3269.1.1 = {death="3269.1.1"}
}
13255923 = {
	name="Qotho"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=12955923
	mother=42955923

	3218.1.1 = {birth="3218.1.1"}
	3234.1.1 = {add_trait=trained_warrior}
	3236.1.1 = {add_spouse=43255923}
	3270.1.1 = {death="3270.1.1"}
}
43255923 = {
	name="Mothi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	3218.1.1 = {birth="3218.1.1"}
	3270.1.1 = {death="3270.1.1"}
}
13355923 = {
	name="Rakharo"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=13255923
	mother=43255923

	3242.1.1 = {birth="3242.1.1"}
	3258.1.1 = {add_trait=poor_warrior}
}
13455923 = {
	name="Iggo"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=13255923
	mother=43255923

	3245.1.1 = {birth="3245.1.1"}
	3261.1.1 = {add_trait=poor_warrior}
}
13555923 = {
	name="Mago"	# a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=13255923
	mother=43255923

	3246.1.1 = {birth="3246.1.1"}
	3262.1.1 = {add_trait=trained_warrior}
	3264.1.1 = {add_spouse=43555923}
}
43555923 = {
	name="Mothi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	3246.1.1 = {birth="3246.1.1"}
}
13655923 = {
	name="Malakho"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=13555923
	mother=43555923

	3279.1.1 = {birth="3279.1.1"}
	3295.1.1 = {add_trait=poor_warrior}
}
13755923 = {
	name="Jhaquo"	# not a lord
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=13555923
	mother=43555923

	3281.1.1 = {birth="3281.1.1"}
	3297.1.1 = {add_trait=poor_warrior}
}
13855923 = {
	name="Rimmi"	# not a lord
	female=yes
	dynasty=55923

	religion="great_shepherd"
	culture="lhazareen"

	father=13555923
	mother=43555923

	3283.1.1 = {birth="3283.1.1"}
}
###Drogikh of Kosrak###
10055901 = {
	name="Moro"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	2932.1.1 = {birth="2932.1.1"}
	2948.1.1 = {add_trait=trained_warrior}
	2950.1.1 = {add_spouse=40055901}
	3004.1.1 = {death="3004.1.1"}
}
40055901 = {
	name="Irri"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	2932.1.1 = {birth="2932.1.1"}
	3004.1.1 = {death="3004.1.1"}
}
10155901 = {
	name="Drogo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10055901
	mother=40055901

	2961.1.1 = {birth="2961.1.1"}
	2977.1.1 = {add_trait=poor_warrior}
	2979.1.1 = {add_spouse=40155901}
	3025.1.1 = {death="3025.1.1"}
}
40155901 = {
	name="Mirri"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	2961.1.1 = {birth="2961.1.1"}
	3025.1.1 = {death="3025.1.1"}
}
10255901 = {
	name="Jhaquo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10055901
	mother=40055901

	2963.1.1 = {birth="2963.1.1"}
	2979.1.1 = {add_trait=poor_warrior}
	3007.1.1 = {death="3007.1.1"}
}
10355901 = {
	name="Rakhiri"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10055901
	mother=40055901

	2964.1.1 = {birth="2964.1.1"}
	3030.1.1 = {death="3030.1.1"}
}
10455901 = {
	name="Mago"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10055901
	mother=40055901

	2967.1.1 = {birth="2967.1.1"}
	2983.1.1 = {add_trait=poor_warrior}
	2985.1.1 = {add_spouse=40455901}
	3046.1.1 = {death="3046.1.1"}
}
40455901 = {
	name="Zolli"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	2967.1.1 = {birth="2967.1.1"}
	3046.1.1 = {death="3046.1.1"}
}
10555901 = {
	name="Pono"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10155901
	mother=40155901

	2989.1.1 = {birth="2989.1.1"}
	3005.1.1 = {add_trait=poor_warrior}
	3050.1.1 = {death="3050.1.1"}
}
10655901 = {
	name="Iggi"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10155901
	mother=40155901

	2990.1.1 = {birth="2990.1.1"}
	3023.1.1 = {death="3023.1.1"}
}
10755901 = {
	name="Aggi"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10455901
	mother=40455901

	2995.1.1 = {birth="2995.1.1"}
	3051.1.1 = {death="3051.1.1"}
}
10855901 = {
	name="Ogo"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10455901
	mother=40455901

	2996.1.1 = {birth="2996.1.1"}
	3012.1.1 = {add_trait=poor_warrior}
	3014.1.1 = {add_spouse=40855901}
	3047.1.1 = {death="3047.1.1"}
}
40855901 = {
	name="Jhigi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	2996.1.1 = {birth="2996.1.1"}
	3047.1.1 = {death="3047.1.1"}
}
10955901 = {
	name="Zikki"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10855901
	mother=40855901

	3028.1.1 = {birth="3028.1.1"}
	3086.1.1 = {death = {death_reason = death_murder}}
}
11055901 = {
	name="Rakharo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10855901
	mother=40855901

	3029.1.1 = {birth="3029.1.1"}
	3045.1.1 = {add_trait=poor_warrior}
	3087.1.1 = {death="3087.1.1"}
}
11155901 = {
	name="Cohollo"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=10855901
	mother=40855901

	3031.1.1 = {birth="3031.1.1"}
	3047.1.1 = {add_trait=trained_warrior}
	3049.1.1 = {add_spouse=41155901}
	3098.1.1 = {death="3098.1.1"}
}
41155901 = {
	name="Irri"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	3031.1.1 = {birth="3031.1.1"}
	3098.1.1 = {death="3098.1.1"}
}
11255901 = {
	name="Jhaquo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11155901
	mother=41155901

	3051.1.1 = {birth="3051.1.1"}
	3067.1.1 = {add_trait=poor_warrior}
	3069.1.1 = {add_spouse=41255901}
	3083.1.1 = {death = {death_reason = death_battle}}
}
41255901 = {
	name="Aggi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	3051.1.1 = {birth="3051.1.1"}
	3083.1.1 = {death="3083.1.1"}
}
11355901 = {
	name="Haggo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11155901
	mother=41155901

	3054.1.1 = {birth="3054.1.1"}
	3070.1.1 = {add_trait=poor_warrior}
	3098.1.1 = {death="3098.1.1"}
}
11455901 = {
	name="Cohollo"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11255901
	mother=41255901

	3074.1.1 = {birth="3074.1.1"}
	3090.1.1 = {add_trait=poor_warrior}
	3092.1.1 = {add_spouse=41455901}
	3128.1.1 = {death="3128.1.1"}
}
41455901 = {
	name="Kovarri"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	3074.1.1 = {birth="3074.1.1"}
	3128.1.1 = {death="3128.1.1"}
}
11555901 = {
	name="Zikki"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11255901
	mother=41255901

	3076.1.1 = {birth="3076.1.1"}
	3098.1.1 = {death="3098.1.1"}
}
11655901 = {
	name="Jommo"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11255901
	mother=41255901

	3079.1.1 = {birth="3079.1.1"}
	3095.1.1 = {add_trait=poor_warrior}
	3097.1.1 = {add_spouse=41655901}
	3110.1.1 = {death="3110.1.1"}
}
41655901 = {
	name="Coholli"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	3079.1.1 = {birth="3079.1.1"}
	3110.1.1 = {death="3110.1.1"}
}
11755901 = {
	name="Zikki"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11255901
	mother=41255901

	3082.1.1 = {birth="3082.1.1"}
	3146.1.1 = {death="3146.1.1"}
}
11855901 = {
	name="Qothi"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11655901
	mother=41655901

	3100.1.1 = {birth="3100.1.1"}
	3140.1.1 = {death="3140.1.1"}
}
11955901 = {
	name="Jhigi"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11455901
	mother=41455901

	3096.1.1 = {birth="3096.1.1"}
	3153.1.1 = {death="3153.1.1"}
}
12055901 = {
	name="Kovarro"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11455901
	mother=41455901

	3098.1.1 = {birth="3098.1.1"}
	3114.1.1 = {add_trait=poor_warrior}
	3161.1.1 = {death="3161.1.1"}
}
12155901 = {
	name="Qothi"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11455901
	mother=41455901

	3100.1.1 = {birth="3100.1.1"}
	3171.1.1 = {death="3171.1.1"}
}
12255901 = {
	name="Rakharo"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=11455901
	mother=41455901

	3103.1.1 = {birth="3103.1.1"}
	3119.1.1 = {add_trait=poor_warrior}
	3121.1.1 = {add_spouse=42255901}
	3144.1.1 = {death="3144.1.1"}
}
42255901 = {
	name="Mothi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	3103.1.1 = {birth="3103.1.1"}
	3144.1.1 = {death="3144.1.1"}
}
12355901 = {
	name="Jhiqui"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=12255901
	mother=42255901

	3124.1.1 = {birth="3124.1.1"}
	3173.1.1 = {death="3173.1.1"}
}
12455901 = {
	name="Fogo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=12255901
	mother=42255901

	3127.1.1 = {birth="3127.1.1"}
	3143.1.1 = {add_trait=trained_warrior}
	3201.1.1 = {death="3201.1.1"}
}
12555901 = {
	name="Jhaquo"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=12255901
	mother=42255901

	3129.1.1 = {birth="3129.1.1"}
	3145.1.1 = {add_trait=poor_warrior}
	3147.1.1 = {add_spouse=42555901}
	3183.1.1 = {death="3183.1.1"}
}
42555901 = {
	name="Mothi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	3129.1.1 = {birth="3129.1.1"}
	3183.1.1 = {death="3183.1.1"}
}
12655901 = {
	name="Rommo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=12555901
	mother=42555901

	3155.1.1 = {birth="3155.1.1"}
	3171.1.1 = {add_trait=poor_warrior}
	3221.1.1 = {death="3221.1.1"}
}
12755901 = {
	name="Zollo"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=12555901
	mother=42555901

	3158.1.1 = {birth="3158.1.1"}
	3174.1.1 = {add_trait=poor_warrior}
	3176.1.1 = {add_spouse=42755901}
	3217.1.1 = {death="3217.1.1"}
}
42755901 = {
	name="Zolli"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	3158.1.1 = {birth="3158.1.1"}
	3217.1.1 = {death="3217.1.1"}
}
12855901 = {
	name="Iggi"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=12555901
	mother=42555901

	3159.1.1 = {birth="3159.1.1"}
	3208.1.1 = {death="3208.1.1"}
}
12955901 = {
	name="Motho"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=12755901
	mother=42755901

	3191.1.1 = {birth="3191.1.1"}
	3207.1.1 = {add_trait=poor_warrior}
	3248.1.1 = {death = {death_reason = death_murder}}
}
13055901 = {
	name="Jhiqui"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=12755901
	mother=42755901

	3194.1.1 = {birth="3194.1.1"}
	3247.1.1 = {death="3247.1.1"}
}
13155901 = {
	name="Pono"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=12755901
	mother=42755901

	3195.1.1 = {birth="3195.1.1"}
	3211.1.1 = {add_trait=trained_warrior}
	3213.1.1 = {add_spouse=43155901}
	3250.1.1 = {death="3250.1.1"}
}
43155901 = {
	name="Zolli"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	3195.1.1 = {birth="3195.1.1"}
	3250.1.1 = {death="3250.1.1"}
}
13255901 = {
	name="Jhiqui"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=12755901
	mother=42755901

	3196.1.1 = {birth="3196.1.1"}
	3236.1.1 = {death="3236.1.1"}
}
13355901 = {
	name="Qotho"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13155901
	mother=43155901

	3228.1.1 = {birth="3228.1.1"}
	3244.1.1 = {add_trait=trained_warrior}
	3282.1.1 = {death="3282.1.1"}
}
13455901 = {
	name="Rakharo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13155901
	mother=43155901

	3229.1.1 = {birth="3229.1.1"}
	3245.1.1 = {add_trait=trained_warrior}
	3299.1.1 = {death="3299.1.1"}
}
13555901 = {
	name="Jhaquo"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13155901
	mother=43155901

	3231.1.1 = {birth="3231.1.1"}
	3247.1.1 = {add_trait=trained_warrior}
	3249.1.1 = {add_spouse=43555901}
	3279.1.1 = {death="3279.1.1"}
}
43555901 = {
	name="Jhiqui"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	3231.1.1 = {birth="3231.1.1"}
	3279.1.1 = {death="3279.1.1"}
}
13655901 = {
	name="Rhogiri"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13155901
	mother=43155901

	3234.1.1 = {birth="3234.1.1"}
	3308.1.1 = {death="3308.1.1"}
}
13755901 = {
	name="Rakhiri"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13555901
	mother=43555901

	3259.1.1 = {birth="3259.1.1"}
}
13855901 = {
	name="Moro"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13555901
	mother=43555901

	3262.1.1 = {birth="3262.1.1"}
	3278.1.1 = {add_trait=poor_warrior}
	3280.1.1 = {add_spouse=43855901}
}
43855901 = {
	name="Zolli"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	3262.1.1 = {birth="3262.1.1"}
}
13955901 = {
	name="Drogo"	# a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13555901
	mother=43555901

	3264.1.1 = {birth="3264.1.1"}
	3280.1.1 = {add_trait=poor_warrior}
	3282.1.1 = {add_spouse=43955901}
}
43955901 = {
	name="Jhigi"
	female=yes

	religion="great_shepherd"
	culture="lhazareen"

	3264.1.1 = {birth="3264.1.1"}
}
14055901 = {
	name="Iggi"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13855901
	mother=43855901

	3281.1.1 = {birth="3281.1.1"}
}
14155901 = {
	name="Fogo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13955901
	mother=43955901

	3292.1.1 = {birth="3292.1.1"}
	3308.1.1 = {add_trait=poor_warrior}
}
14255901 = {
	name="Haggo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13955901
	mother=43955901

	3295.1.1 = {birth="3295.1.1"}
	3311.1.1 = {add_trait=trained_warrior}
}
14355901 = {
	name="Mirri"	# not a lord
	female=yes
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13955901
	mother=43955901

	3296.1.1 = {birth="3296.1.1"}
}
14455901 = {
	name="Temmo"	# not a lord
	dynasty=55901

	religion="great_shepherd"
	culture="lhazareen"

	father=13955901
	mother=43955901

	3299.1.1 = {birth="3299.1.1"}
	3315.1.1 = {add_trait=poor_warrior}
}
