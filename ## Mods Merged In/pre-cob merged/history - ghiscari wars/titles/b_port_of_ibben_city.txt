1.1.1={ 
	law = succ_appointment	
	effect = {
		set_title_flag = military_command
		holder_scope = {
			if = {
				limit = { primary_title = { title = PREVPREV } }
				set_government_type = military_command_government_city 
				PREV = { succession = appointment }
				recalc_succession = yes
			}
		}
	}	
}
# 2990.1.1 = { holder=1055927 } # Togg (nc)
# 3022.1.1 = { holder=1155927 } # Togg (nc)
# 3040.1.1 = { holder=1655927 } # Groo (nc)
# 3042.1.1 = { holder=1755927 } # Caloth (nc)
# 3081.1.1 = { holder=1955927 } # Tugg (nc)
# 3127.1.1 = { holder=2155927 } # Galt (nc)
# 3146.1.1 = { holder=2255927 } # Galt (nc)
# 3159.1.1 = { holder=2555927 } # Slugg (nc)
# 3229.1.1 = { holder=2855927 } # Togg (nc)
# 3255.1.1 = { holder=3255927 } # Krull (nc)
# 3263.1.1 = { holder=3355927 } # Durr (nc)
# 3296.1.1 = { holder=3755927 } # Holger (nc)
