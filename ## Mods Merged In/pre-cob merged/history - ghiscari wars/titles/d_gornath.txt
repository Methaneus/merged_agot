400.1.1={
	#liege="k_sarnor"
	effect = {
		holder_scope = { 
			k_sarnor = {
				holder_scope = {
					make_tributary = { who = PREVPREV tributary_type = sarnori }
				}
			}
		}	
	}
	law = succ_primogeniture
}
2995.1.1 = { holder=10056811 } # Ishwar (nc)
3017.1.1 = { holder=10156811 } # Nayan (nc)
3022.1.1 = { holder=10356811 } # Avilash (nc)
3060.1.1 = { holder=10556811 } # Ganesh (nc)
3097.1.1 = { holder=11056811 } # Rathin (nc)
3132.1.1 = { holder=11256811 } # Kirthana (nc)
3154.1.1 = { holder=11656811 } # Utkarsh (nc)
3176.1.1 = { holder=11756811 } # Rathin (nc)
3207.1.1 = { holder=12156811 } # Yathavan (nc)
3229.1.1 = { holder=12556811 } # Avilash (nc)
3262.1.1 = { holder=13056811 } # Bhagesh (nc)
3283.1.1 = { holder=13356811 } # Zaara (nc)
