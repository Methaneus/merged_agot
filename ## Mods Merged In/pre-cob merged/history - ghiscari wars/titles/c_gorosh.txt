1.1.1 = {
	liege = e_ghiscar
	effect = {
		location = { 
			set_province_flag = ca_colony_1
			set_province_flag = ruined_province
			capital_holding = { add_building = ct_colony_1 }
			set_province_flag = colony_province
			add_province_modifier = {
				name = colony
				duration = -1
			}
		}		
	}
}

