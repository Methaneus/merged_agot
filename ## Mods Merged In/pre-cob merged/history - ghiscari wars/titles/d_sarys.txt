400.1.1={
	#liege="k_sarnor"
	effect = {
		holder_scope = { 
			k_sarnor = {
				holder_scope = {
					make_tributary = { who = PREVPREV tributary_type = sarnori }
				}
			}
		}	
	}
	law = succ_primogeniture
}
2995.1.1 = { holder=10056819 } # Etash (nc)
3020.1.1 = { holder=10156819 } # Balavant (nc)
3037.1.1 = { holder=10356819 } # Manish (nc)
3062.1.1 = { holder=10856819 } # Balavant (nc)
3095.1.1 = { holder=11256819 } # Nitish (nc)
3131.1.1 = { holder=11656819 } # Lakshan (nc)
3155.1.1 = { holder=12056819 } # Ranak (nc)
3181.1.1 = { holder=12256819 } # Kalpesh (nc)
3240.1.1 = { holder=12556819 } # Rathin (nc)
3273.1.1 = { holder=12756819 } # Kedaar (nc)
3286.1.1 = { holder=12956819 } # Darshan (nc)
3294.1.1 = { holder=13156819 } # Lokesh (nc)
