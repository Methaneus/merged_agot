#c_lannisport
1.1.1={
	liege="d_casterly_rock"
}

1.1.1 = { holder=80270190 } # Loreon the Lion
1.1.1 = { holder=0 }

# Pre-Andal period
1842.1.1 = { holder=80310190 } # Gerold the Great (C)
1887.1.1 = { holder = 0 }

2970.1.1 = { holder=860370190 } # Tyson (NC)
2999.1.1 = { holder=850370190 } # Tymore (NC)
3018.1.1 = { holder=840370190 } # Tyland (NC)
3046.1.1 = { holder=830370190 } # Norbert (NC)
3065.1.1 = { holder=820370190 } # Luceon (NC)
3092.1.1 = { holder=810370190 } # Tywin (NC)

3131.1.1 = { holder=80370190 } # Tymeon I (nc)
3141.1.1 = { holder=80380190 } # Tybolt Tunderbolt (C)
3187.1.1 = { holder=80390190 } # Tyrion III (C)
3204.1.1 = { holder=80400190 } # Gerold II (C)
3217.1.1 = { holder=80401190 } # Gerold III (C)
3254.1.1 = { holder=80403190 } # Joffrey I Lydden (C)
