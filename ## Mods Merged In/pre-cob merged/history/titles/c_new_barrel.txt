1.1.1={
	liege="d_mandervale"
	name = c_dunstonbury
	effect = {
		if = {
			limit = { NOT = { year = 8196 } }
			location = { set_name = c_dunstonbury } # Manderly Castle before exile
		}	
	}
}

6318.1.1 = { holder=660080 } # Wyman (nc)
6338.1.1 = { holder=670080 } # Borian (nc)
6375.1.1 = { holder=680080 } # Torrhen (nc)
6406.1.1 = { holder=60080 } # Emmett (nc)
6415.1.1 = { holder=60180 } # Wylan (nc)
6439.1.1 = { holder=60280 } # Elman (nc)

6443.1.1={
	holder = 20080
}
6482.1.1={
	holder = 19080
}
6523.1.1={
	holder = 17080
}
6536.1.1={
	holder = 16080
}
6586.1.1={
	holder = 15080 # Heward
}
6594.1.1={
	holder = 13080 # Desmond
}
6610.2.2={
	holder = 1015080 # Torrhen
}
6617.2.8={
	holder = 12080 # Borion
}
6650.1.1={
	holder = 11080 # Waymar
}
6700.1.1={
	holder = 10080 # Wayn
}
6710.1.1={
	holder = 1034080
}
6729.1.1={
	holder = 1004080
}






