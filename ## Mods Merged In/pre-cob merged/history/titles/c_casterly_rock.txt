1.1.1={
	liege="d_casterly_rock"
}

40.1.1 = { holder=20174406 } # Corlos, son of Caster # Line of Casterlys
80.1.1 = { holder=21174406 } # Carlon, son of Corlos
120.1.1 = { holder=22174406 } # Caston, son of Carlon
150.1.1 = { holder=23174406 } # Criston, son of Caston
156.1.1 = { holder=24174406 } # Corlos, son of Criston
186.1.1 = { holder=25174406 } # Caster, son of Corlos
198.1.1 = { holder=26174406 } # Castella, daughter of Caster, bride of Lann the Clever

210.1.1 = { holder=80260190 } # Lanna Lannister # Line of Lannisters
250.1.1 = { holder=80270190 } # Loreon the Lion
280.1.1 = { holder=0 }

# Pre-Andal period
5320.1.1 = { holder=80310190 } # Gerold the Great (C)
5365.1.1 = { holder = 0 }

6448.1.1 = { holder=860370190 } # Tyson (NC)
6477.1.1 = { holder=850370190 } # Tymore (NC)
6496.1.1 = { holder=840370190 } # Tyland (NC)
6524.1.1 = { holder=830370190 } # Norbert (NC)
6543.1.1 = { holder=820370190 } # Luceon (NC)
6570.1.1 = { holder=810370190 } # Tywin (NC)

6609.1.1 = { holder=80370190 } # Tymeon I (nc)
6619.1.1 = { holder=80380190 } # Tybolt Tunderbolt (C)
6665.1.1 = { holder=80390190 } # Tyrion III (C)
6682.1.1 = { holder=80400190 } # Gerold II (C)
6695.1.1 = { holder=80401190 } # Gerold III (C)
6732.1.1 = { holder=80403190 } # Joffrey I Lydden (C)

