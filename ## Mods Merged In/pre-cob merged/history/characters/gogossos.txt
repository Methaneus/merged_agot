#### House Paenorhin
10003350 = {
	name="Benerro"	# a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	6448.1.1 = {birth="6448.1.1"}
	6464.1.1 = {add_trait=poor_warrior}
	6466.1.1 = {add_spouse=20003350}
	6502.1.1 = {death="6502.1.1"}
}
20003350 = {
	name="Talisa"
	female=yes

	religion="valyrian_rel"
	culture="eastern_valyrian"

	6448.1.1 = {birth="6448.1.1"}
	6502.1.1 = {death="6502.1.1"}
}
10013350 = {
	name="Qoherys"	# a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10003350
	mother=20003350

	6483.1.1 = {birth="6483.1.1"}
	6499.1.1 = {add_trait=poor_warrior}
	6501.1.1 = {add_spouse=20013350}
	6527.1.1 = {death="6527.1.1"}
}
20013350 = {
	name="Alearys"
	female=yes

	religion="valyrian_rel"
	culture="eastern_valyrian"

	6483.1.1 = {birth="6483.1.1"}
	6527.1.1 = {death="6527.1.1"}
}
10023350 = {
	name="Naerys"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10003350
	mother=20003350

	6485.1.1 = {birth="6485.1.1"}
	6536.1.1 = {death="6536.1.1"}
}
10033350 = {
	name="Vogarra"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10013350
	mother=20013350

	6503.1.1 = {birth="6503.1.1"}
	6572.1.1 = {death="6572.1.1"}
}
10043350 = {
	name="Donipha"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10013350
	mother=20013350

	6505.1.1 = {birth="6505.1.1"}
	6548.1.1 = {death="6548.1.1"}
}
10053350 = {
	name="Aenar"	# a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10013350
	mother=20013350

	6508.1.1 = {birth="6508.1.1"}
	6524.1.1 = {add_trait=trained_warrior}
	6526.1.1 = {add_spouse=20053350}
	6566.1.1 = {death="6566.1.1"}
}
20053350 = {
	name="Horonna"
	female=yes

	religion="valyrian_rel"
	culture="eastern_valyrian"

	6508.1.1 = {birth="6508.1.1"}
	6566.1.1 = {death="6566.1.1"}
}
10063350 = {
	name="Cyeana"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10013350
	mother=20013350

	6509.1.1 = {birth="6509.1.1"}
	6564.1.1 = {death="6564.1.1"}
}
10073350 = {
	name="Alios"	# a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10053350
	mother=20053350

	6533.1.1 = {birth="6533.1.1"}
	6549.1.1 = {add_trait=poor_warrior}
	6551.1.1 = {add_spouse=20073350}
	6600.1.1 = {death="6600.1.1"}
}
20073350 = {
	name="Alia"
	female=yes

	religion="valyrian_rel"
	culture="eastern_valyrian"

	6533.1.1 = {birth="6533.1.1"}
	6600.1.1 = {death="6600.1.1"}
}
10083350 = {
	name="Allyria"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10053350
	mother=20053350

	6536.1.1 = {birth="6536.1.1"}
	6579.1.1 = {death="6579.1.1"}
}
10093350 = {
	name="Horonno"	# a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10073350
	mother=20073350

	6555.1.1 = {birth="6555.1.1"}
	6571.1.1 = {add_trait=master_warrior}
	6573.1.1 = {add_spouse=20093350}
	6610.1.1 = {death="6610.1.1"}
}
20093350 = {
	name="Parquella"
	female=yes

	religion="valyrian_rel"
	culture="eastern_valyrian"

	6555.1.1 = {birth="6555.1.1"}
	6610.1.1 = {death="6610.1.1"}
}
10103350 = {
	name="Vaeron"	# a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10093350
	mother=20093350

	6584.1.1 = {birth="6584.1.1"}
	6600.1.1 = {add_trait=poor_warrior}
	6602.1.1 = {add_spouse=20103350}
	6661.1.1 = {death="6661.1.1"}
}
20103350 = {
	name="Donipha"
	female=yes

	religion="valyrian_rel"
	culture="eastern_valyrian"

	6584.1.1 = {birth="6584.1.1"}
	6661.1.1 = {death="6661.1.1"}
}
10113350 = {
	name="Leaysa"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10103350
	mother=20103350

	6606.1.1 = {birth="6606.1.1"}
	6653.1.1 = {death="6653.1.1"}
}
10123350 = {
	name="Nyessos"	# a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10103350
	mother=20103350

	6609.1.1 = {birth="6609.1.1"}
	6625.1.1 = {add_trait=poor_warrior}
	6627.1.1 = {add_spouse=20123350}
	6666.1.1 = {death="6666.1.1"}
}
20123350 = {
	name="Alearys"
	female=yes

	religion="valyrian_rel"
	culture="eastern_valyrian"

	6609.1.1 = {birth="6609.1.1"}
	6666.1.1 = {death="6666.1.1"}
}
10133350 = {
	name="Elaena"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10103350
	mother=20103350

	6611.1.1 = {birth="6611.1.1"}
	6669.1.1 = {death="6669.1.1"}
}
10143350 = {
	name="Trianna"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10123350
	mother=20123350

	6644.1.1 = {birth="6644.1.1"}
	6677.1.1 = {death="6677.1.1"}
}
10153350 = {
	name="Rhae"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10123350
	mother=20123350

	6646.1.1 = {birth="6646.1.1"}
	6716.1.1 = {death="6716.1.1"}
}
10163350 = {
	name="Syaella"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10123350
	mother=20123350

	6648.1.1 = {birth="6648.1.1"}
	6722.1.1 = {death="6722.1.1"}
}
10173350 = {
	name="Colloquo"	# a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10123350
	mother=20123350

	6651.1.1 = {birth="6651.1.1"}
	6667.1.1 = {add_trait=poor_warrior}
	6669.1.1 = {add_spouse=20173350}
	6715.1.1 = {death="6715.1.1"}
}
20173350 = {
	name="Syaella"
	female=yes

	religion="valyrian_rel"
	culture="eastern_valyrian"

	6651.1.1 = {birth="6651.1.1"}
	6715.1.1 = {death="6715.1.1"}
}
10183350 = {
	name="Orys"	# not a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10123350
	mother=20123350

	6654.1.1 = {birth="6654.1.1"}
	6670.1.1 = {add_trait=poor_warrior}
	6718.1.1 = {death="6718.1.1"}
}
10193350 = {
	name="Gorys"	# a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10173350
	mother=20173350

	6680.1.1 = {birth="6680.1.1"}
	6696.1.1 = {add_trait=skilled_warrior}
	6698.1.1 = {add_spouse=20193350}
	6727.1.1 = {death="6727.1.1"}
}
20193350 = {
	name="Saenrys"
	female=yes

	religion="valyrian_rel"
	culture="eastern_valyrian"

	6680.1.1 = {birth="6680.1.1"}
	6727.1.1 = {death="6727.1.1"}
}
10203350 = {
	name="Vaekar"	# a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10193350
	mother=20193350

	6701.1.1 = {birth="6701.1.1"}
	6717.1.1 = {add_trait=poor_warrior}
	6719.1.1 = {add_spouse=20203350}
	6774.1.1 = {death="6774.1.1"}
}
20203350 = {
	name="Moqorra"
	female=yes

	religion="valyrian_rel"
	culture="eastern_valyrian"

	6701.1.1 = {birth="6701.1.1"}
	6774.1.1 = {death="6774.1.1"}
}
10213350 = {
	name="Parquello"	# a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10203350
	mother=20203350

	6720.1.1 = {birth="6720.1.1"}
	6736.1.1 = {add_trait=poor_warrior}
	6738.1.1 = {add_spouse=20213350}
	6800.1.1 = {death="6800.1.1"}
}
20213350 = {
	name="Parquella"
	female=yes

	religion="valyrian_rel"
	culture="eastern_valyrian"

	6720.1.1 = {birth="6720.1.1"}
	6800.1.1 = {death="6800.1.1"}
}
10223350 = {
	name="Parquella"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10203350
	mother=20203350

	6723.1.1 = {birth="6723.1.1"}
	6772.1.1 = {death="6772.1.1"}
}
10233350 = {
	name="Qoherys"	# a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10213350
	mother=20213350

	6755.1.1 = {birth="6755.1.1"}
	6771.1.1 = {add_trait=poor_warrior}
	6773.1.1 = {add_spouse=20233350}
	6822.1.1 = {death="6822.1.1"}
}
20233350 = {
	name="Rhaeys"
	female=yes

	religion="valyrian_rel"
	culture="eastern_valyrian"

	6755.1.1 = {birth="6755.1.1"}
	6822.1.1 = {death="6822.1.1"}
}
10243350 = {
	name="Nyessa"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10233350
	mother=20233350

	6777.1.1 = {birth="6777.1.1"}
	6857.1.1 = {death="6857.1.1"}
}
10253350 = {
	name="Naerys"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10233350
	mother=20233350

	6778.1.1 = {birth="6778.1.1"}
	6822.1.1 = {death="6822.1.1"}
}
10263350 = {
	name="Trianna"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10233350
	mother=20233350

	6781.1.1 = {birth="6781.1.1"}
	6834.1.1 = {death="6834.1.1"}
}
10273350 = {
	name="Aenar"	# a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10233350
	mother=20233350

	6784.1.1 = {birth="6784.1.1"}
	6800.1.1 = {add_trait=poor_warrior}
	6860.1.1 = {death="6860.1.1"}
}
10283350 = {
	name="Haerys"	# not a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10233350
	mother=20233350

	6786.1.1 = {birth="6786.1.1"}
	6802.1.1 = {add_trait=skilled_warrior}
	6838.1.1 = {death="6838.1.1"}
}

115723354 = {
	name="Mero"	# not a lord
	dynasty=723354

	religion="valyrian_rel"
	culture="gogossosi"

	6362.1.1 = {birth="6362.1.1"}
	6378.1.1 = {add_trait=skilled_warrior}
}

