name = "Armistead Vance's Invasion of the Riverlands"

casus_belli={
	casus_belli=invasion
	actor=650153 	# Armistead Vance
	recipient=643174399
	landed_title=e_riverlands
	date=6650.1.1
}

6650.1.1 = {
	add_defender = 643174399
	add_attacker = 650153 	# Armistead Vance
}

6651.1.1 = {
	rem_defender = 643174399
	rem_attacker = 650153 	# Armistead Vance
}                                                                                                                                                                                                                                                                                                                