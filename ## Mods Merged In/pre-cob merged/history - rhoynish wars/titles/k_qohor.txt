7065.1.1 = {
	holder=100300567 # Daario (nc)
	law = slavery_1
	law = centralization_2
	effect = {
		holder_scope = { 
			e_new_valyria = {
				holder_scope = {
					make_tributary = { who = PREVPREV tributary_type = valyrian_tributary }
				}
			}
		}	
	}
}
7107.1.1 = { holder=100300564 } # Meralyn (nc)
7108.1.1 = { holder=101300564 } # Tagganaro (nc)
7146.1.1 = { holder=101300520 } # Beqqo (nc)
7149.1.1 = { holder=105300520 } # Luco (nc)
7178.1.1 = { holder=105300564 } # Collio (nc)
7182.1.1 = { holder=108300564 } # Tagganaro (nc)
7204.1.1 = { holder=109300520 } # Byan (nc)
7210.1.1 = { holder=113300529 } # Ternesio (nc)
7233.1.1 = { holder=117300520 } # Beqqo (nc)
7253.1.1 = { holder=119300520 } # Moreo (nc)
7264.1.1 = { holder=116300529 } # Vargo (nc)
7274.1.1 = { holder=124300565 } # Ordello (nc)
7285.1.1 = { holder=129300565 } # Moredo (nc)
7318.1.1 = { holder=131300565 } # Tobho (nc)
7324.1.1 = { holder=125300520 } # Joss (nc)
7327.1.1 = { holder=128300520 } # Myrmello (nc)
7330.1.1 = { holder=130300520 } # Brusco (nc)
7341.1.1 = { holder=132300520 } # Collio (nc)
7375.1.1 = { holder=133300564 } # Tagganaro (nc)
7394.1.1 = { holder=134300564 } # Groleo (nc)
