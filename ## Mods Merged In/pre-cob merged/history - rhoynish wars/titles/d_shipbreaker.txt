642.1.1 = {
	holder = 41544 #Durran Godsgrief
	liege="e_stormlands"
	law = succ_primogeniture
}
712.1.1 = { holder = 1501544 } #Durran II Durrandon
744.1.1 = { holder = 1511544 } #Durran III Durrandon (NC)
771.1.1 = { holder = 1521544 } #Durran IV Durrandon
784.1.1 = { holder = 1531544 } #Durran V Durrandon
811.1.1 = { holder = 1541544 } #Durran VI Durrandon
841.1.1 = { holder = 1551544 } #Erich I Durrandon (NC)
852.1.1 = { holder = 1561544 } #Durran VII Durrandon
904.1.1 = { holder = 0 }

3672.1.1 = { holder = 1591544 } #Durran VIII Durrandon
3700.1.1 = { holder = 1601544 } #Erich II Durrandon (NC)
3712.1.1 = { holder = 1611544 } #Erich III Durrandon
3748.1.1 = { holder = 1621544 } #Maldon I Durrandon (NC)
3767.1.1 = { holder = 1631544 } #Maldon II Durrandon (NC)
3778.1.1 = { holder = 1641544 } #Ormund I Durrandon (NC)
3820.1.1 = { holder = 1651544 } #Durran IX Durrandon (NC)
3827.1.1 = { holder = 1661544 } #Morden I Durrandon (NC)
3860.1.1 = { holder = 1671544 } #Erich IV Durrandon (NC)
3871.1.1 = { holder = 1681544 } #Durran X Durrandon
3897.1.1 = { holder = 1691544 } #Monfryd I Durrandon
3921.1.1 = { holder = 1701544 } #Durran XI Durrandon
3953.1.1 = { holder = 1711544 } #Barron  Durrandon
3987.1.1 = { holder = 1721544 } #Monfryd II Durrandon (NC)
4002.1.1 = { holder = 1731544 } #Durran XII Durrandon (NC)
4013.1.1 = { holder = 1741544 } #Erich V Durrandon (NC)
4041.1.1 = { holder = 1751544 } #Qarlton I Durrandon (NC)
4073.1.1 = { holder = 1761544 } #Durran XIII Durrandon (NC)
4085.1.1 = { holder = 1771544 } #Durran XIV Durrandon (NC)
4097.1.1 = { holder = 1781544 } #Durwald I Durrandon
4156.1.1 = { holder = 1791544 } #Monfryd III Durrandon (NC)
4171.1.1 = { holder = 1801544 } #Erich VI Durrandon (NC)
4190.1.1 = { holder = 1811544 } #Durran XV Durrandon (NC)
4210.1.1 = { holder = 1821544 } #Durran XVI Durrandon (NC)
4225.1.1 = { holder = 1831544 } #Morden II Durrandon
4230.1.1 = { holder = 1841544 } #Ronard  Durrandon
4260.1.1 = { holder = 0 }

6961.1.1 = { holder = 1861544 } #Maldon III Durrandon (NC)
6985.1.1 = { holder = 1871544 } #Ormund II Durrandon (NC)
7015.1.1 = { holder = 1881544 } #Durran XVII Durrandon (NC)
7045.1.1 = { holder = 1891544 } #Durran XVIII Durrandon (NC)
7067.1.1 = { holder = 1901544 } #Monfryd IV Durrandon (NC)
7090.1.1 = { holder = 1911544 } #Erich VII Durrandon
7123.1.1 = { holder = 1921544 } #Durran IXX Durrandon (NC)
7137.1.1 = { holder = 1931544 } #Qarlton II Durrandon
7154.1.1 = { holder = 1941544 } #Qarlton III Durrandon
7177.1.1 = { holder = 1951544 } #Monfryd V Durrandon
7189.1.1 = { holder = 1961544 } #Durran XX Durrandon (NC)
7204.1.1 = { holder = 1971544 } #Baldric I Durrandon
7239.1.1 = { holder = 1981544 } #Durran XXI Durrandon
7254.1.1 = { holder = 1991544 } #Cleoden I Durrandon
7269.1.1 = { holder = 2021544 } #Maldon IV Durrandon
7300.1.1 = { holder = 2031544 } #Durran XXIV Durrandon
7312.1.1 = { holder = 2041544 } #Ormund III Durrandon
