6400.1.1 = {
	law = slavery_2
	law = centralization_2
	law = first_night_0
	effect = {
		holder_scope = { 
			e_new_valyria = {
				holder_scope = {
					make_tributary = { who = PREVPREV tributary_type = valyrian_tributary }
				}
			}
		}	
	}
}

7096.12.16 = { holder=100174373 } # Trianna (C) 
7101.1.1 = { holder=100174372 } 
7110.1.1 = { holder=101174373 } # Haerys (nc)
7114.1.1 = { holder=100174372 } 
7125.1.1 = { holder=102174372 } 
7149.1.1 = { holder=105174372 } 
7176.1.1 = { holder=111174371 } # Belicho (nc)
7186.1.1 = { holder=115174371 } # Benerro (nc)
7209.1.1 = { holder=111174373 } # Colloquo (nc)
7214.1.1 = { holder=115174373 } # Vaeron (nc)
7238.1.1 = { holder=113174396 } # Methyso (nc)
7248.1.1 = { holder=115174396 } # Gorys (nc)
7267.1.1 = { holder=119174397 } # Methyso (nc)
7270.1.1 = { holder=125174397 } # Parquello (nc)
7303.1.1 = { holder=127174396 } # Matarys (nc)
7350.1.1 = { holder=131174396 } # Benerro (nc)
7356.1.1 = { holder=133174397 } # Qavo (nc)
7360.1.1 = { holder=139174397 } # Vogarro (C)
7366.1.1 = { holder=141174397 } # Qavo (nc)
7372.1.1 = { holder=137174372 } 
7376.1.1 = { holder=147174371 } # Malaquo
