1.1.1 = { 
	holder=750066  # The Grey King
	law = succ_kingsmoot_elective
	law = agnatic_succession
	law = centralization_1
	law = slavery_3
	law = first_night_1
} 
1001.1.1 = { holder = 0 }

2917.1.1 = { holder = 500174420 } #Urras  Greyiron
2942.1.1 = { holder = 501174420 } #Erich I Greyiron
2943.1.1 = { holder = 5011121 } #Regnar  Drumm
2956.1.1 = { holder = 502174420 } #Theon I Greyiron (NC)
2993.1.1 = { holder = 354066 } #Sylas  
3009.1.1 = { holder = 503174420 } #Balon I Greyiron (NC)
3034.1.1 = { holder = 0 }

3487.1.1 = { holder = 355066 } #Harrag  Hoare
3519.1.1 = { holder = 357066 } #Erich II Hoare
3545.1.1 = { holder = 504174420 } #Urrathon I Greyiron (NC)
3556.1.1 = { holder = 505174420 } #Urrathon II Greyiron (NC)
3578.1.1 = { holder = 500107 } #Loron  Greyjoy
3632.1.1 = { holder = 501107 } #Theon II Greyjoy (NC)
3638.1.1 = { holder = 506174420 } #Urragon I Greyiron (NC)
3658.1.1 = { holder = 502107 } #Balon II Greyjoy (NC)
3670.1.1 = { holder = 0 }

4159.1.1 = { holder = 507174420 } #Balon III Greyiron (NC)
4177.1.1 = { holder = 508174420 } #Erich III Greyiron (NC)
4216.1.1 = { holder = 509174420 } #Rognar I Greyiron (NC)
4230.1.1 = { holder = 503107 } #Theon III Greyjoy
4238.1.1 = { holder = 510174420 } #Erich IV Greyiron (NC)
4278.1.1 = { holder = 511174420 } #Urrathon III Greyiron (NC)
4287.1.1 = { holder = 504107 } #Balon IV Greyjoy (NC)
4303.1.1 = { holder = 505107 } #Balon V Greyjoy
4337.1.1 = { holder = 500122 } #Erich V Harlaw
4376.1.1 = { holder = 501122 } #Harron  Harlaw
4393.1.1 = {
	holder = 500118 # Joron Blacktyde
}
4449.1.1 = {
	holder = 0
}

4978.1.1 = { holder = 506107 } #Balon VI Greyjoy (NC)
5014.1.1 = { holder = 524174420 } #Urragon II Greyiron (NC)
5043.1.1 = { holder = 500118 } #Joron I Blacktyde
5059.1.1 = { holder = 512174420 } #Urragon III Greyiron
5087.1.1 = { holder = 550112 } #Urrathon IV Goodbrother
5089.1.1 = { holder = 513174420 } #Torgon  Greyiron
5129.1.1 = { holder = 517174420 } #Urragon IV Greyiron

5181.1.1 = { 
	holder = 520174420  #Urron  Greyiron
	law = succ_primogeniture
} 
5203.1.1 = { holder = 0 }

5690.1.1 = { holder = 525174420 } #Joron II Greyiron (NC)
6521.1.1 = { holder = 521174420 } #Euron I Greyiron (NC)
6540.1.1 = { holder = 522174420 } #Balon VII Greyiron (NC)
6563.1.1 = { holder = 523174420 } #Rognar II Greyiron

6584.1.1 = { holder = 359066 } #Harras  Hoare
6605.1.1 = { holder = 360066 } #Wulfgar  Hoare
6639.1.1 = { holder = 361066 } #Harrag Hoare (NC)
6648.1.1 = { holder = 362066 } #Euron II Hoare (NC)
6671.1.1 = { holder = 363066 } #Horgan  Hoare
6692.1.1 = { holder = 3163066 } #Balon VIII Hoare (NC)

6734.1.1 = { holder = 0 }



7067.1.1 = { holder=100066 } # Quellon (nc)
7082.1.1 = { holder = 358066 } #Qhored  Hoare
7157.1.1 = { holder=100566 } # Quellon (nc)
7175.1.1 = { holder=100766 } # Urrigon (nc)
7199.1.1 = { holder=101066 } # Drennan (nc)
7228.1.1 = { holder=101366 } # Urek (nc)
7263.1.1 = { holder=101466 } # Emmond (nc)
7283.1.1 = { holder=101866 } # Cragorn (nc)
7298.1.1 = { holder=102166 } # Dagon (nc)
7323.1.1 = { holder=102366 } # Gran (nc)
7342.1.1 = { holder=102666 } # Qhored (nc)
7370.1.1 = { holder=103066 } # Harwyn (nc)
7386.1.1 = { holder=103466 } # Emmond (nc)
7431.1.1 = { holder=103566 } # Aggar (nc)