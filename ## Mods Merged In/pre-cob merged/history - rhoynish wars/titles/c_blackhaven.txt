#NOTE: The holder of this title has the House Dondarrion dynasty tracker set in roberts_rebellion_events
1.1.1 = { liege="e_stormlands" }

7094.1.1 = { holder = 300303 } # Alaric Dondarrion (nc)

7106.1.1 = { holder = 301303 } # Donnel Dondarrion (nc)
7140.1.1 = { holder = 302303 } # Harbert Dondarrion (nc)
7169.1.1 = { holder = 303303 } # Galladon Dondarrion (nc)
7186.1.1 = { holder = 304303 } # Maric Dondarrion (nc)
7207.1.1 = { holder = 306303 } # Rickard Dondarrion (nc)
7260.1.1 = { holder = 307303 } # Devan Dondarrion (nc)
7276.1.1 = { holder = 200303 } # Lyonel Dondarrion (C)
7312.1.1 = { holder = 201303 } # Manfred Dondarrion (C)
7341.1.1 = { holder = 310303 } # Gulian Dondarrion (nc)
7371.1.1 = { holder = 303 } # Edric Dondarrion (nc)
