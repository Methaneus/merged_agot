#### House Paenorhin
10003350 = {
	name="Benerro"	# a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	7070.1.1 = {birth="7070.1.1"}
	7086.1.1 = {add_trait=poor_warrior}
	7088.1.1 = {add_spouse=20003350}
	7124.1.1 = {death="7124.1.1"}
}
20003350 = {
	name="Talisa"
	female=yes

	religion="valyrian_rel"
	culture="eastern_valyrian"

	7070.1.1 = {birth="7070.1.1"}
	7124.1.1 = {death="7124.1.1"}
}
10013350 = {
	name="Qoherys"	# a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10003350
	mother=20003350

	7105.1.1 = {birth="7105.1.1"}
	7121.1.1 = {add_trait=poor_warrior}
	7123.1.1 = {add_spouse=20013350}
	7149.1.1 = {death="7149.1.1"}
}
20013350 = {
	name="Alearys"
	female=yes

	religion="valyrian_rel"
	culture="eastern_valyrian"

	7105.1.1 = {birth="7105.1.1"}
	7149.1.1 = {death="7149.1.1"}
}
10023350 = {
	name="Naerys"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10003350
	mother=20003350

	7107.1.1 = {birth="7107.1.1"}
	7158.1.1 = {death="7158.1.1"}
}
10033350 = {
	name="Vogarra"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10013350
	mother=20013350

	7125.1.1 = {birth="7125.1.1"}
	7194.1.1 = {death="7194.1.1"}
}
10043350 = {
	name="Donipha"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10013350
	mother=20013350

	7127.1.1 = {birth="7127.1.1"}
	7170.1.1 = {death="7170.1.1"}
}
10053350 = {
	name="Aenar"	# a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10013350
	mother=20013350

	7130.1.1 = {birth="7130.1.1"}
	7146.1.1 = {add_trait=trained_warrior}
	7148.1.1 = {add_spouse=20053350}
	7188.1.1 = {death="7188.1.1"}
}
20053350 = {
	name="Horonna"
	female=yes

	religion="valyrian_rel"
	culture="eastern_valyrian"

	7130.1.1 = {birth="7130.1.1"}
	7188.1.1 = {death="7188.1.1"}
}
10063350 = {
	name="Cyeana"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10013350
	mother=20013350

	7131.1.1 = {birth="7131.1.1"}
	7186.1.1 = {death="7186.1.1"}
}
10073350 = {
	name="Alios"	# a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10053350
	mother=20053350

	7155.1.1 = {birth="7155.1.1"}
	7171.1.1 = {add_trait=poor_warrior}
	7173.1.1 = {add_spouse=20073350}
	7222.1.1 = {death="7222.1.1"}
}
20073350 = {
	name="Alia"
	female=yes

	religion="valyrian_rel"
	culture="eastern_valyrian"

	7155.1.1 = {birth="7155.1.1"}
	7222.1.1 = {death="7222.1.1"}
}
10083350 = {
	name="Allyria"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10053350
	mother=20053350

	7158.1.1 = {birth="7158.1.1"}
	7201.1.1 = {death="7201.1.1"}
}
10093350 = {
	name="Horonno"	# a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10073350
	mother=20073350

	7177.1.1 = {birth="7177.1.1"}
	7193.1.1 = {add_trait=master_warrior}
	7195.1.1 = {add_spouse=20093350}
	7232.1.1 = {death="7232.1.1"}
}
20093350 = {
	name="Parquella"
	female=yes

	religion="valyrian_rel"
	culture="eastern_valyrian"

	7177.1.1 = {birth="7177.1.1"}
	7232.1.1 = {death="7232.1.1"}
}
10103350 = {
	name="Vaeron"	# a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10093350
	mother=20093350

	7206.1.1 = {birth="7206.1.1"}
	7222.1.1 = {add_trait=poor_warrior}
	7224.1.1 = {add_spouse=20103350}
	7283.1.1 = {death="7283.1.1"}
}
20103350 = {
	name="Donipha"
	female=yes

	religion="valyrian_rel"
	culture="eastern_valyrian"

	7206.1.1 = {birth="7206.1.1"}
	7283.1.1 = {death="7283.1.1"}
}
10113350 = {
	name="Leaysa"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10103350
	mother=20103350

	7228.1.1 = {birth="7228.1.1"}
	7275.1.1 = {death="7275.1.1"}
}
10123350 = {
	name="Nyessos"	# a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10103350
	mother=20103350

	7231.1.1 = {birth="7231.1.1"}
	7247.1.1 = {add_trait=poor_warrior}
	7249.1.1 = {add_spouse=20123350}
	7288.1.1 = {death="7288.1.1"}
}
20123350 = {
	name="Alearys"
	female=yes

	religion="valyrian_rel"
	culture="eastern_valyrian"

	7231.1.1 = {birth="7231.1.1"}
	7288.1.1 = {death="7288.1.1"}
}
10133350 = {
	name="Elaena"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10103350
	mother=20103350

	7233.1.1 = {birth="7233.1.1"}
	7291.1.1 = {death="7291.1.1"}
}
10143350 = {
	name="Trianna"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10123350
	mother=20123350

	7266.1.1 = {birth="7266.1.1"}
	7299.1.1 = {death="7299.1.1"}
}
10153350 = {
	name="Rhae"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10123350
	mother=20123350

	7268.1.1 = {birth="7268.1.1"}
	7338.1.1 = {death="7338.1.1"}
}
10163350 = {
	name="Syaella"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10123350
	mother=20123350

	7270.1.1 = {birth="7270.1.1"}
	7344.1.1 = {death="7344.1.1"}
}
10173350 = {
	name="Colloquo"	# a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10123350
	mother=20123350

	7273.1.1 = {birth="7273.1.1"}
	7289.1.1 = {add_trait=poor_warrior}
	7291.1.1 = {add_spouse=20173350}
	7337.1.1 = {death="7337.1.1"}
}
20173350 = {
	name="Syaella"
	female=yes

	religion="valyrian_rel"
	culture="eastern_valyrian"

	7273.1.1 = {birth="7273.1.1"}
	7337.1.1 = {death="7337.1.1"}
}
10183350 = {
	name="Orys"	# not a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10123350
	mother=20123350

	7276.1.1 = {birth="7276.1.1"}
	7292.1.1 = {add_trait=poor_warrior}
	7340.1.1 = {death="7340.1.1"}
}
10193350 = {
	name="Gorys"	# a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10173350
	mother=20173350

	7302.1.1 = {birth="7302.1.1"}
	7318.1.1 = {add_trait=skilled_warrior}
	7320.1.1 = {add_spouse=20193350}
	7349.1.1 = {death="7349.1.1"}
}
20193350 = {
	name="Saenrys"
	female=yes

	religion="valyrian_rel"
	culture="eastern_valyrian"

	7302.1.1 = {birth="7302.1.1"}
	7349.1.1 = {death="7349.1.1"}
}
10203350 = {
	name="Vaekar"	# a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10193350
	mother=20193350

	7323.1.1 = {birth="7323.1.1"}
	7339.1.1 = {add_trait=poor_warrior}
	7341.1.1 = {add_spouse=20203350}
	7396.1.1 = {death="7396.1.1"}
}
20203350 = {
	name="Moqorra"
	female=yes

	religion="valyrian_rel"
	culture="eastern_valyrian"

	7323.1.1 = {birth="7323.1.1"}
	7396.1.1 = {death="7396.1.1"}
}
10213350 = {
	name="Parquello"	# a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10203350
	mother=20203350

	7342.1.1 = {birth="7342.1.1"}
	7358.1.1 = {add_trait=poor_warrior}
	7360.1.1 = {add_spouse=20213350}
	7422.1.1 = {death="7422.1.1"}
}
20213350 = {
	name="Parquella"
	female=yes

	religion="valyrian_rel"
	culture="eastern_valyrian"

	7342.1.1 = {birth="7342.1.1"}
	7422.1.1 = {death="7422.1.1"}
}
10223350 = {
	name="Parquella"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10203350
	mother=20203350

	7345.1.1 = {birth="7345.1.1"}
	7394.1.1 = {death="7394.1.1"}
}
10233350 = {
	name="Qoherys"	# a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10213350
	mother=20213350

	7377.1.1 = {birth="7377.1.1"}
	7393.1.1 = {add_trait=poor_warrior}
	7395.1.1 = {add_spouse=20233350}
	7444.1.1 = {death="7444.1.1"}
}
20233350 = {
	name="Rhaeys"
	female=yes

	religion="valyrian_rel"
	culture="eastern_valyrian"

	7377.1.1 = {birth="7377.1.1"}
	7444.1.1 = {death="7444.1.1"}
}
10243350 = {
	name="Nyessa"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10233350
	mother=20233350

	7399.1.1 = {birth="7399.1.1"}
	7479.1.1 = {death="7479.1.1"}
}
10253350 = {
	name="Naerys"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10233350
	mother=20233350

	7400.1.1 = {birth="7400.1.1"}
	7444.1.1 = {death="7444.1.1"}
}
10263350 = {
	name="Trianna"	# not a lord
	female=yes
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10233350
	mother=20233350

	7403.1.1 = {birth="7403.1.1"}
	7456.1.1 = {death="7456.1.1"}
}
10273350 = {
	name="Aenar"	# a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10233350
	mother=20233350

	7406.1.1 = {birth="7406.1.1"}
	7422.1.1 = {add_trait=poor_warrior}
	7482.1.1 = {death="7482.1.1"}
}
10283350 = {
	name="Haerys"	# not a lord
	dynasty=723350

	religion="valyrian_rel"
	culture="eastern_valyrian"

	father=10233350
	mother=20233350

	7408.1.1 = {birth="7408.1.1"}
	7424.1.1 = {add_trait=skilled_warrior}
	7460.1.1 = {death="7460.1.1"}
}

