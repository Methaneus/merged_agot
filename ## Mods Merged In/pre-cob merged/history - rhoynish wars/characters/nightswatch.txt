
########
#####Historic Lord Commanders#####
########
121400 = {
	name="Tristan"	# Canon, one of the worst Lord Commander's in history
	dynasty=174399 # Mudd
	
	martial = 12
	stewardship = 12
	
	religion="old_gods"
	culture="old_first_man"
	
	father=617174399
	
	
	6173.1.1 = {birth="6173.1.1"}
	6195.1.1 = { add_trait=nightswatch}
	6218.1.1 = {death="6218.1.1"}
}
121406 = {
	name="Marq" #Canon, one of the worst Lord Commanders.	
	dynasty=174415 #Rankenfell
	
	martial = 12
	stewardship = 12
	
	religion="old_gods"
	culture="old_first_man"
		
	6324.1.1 = {birth="6324.1.1"}
	6340.1.1 = {add_trait=skilled_warrior add_trait=tough_soldier}
	6361.1.1 = { add_trait=nightswatch}
	6377.1.1 = { give_nickname = nick_the_mad}
	6378.1.1 = {death="6378.1.1"}
}
121404 = {
	name="Robin"	
	
	martial = 12
	stewardship = 12
	
	religion="old_gods"
	culture="old_first_man"

	add_trait="bastard"
	give_nickname = nick_hill
		
	6384.1.1 = {birth="6384.1.1"}
	6400.1.1 = {add_trait=skilled_warrior add_trait=tough_soldier}
	6405.6.1={ add_trait=nightswatch}
	6428.1.1 = {death="6428.1.1"}
}



1000868 = {
	name="Bass"	# a lord
	

	religion="old_gods"
	culture="northman"

	7058.1.1 = {birth="7058.1.1"}
	7074.1.1 = {add_trait=nightswatch add_trait=poor_warrior}
	7115.1.1 = {death="7115.1.1"}
}
1001868 = {
	name="Brenett"	# a lord
	

	religion="old_gods"
	culture="northman"

	7076.1.1 = {birth="7076.1.1"}
	7092.1.1 = {add_trait=nightswatch add_trait=poor_warrior}
	7128.1.1 = {death="7128.1.1"}
}
1002868 = {
	name="Dywen"	# a lord
	

	religion="old_gods"
	culture="northman"

	7088.1.1 = {birth="7088.1.1"}
	7104.1.1 = {add_trait=nightswatch add_trait=poor_warrior}
	7135.1.1 = {death="7135.1.1"}
}
1003868 = {
	name="Alaric"	# a lord
	

	religion="old_gods"
	culture="northman"

	7105.1.1 = {birth="7105.1.1"}
	7121.1.1 = {add_trait=nightswatch add_trait=poor_warrior}
	7149.1.1 = {death="7149.1.1"}
}
1004868 = {
	name="Porther"	# a lord
	

	religion="old_gods"
	culture="northman"

	7116.1.1 = {birth="7116.1.1"}
	7132.1.1 = {add_trait=nightswatch add_trait=poor_warrior}
	7162.1.1 = {death="7162.1.1"}
}
1005868 = {
	name="Rickon"	# a lord
	

	religion="old_gods"
	culture="northman"

	7137.1.1 = {birth="7137.1.1"}
	7153.1.1 = {add_trait=nightswatch add_trait=poor_warrior}
	7181.1.1 = {death="7181.1.1"}
}
1006868 = {
	name="Hareth"	# a lord
	

	religion="old_gods"
	culture="northman"

	7159.1.1 = {birth="7159.1.1"}
	7175.1.1 = {add_trait=nightswatch add_trait=poor_warrior}
	7208.1.1 = {death="7208.1.1"}
}
1007868 = {
	name="Qhorin"	# a lord
	

	religion="old_gods"
	culture="northman"

	7181.1.1 = {birth="7181.1.1"}
	7197.1.1 = {add_trait=nightswatch add_trait=poor_warrior}
	7237.1.1 = {death="7237.1.1"}
}
1008868 = {
	name="Harmond"	# a lord
	

	religion="old_gods"
	culture="northman"

	7203.1.1 = {birth="7203.1.1"}
	7219.1.1 = {add_trait=nightswatch add_trait=poor_warrior}
	7261.1.1 = {death="7261.1.1"}
}
1009868 = {
	name="Jory"	# a lord
	

	religion="old_gods"
	culture="northman"

	7233.1.1 = {birth="7233.1.1"}
	7249.1.1 = {add_trait=nightswatch add_trait=poor_warrior}
	7292.1.1 = {death="7292.1.1"}
}
1010868 = {
	name="Benfred"	# a lord
	

	religion="old_gods"
	culture="northman"

	7274.1.1 = {birth="7274.1.1"}
	7290.1.1 = {add_trait=nightswatch add_trait=poor_warrior}
	7317.1.1 = {death="7317.1.1"}
}
1011868 = {
	name="Duncan"	# a lord
	

	religion="old_gods"
	culture="northman"

	7289.1.1 = {birth="7289.1.1"}
	7305.1.1 = {add_trait=nightswatch add_trait=poor_warrior}
	7345.1.1 = {death="7345.1.1"}
}
1012868 = {
	name="Theodan"	# a lord
	

	religion="old_gods"
	culture="northman"

	7309.1.1 = {birth="7309.1.1"}
	7325.1.1 = {add_trait=nightswatch add_trait=poor_warrior}
	7358.1.1 = {death="7358.1.1"}
}
1013868 = {
	name="Kyle"	# a lord
	

	religion="old_gods"
	culture="northman"

	7312.1.1 = {birth="7312.1.1"}
	7328.1.1 = {add_trait=nightswatch add_trait=poor_warrior}
	7376.1.1 = {death="7376.1.1"}
}
1014868 = {
	name="Mallador"	# a lord
	

	religion="old_gods"
	culture="northman"

	7343.1.1 = {birth="7343.1.1"}
	7359.1.1 = {add_trait=nightswatch add_trait=poor_warrior}
	7394.1.1 = {death="7394.1.1"}
}
1015868 = {
	name="Benjen"	# a lord
	

	religion="old_gods"
	culture="northman"

	7377.1.1 = {birth="7377.1.1"}
	7393.1.1 = {add_trait=nightswatch add_trait=poor_warrior}
	7412.1.1 = {death="7412.1.1"}
}
