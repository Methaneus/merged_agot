
#######################################   House Tyrell  ##############################################################

61000253 = {
	name="Alester" # First Tyrell. Founder.
	dynasty=253

	religion="the_seven"
	culture="old_andal"
	
	stewardship=8
	
	add_trait=trained_warrior
	add_trait=fortune_builder
	add_trait=knight
	add_trait=just
	add_trait=charitable

	6590.1.1 = {birth="6590.1.1"}
	6655.1.1 = {death="6655.1.1"}
}
62000253 = {
	name="Willas" # First son, Knight who died in a tourney.
	dynasty=253

	religion="the_seven"
	culture="old_andal"
	
	father=61000253
	
	add_trait=skilled_warrior
	add_trait=knight
	add_trait=just
	add_trait=charitable

	6612.1.1 = {birth="6612.1.1"}
	6631.1.1 = {death= {death_reason = death_accident}}
}
63000253 = {
	name="Gareth" # Second son, continued Tyrell line.
	dynasty=253

	religion="the_seven"
	culture="old_andal"
	
	stewardship=8
	
	father=61000253
	
	add_trait=poor_warrior
	add_trait=just
	add_trait=charitable
	add_trait=midas_touched
	add_trait=scholar
	add_trait=gregarious
	add_trait=humble
	add_trait=content

	6614.1.1 = {birth="6614.1.1"}
	6682.1.1 = {death="6682.1.1"}
}
64000253 = {
	name="Leo" #
	dynasty=253

	religion="the_seven"
	culture="old_andal"
	
	father=63000253
	
	6640.1.1 = {birth="6640.1.1"}
	6700.1.1 = {death="6700.1.1"}
}
65000253 = {
	name="Line of Leo" #
	dynasty=253

	religion="the_seven"
	culture="old_andal"
	
	father=64000253
	
	occluded=yes

	6680.1.1 = {birth="6680.1.1"}
	10750.1.1 = {death="10750.1.1"}
}
