#############   House zo Pazak

1000208 = {
	name="Grazdhar"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	7074.1.1 = {birth="7074.1.1"}
	7090.1.1 = {add_trait=poor_warrior}
	7092.1.1 = {add_spouse=4000208}
	7130.1.1 = {death="7130.1.1"}
}
4000208 = {
	name="Yezza"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7074.1.1 = {birth="7074.1.1"}
	7130.1.1 = {death="7130.1.1"}
}
1001208 = {
	name="Yarkhaz"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1000208
	mother=4000208

	7107.1.1 = {birth="7107.1.1"}
	7123.1.1 = {add_trait=trained_warrior}
	7125.1.1 = {add_spouse=4001208}
	7182.1.1 = {death="7182.1.1"}
}
4001208 = {
	name="Yezza"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7107.1.1 = {birth="7107.1.1"}
	7182.1.1 = {death="7182.1.1"}
}
1002208 = {
	name="Morghaz"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1001208
	mother=4001208

	7132.1.1 = {birth="7132.1.1"}
	7148.1.1 = {add_trait=skilled_warrior}
	7150.1.1 = {add_spouse=4002208}
	7209.1.1 = {death = {death_reason = death_accident}}
}
4002208 = {
	name="Gezzella"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7132.1.1 = {birth="7132.1.1"}
	7209.1.1 = {death="7209.1.1"}
}
1003208 = {
	name="Maezon"	# not a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1001208
	mother=4001208

	7133.1.1 = {birth="7133.1.1"}
	7149.1.1 = {add_trait=trained_warrior}
	7207.1.1 = {death="7207.1.1"}
}
1004208 = {
	name="Zellazara"	# not a lord
	female=yes
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1002208
	mother=4002208

	7163.1.1 = {birth="7163.1.1"}
	7207.1.1 = {death="7207.1.1"}
}
1005208 = {
	name="Reznak"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1002208
	mother=4002208

	7166.1.1 = {birth="7166.1.1"}
	7182.1.1 = {add_trait=poor_warrior}
	7184.1.1 = {add_spouse=4005208}
	7229.1.1 = {death="7229.1.1"}
}
4005208 = {
	name="Zellazara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7166.1.1 = {birth="7166.1.1"}
	7229.1.1 = {death="7229.1.1"}
}
1006208 = {
	name="Zaraq"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1005208
	mother=4005208

	7192.1.1 = {birth="7192.1.1"}
	7208.1.1 = {add_trait=skilled_warrior}
	7210.1.1 = {add_spouse=4006208}
	7255.1.1 = {death="7255.1.1"}
}
4006208 = {
	name="Qezza"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7192.1.1 = {birth="7192.1.1"}
	7255.1.1 = {death="7255.1.1"}
}
1007208 = {
	name="Hazzea"	# not a lord
	female=yes
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1005208
	mother=4005208

	7195.1.1 = {birth="7195.1.1"}
	7237.1.1 = {death="7237.1.1"}
}
1008208 = {
	name="Ghazdor"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1006208
	mother=4006208

	7224.1.1 = {birth="7224.1.1"}
	7240.1.1 = {add_trait=skilled_warrior}
	7242.1.1 = {add_spouse=4008208}
	7293.1.1 = {death="7293.1.1"}
}
4008208 = {
	name="Selezzqa"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7224.1.1 = {birth="7224.1.1"}
	7293.1.1 = {death="7293.1.1"}
}
1009208 = {
	name="Grazdhar"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1008208
	mother=4008208

	7249.1.1 = {birth="7249.1.1"}
	7265.1.1 = {add_trait=poor_warrior}
	7267.1.1 = {add_spouse=4009208}
	7324.1.1 = {death="7324.1.1"}
}
4009208 = {
	name="Mezzara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7249.1.1 = {birth="7249.1.1"}
	7324.1.1 = {death="7324.1.1"}
}
1010208 = {
	name="Mazdhan"	# not a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1008208
	mother=4008208

	7252.1.1 = {birth="7252.1.1"}
	7268.1.1 = {add_trait=skilled_warrior}
	7290.1.1 = {death="7290.1.1"}
}
1011208 = {
	name="Ellezzara"	# not a lord
	female=yes
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1009208
	mother=4009208

	7282.1.1 = {birth="7282.1.1"}
	7319.1.1 = {death="7319.1.1"}
}
1012208 = {
	name="Fendahl"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1009208
	mother=4009208

	7283.1.1 = {birth="7283.1.1"}
	7299.1.1 = {add_trait=poor_warrior}
	7301.1.1 = {add_spouse=4012208}
	7338.1.1 = {death="7338.1.1"}
}
4012208 = {
	name="Qezza"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7283.1.1 = {birth="7283.1.1"}
	7338.1.1 = {death="7338.1.1"}
}
1013208 = {
	name="Yarkhaz"	# not a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1009208
	mother=4009208

	7284.1.1 = {birth="7284.1.1"}
	7300.1.1 = {add_trait=trained_warrior}
	7326.1.1 = {death="7326.1.1"}
}
1014208 = {
	name="Marghaz"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1012208
	mother=4012208

	7307.1.1 = {birth="7307.1.1"}
	7323.1.1 = {add_trait=poor_warrior}
	7325.1.1 = {add_spouse=4014208}
	7353.1.1 = {death="7353.1.1"}
}
4014208 = {
	name="Hazzea"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7307.1.1 = {birth="7307.1.1"}
	7353.1.1 = {death="7353.1.1"}
}
1015208 = {
	name="Reznak"	# not a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1012208
	mother=4012208

	7308.1.1 = {birth="7308.1.1"}
	7324.1.1 = {add_trait=poor_warrior}
	7379.1.1 = {death="7379.1.1"}
}
1016208 = {
	name="Dhazzar"	# not a lord
	female=yes
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1012208
	mother=4012208

	7309.1.1 = {birth="7309.1.1"}
	7374.1.1 = {death="7374.1.1"}
}
1017208 = {
	name="Haznak"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1014208
	mother=4014208

	7331.1.1 = {birth="7331.1.1"}
	7347.1.1 = {add_trait=poor_warrior}
	7349.1.1 = {add_spouse=4017208}
	7383.1.1 = {death="7383.1.1"}
}
4017208 = {
	name="Jezhane"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7331.1.1 = {birth="7331.1.1"}
	7383.1.1 = {death="7383.1.1"}
}
1018208 = {
	name="Tezza"	# not a lord
	female=yes
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1014208
	mother=4014208

	7333.1.1 = {birth="7333.1.1"}
	7392.1.1 = {death="7392.1.1"}
}
1019208 = {
	name="Selezzqa"	# not a lord
	female=yes
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1014208
	mother=4014208

	7336.1.1 = {birth="7336.1.1"}
	7377.1.1 = {death="7377.1.1"}
}
1020208 = {
	name="Azzara"	# not a lord
	female=yes
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1014208
	mother=4014208

	7338.1.1 = {birth="7338.1.1"}
	7390.1.1 = {death="7390.1.1"}
}
1021208 = {
	name="Skahaz"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1017208
	mother=4017208

	7359.1.1 = {birth="7359.1.1"}
	7375.1.1 = {add_trait=trained_warrior}
	7377.1.1 = {add_spouse=4021208}
	7416.1.1 = {death="7416.1.1"}
}
4021208 = {
	name="Ellezzara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7359.1.1 = {birth="7359.1.1"}
	7416.1.1 = {death="7416.1.1"}
}
1022208 = {
	name="Yezza"	# not a lord
	female=yes
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1017208
	mother=4017208

	7360.1.1 = {birth="7360.1.1"}
	7436.1.1 = {death="7436.1.1"}
}
1023208 = {
	name="Fendahl"	# a lord
	dynasty=360208

	religion="harpy"
	culture="ghiscari"

	father=1021208
	mother=4021208

	7383.1.1 = {birth="7383.1.1"}
	7399.1.1 = {add_trait=poor_warrior}
	7431.1.1 = {death="7431.1.1"}
}


#################   House zo Zasnzn

1000213 = {
	name="Miklaz"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	7060.1.1 = {birth="7060.1.1"}
	7076.1.1 = {add_trait=poor_warrior}
	7078.1.1 = {add_spouse=4000213}
	7123.1.1 = {death="7123.1.1"}
}
4000213 = {
	name="Galazza"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7060.1.1 = {birth="7060.1.1"}
	7123.1.1 = {death="7123.1.1"}
}
1001213 = {
	name="Skahaz"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1000213
	mother=4000213

	7087.1.1 = {birth="7087.1.1"}
	7103.1.1 = {add_trait=poor_warrior}
	7105.1.1 = {add_spouse=4001213}
	7143.1.1 = {death="7143.1.1"}
}
4001213 = {
	name="Galazza"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7087.1.1 = {birth="7087.1.1"}
	7143.1.1 = {death="7143.1.1"}
}
1002213 = {
	name="Skahaz"	# not a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1000213
	mother=4000213

	7090.1.1 = {birth="7090.1.1"}
	7106.1.1 = {add_trait=poor_warrior}
	7157.1.1 = {death="7157.1.1"}
}
1003213 = {
	name="Azzak"	# not a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1000213
	mother=4000213

	7093.1.1 = {birth="7093.1.1"}
	7109.1.1 = {add_trait=trained_warrior}
	7142.1.1 = {death="7142.1.1"}
}
1004213 = {
	name="Yezza"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1000213
	mother=4000213

	7096.1.1 = {birth="7096.1.1"}
	7147.1.1 = {death="7147.1.1"}
}
1005213 = {
	name="Tellezzara"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1001213
	mother=4001213

	7111.1.1 = {birth="7111.1.1"}
	7190.1.1 = {death="7190.1.1"}
}
1006213 = {
	name="Sezza"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1001213
	mother=4001213

	7113.1.1 = {birth="7113.1.1"}
	7145.1.1 = {death="7145.1.1"}
}
1007213 = {
	name="Maezon"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1001213
	mother=4001213

	7116.1.1 = {birth="7116.1.1"}
	7132.1.1 = {add_trait=poor_warrior}
	7134.1.1 = {add_spouse=4007213}
	7157.1.1 = {death="7157.1.1"}
}
4007213 = {
	name="Hazzaara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7116.1.1 = {birth="7116.1.1"}
	7157.1.1 = {death="7157.1.1"}
}
1008213 = {
	name="Maezon"	# not a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1001213
	mother=4001213

	7118.1.1 = {birth="7118.1.1"}
	7134.1.1 = {add_trait=poor_warrior}
	7179.1.1 = {death = {death_reason = death_accident}}
}
1009213 = {
	name="Azzara"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1007213
	mother=4007213

	7134.1.1 = {birth="7134.1.1"}
	7185.1.1 = {death="7185.1.1"}
}
1010213 = {
	name="Hizdhar"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1007213
	mother=4007213

	7136.1.1 = {birth="7136.1.1"}
	7152.1.1 = {add_trait=trained_warrior}
	7154.1.1 = {add_spouse=4010213}
	7209.1.1 = {death="7209.1.1"}
}
4010213 = {
	name="Zellazara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7136.1.1 = {birth="7136.1.1"}
	7209.1.1 = {death="7209.1.1"}
}
1011213 = {
	name="Fendahl"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1010213
	mother=4010213

	7154.1.1 = {birth="7154.1.1"}
	7170.1.1 = {add_trait=master_warrior}
	7172.1.1 = {add_spouse=4011213}
	7212.1.1 = {death="7212.1.1"}
}
4011213 = {
	name="Selezzqa"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7154.1.1 = {birth="7154.1.1"}
	7212.1.1 = {death="7212.1.1"}
}
1012213 = {
	name="Azzea"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1010213
	mother=4010213

	7155.1.1 = {birth="7155.1.1"}
	7212.1.1 = {death = {death_reason = death_accident}}
}
1013213 = {
	name="Galazza"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1010213
	mother=4010213

	7158.1.1 = {birth="7158.1.1"}
	7208.1.1 = {death="7208.1.1"}
}
1014213 = {
	name="Dhazzar"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1010213
	mother=4010213

	7161.1.1 = {birth="7161.1.1"}
	7223.1.1 = {death="7223.1.1"}
}
1015213 = {
	name="Morghaz"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1011213
	mother=4011213

	7182.1.1 = {birth="7182.1.1"}
	7198.1.1 = {add_trait=poor_warrior}
	7200.1.1 = {add_spouse=4015213}
	7237.1.1 = {death="7237.1.1"}
}
4015213 = {
	name="Ellezzara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7182.1.1 = {birth="7182.1.1"}
	7237.1.1 = {death="7237.1.1"}
}
1016213 = {
	name="Azzea"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1015213
	mother=4015213

	7214.1.1 = {birth="7214.1.1"}
	7276.1.1 = {death="7276.1.1"}
}
1017213 = {
	name="Paezhar"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1015213
	mother=4015213

	7216.1.1 = {birth="7216.1.1"}
	7232.1.1 = {add_trait=poor_warrior}
	7234.1.1 = {add_spouse=4017213}
	7284.1.1 = {death="7284.1.1"}
}
4017213 = {
	name="Azzara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7216.1.1 = {birth="7216.1.1"}
	7284.1.1 = {death="7284.1.1"}
}
1018213 = {
	name="Qazella"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1015213
	mother=4015213

	7218.1.1 = {birth="7218.1.1"}
	7295.1.1 = {death="7295.1.1"}
}
1019213 = {
	name="Maezon"	# not a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1015213
	mother=4015213

	7219.1.1 = {birth="7219.1.1"}
	7235.1.1 = {add_trait=poor_warrior}
	7289.1.1 = {death = {death_reason = death_accident}}
}
1020213 = {
	name="Ghazdor"	# not a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1017213
	mother=4017213

	7243.1.1 = {birth="7243.1.1"}
	7259.1.1 = {add_trait=skilled_warrior}
	7273.1.1 = {death = {death_reason = death_murder}}
}
1021213 = {
	name="Mazdhan"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1017213
	mother=4017213

	7245.1.1 = {birth="7245.1.1"}
	7261.1.1 = {add_trait=poor_warrior}
	7263.1.1 = {add_spouse=4021213}
	7298.1.1 = {death="7298.1.1"}
}
4021213 = {
	name="Hazzea"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7245.1.1 = {birth="7245.1.1"}
	7298.1.1 = {death="7298.1.1"}
}
1022213 = {
	name="Faezdhar"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1021213
	mother=4021213

	7263.1.1 = {birth="7263.1.1"}
	7279.1.1 = {add_trait=poor_warrior}
	7281.1.1 = {add_spouse=4022213}
	7331.1.1 = {death="7331.1.1"}
}
4022213 = {
	name="Ezzara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7263.1.1 = {birth="7263.1.1"}
	7331.1.1 = {death="7331.1.1"}
}
1023213 = {
	name="Kraznys"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1022213
	mother=4022213

	7292.1.1 = {birth="7292.1.1"}
	7308.1.1 = {add_trait=trained_warrior}
	7310.1.1 = {add_spouse=4023213}
	7367.1.1 = {death="7367.1.1"}
}
4023213 = {
	name="Qazella"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7292.1.1 = {birth="7292.1.1"}
	7367.1.1 = {death="7367.1.1"}
}
1024213 = {
	name="Haznak"	# not a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1022213
	mother=4022213

	7295.1.1 = {birth="7295.1.1"}
	7311.1.1 = {add_trait=poor_warrior}
	7340.1.1 = {death="7340.1.1"}
}
1025213 = {
	name="Ghazdor"	# not a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1022213
	mother=4022213

	7296.1.1 = {birth="7296.1.1"}
	7312.1.1 = {add_trait=skilled_warrior}
	7356.1.1 = {death="7356.1.1"}
}
1026213 = {
	name="Harghaz"	# not a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1022213
	mother=4022213

	7299.1.1 = {birth="7299.1.1"}
	7315.1.1 = {add_trait=poor_warrior}
	7356.1.1 = {death="7356.1.1"}
}
1027213 = {
	name="Gezzella"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1023213
	mother=4023213

	7319.1.1 = {birth="7319.1.1"}
	7352.1.1 = {death="7352.1.1"}
}
1028213 = {
	name="Jezhane"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1023213
	mother=4023213

	7322.1.1 = {birth="7322.1.1"}
	7395.1.1 = {death="7395.1.1"}
}
1029213 = {
	name="Ghazdor"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1023213
	mother=4023213

	7324.1.1 = {birth="7324.1.1"}
	7340.1.1 = {add_trait=poor_warrior}
	7342.1.1 = {add_spouse=4029213}
	7372.1.1 = {death="7372.1.1"}
}
4029213 = {
	name="Ezzara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7324.1.1 = {birth="7324.1.1"}
	7372.1.1 = {death="7372.1.1"}
}
1030213 = {
	name="Sezqa"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1023213
	mother=4023213

	7326.1.1 = {birth="7326.1.1"}
	7400.1.1 = {death="7400.1.1"}
}
1031213 = {
	name="Paezhar"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1029213
	mother=4029213

	7346.1.1 = {birth="7346.1.1"}
	7362.1.1 = {add_trait=poor_warrior}
	7364.1.1 = {add_spouse=4031213}
	7389.1.1 = {death="7389.1.1"}
}
4031213 = {
	name="Qezza"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7346.1.1 = {birth="7346.1.1"}
	7389.1.1 = {death="7389.1.1"}
}
1032213 = {
	name="Selezzqa"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1031213
	mother=4031213

	7364.1.1 = {birth="7364.1.1"}
	7440.1.1 = {death="7440.1.1"}
}
1033213 = {
	name="Skahaz"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1031213
	mother=4031213

	7365.1.1 = {birth="7365.1.1"}
	7381.1.1 = {add_trait=skilled_warrior}
	7383.1.1 = {add_spouse=4033213}
	7424.1.1 = {death="7424.1.1"}
}
4033213 = {
	name="Mezzala"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7365.1.1 = {birth="7365.1.1"}
	7424.1.1 = {death="7424.1.1"}
}
1034213 = {
	name="Ellezzara"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1031213
	mother=4031213

	7368.1.1 = {birth="7368.1.1"}
	7437.1.1 = {death="7437.1.1"}
}
1035213 = {
	name="Barghaz"	# not a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1031213
	mother=4031213

	7371.1.1 = {birth="7371.1.1"}
	7387.1.1 = {add_trait=trained_warrior}
	7395.1.1 = {death="7395.1.1"}
}
1036213 = {
	name="Reznak"	# a lord
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1033213
	mother=4033213

	7385.1.1 = {birth="7385.1.1"}
	7401.1.1 = {add_trait=poor_warrior}
	7441.1.1 = {death="7441.1.1"}
}
1037213 = {
	name="Galazza"	# not a lord
	female=yes
	dynasty=360213

	religion="harpy"
	culture="ghiscari"

	father=1033213
	mother=4033213

	7387.1.1 = {birth="7387.1.1"}
	7390.1.1 = {death="7390.1.1"}
}


######################  House na Yhezher

1000220 = {
	name="Paezhar"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	7080.1.1 = {birth="7080.1.1"}
	7096.1.1 = {add_trait=poor_warrior}
	7098.1.1 = {add_spouse=4000220}
	7135.1.1 = {death="7135.1.1"}
}
4000220 = {
	name="Yezza"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7080.1.1 = {birth="7080.1.1"}
	7135.1.1 = {death="7135.1.1"}
}
1001220 = {
	name="Haznak"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1000220
	mother=4000220

	7106.1.1 = {birth="7106.1.1"}
	7122.1.1 = {add_trait=poor_warrior}
	7124.1.1 = {add_spouse=4001220}
	7174.1.1 = {death="7174.1.1"}
}
4001220 = {
	name="Zellazara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7106.1.1 = {birth="7106.1.1"}
	7174.1.1 = {death="7174.1.1"}
}
1002220 = {
	name="Fendahl"	# not a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1001220
	mother=4001220

	7130.1.1 = {birth="7130.1.1"}
	7146.1.1 = {add_trait=trained_warrior}
	7155.1.1 = {death="7155.1.1"}
}
1003220 = {
	name="Gezzella"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1001220
	mother=4001220

	7132.1.1 = {birth="7132.1.1"}
	7185.1.1 = {death="7185.1.1"}
}
1004220 = {
	name="Skahaz"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1001220
	mother=4001220

	7133.1.1 = {birth="7133.1.1"}
	7149.1.1 = {add_trait=poor_warrior}
	7151.1.1 = {add_spouse=4004220}
	7191.1.1 = {death="7191.1.1"}
}
4004220 = {
	name="Hazzea"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7133.1.1 = {birth="7133.1.1"}
	7191.1.1 = {death="7191.1.1"}
}
1005220 = {
	name="Skahaz"	# not a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1001220
	mother=4001220

	7135.1.1 = {birth="7135.1.1"}
	7151.1.1 = {add_trait=trained_warrior}
	7190.1.1 = {death="7190.1.1"}
}
1006220 = {
	name="Zaraq"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1004220
	mother=4004220

	7160.1.1 = {birth="7160.1.1"}
	7176.1.1 = {add_trait=poor_warrior}
	7178.1.1 = {add_spouse=4006220}
	7226.1.1 = {death="7226.1.1"}
}
4006220 = {
	name="Qazella"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7160.1.1 = {birth="7160.1.1"}
	7226.1.1 = {death="7226.1.1"}
}
1007220 = {
	name="Yezza"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1006220
	mother=4006220

	7191.1.1 = {birth="7191.1.1"}
	7255.1.1 = {death="7255.1.1"}
}
1008220 = {
	name="Gorzhak"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1006220
	mother=4006220

	7192.1.1 = {birth="7192.1.1"}
	7208.1.1 = {add_trait=trained_warrior}
	7210.1.1 = {add_spouse=4008220}
	7258.1.1 = {death="7258.1.1"}
}
4008220 = {
	name="Tezza"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7192.1.1 = {birth="7192.1.1"}
	7258.1.1 = {death="7258.1.1"}
}
1009220 = {
	name="Reznak"	# not a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1006220
	mother=4006220

	7193.1.1 = {birth="7193.1.1"}
	7209.1.1 = {add_trait=poor_warrior}
	7222.1.1 = {death="7222.1.1"}
}
1010220 = {
	name="Morghaz"	# not a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1006220
	mother=4006220

	7195.1.1 = {birth="7195.1.1"}
	7211.1.1 = {add_trait=trained_warrior}
	7267.1.1 = {death="7267.1.1"}
}
1011220 = {
	name="Mezzala"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1008220
	mother=4008220

	7226.1.1 = {birth="7226.1.1"}
	7275.1.1 = {death="7275.1.1"}
}
1012220 = {
	name="Ghazdor"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1008220
	mother=4008220

	7229.1.1 = {birth="7229.1.1"}
	7245.1.1 = {add_trait=poor_warrior}
	7247.1.1 = {add_spouse=4012220}
	7290.1.1 = {death="7290.1.1"}
}
4012220 = {
	name="Azzara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7229.1.1 = {birth="7229.1.1"}
	7290.1.1 = {death="7290.1.1"}
}
1013220 = {
	name="Azzara"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1008220
	mother=4008220

	7232.1.1 = {birth="7232.1.1"}
	7264.1.1 = {death="7264.1.1"}
}
1014220 = {
	name="Barkhaz"	# not a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1008220
	mother=4008220

	7233.1.1 = {birth="7233.1.1"}
	7249.1.1 = {add_trait=trained_warrior}
	7310.1.1 = {death="7310.1.1"}
}
1015220 = {
	name="Yurkhaz"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1012220
	mother=4012220

	7255.1.1 = {birth="7255.1.1"}
	7271.1.1 = {add_trait=skilled_warrior}
	7273.1.1 = {add_spouse=4015220}
	7303.1.1 = {death="7303.1.1"}
}
4015220 = {
	name="Dhazzar"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7255.1.1 = {birth="7255.1.1"}
	7303.1.1 = {death="7303.1.1"}
}
1016220 = {
	name="Hazzea"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1012220
	mother=4012220

	7258.1.1 = {birth="7258.1.1"}
	7311.1.1 = {death="7311.1.1"}
}
1017220 = {
	name="Grazdhan"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1015220
	mother=4015220

	7274.1.1 = {birth="7274.1.1"}
	7290.1.1 = {add_trait=trained_warrior}
	7292.1.1 = {add_spouse=4017220}
	7324.1.1 = {death="7324.1.1"}
}
4017220 = {
	name="Dhazzar"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7274.1.1 = {birth="7274.1.1"}
	7324.1.1 = {death="7324.1.1"}
}
1018220 = {
	name="Barkhaz"	# not a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1015220
	mother=4015220

	7275.1.1 = {birth="7275.1.1"}
	7291.1.1 = {add_trait=poor_warrior}
	7344.1.1 = {death="7344.1.1"}
}
1019220 = {
	name="Bharkaz"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1017220
	mother=4017220

	7297.1.1 = {birth="7297.1.1"}
	7313.1.1 = {add_trait=skilled_warrior}
	7315.1.1 = {add_spouse=4019220}
	7350.1.1 = {death="7350.1.1"}
}
4019220 = {
	name="Jezhane"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7297.1.1 = {birth="7297.1.1"}
	7350.1.1 = {death="7350.1.1"}
}
1020220 = {
	name="Mellezzara"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1017220
	mother=4017220

	7298.1.1 = {birth="7298.1.1"}
	7328.1.1 = {death="7328.1.1"}
}
1021220 = {
	name="Skahaz"	# not a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1017220
	mother=4017220

	7300.1.1 = {birth="7300.1.1"}
	7316.1.1 = {add_trait=skilled_warrior}
	7338.1.1 = {death="7338.1.1"}
}
1022220 = {
	name="Azzara"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1017220
	mother=4017220

	7301.1.1 = {birth="7301.1.1"}
	7372.1.1 = {death="7372.1.1"}
}
1023220 = {
	name="Yarkhaz"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1019220
	mother=4019220

	7316.1.1 = {birth="7316.1.1"}
	7332.1.1 = {add_trait=trained_warrior}
	7334.1.1 = {add_spouse=4023220}
	7382.1.1 = {death="7382.1.1"}
}
4023220 = {
	name="Gezzella"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7316.1.1 = {birth="7316.1.1"}
	7382.1.1 = {death="7382.1.1"}
}
1024220 = {
	name="Tezza"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1019220
	mother=4019220

	7318.1.1 = {birth="7318.1.1"}
	7362.1.1 = {death="7362.1.1"}
}
1025220 = {
	name="Yezza"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1019220
	mother=4019220

	7320.1.1 = {birth="7320.1.1"}
	7373.1.1 = {death="7373.1.1"}
}
1026220 = {
	name="Yezzan"	# not a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1019220
	mother=4019220

	7321.1.1 = {birth="7321.1.1"}
	7337.1.1 = {add_trait=trained_warrior}
	7396.1.1 = {death="7396.1.1"}
}
1027220 = {
	name="Barkhaz"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1023220
	mother=4023220

	7351.1.1 = {birth="7351.1.1"}
	7367.1.1 = {add_trait=poor_warrior}
	7369.1.1 = {add_spouse=4027220}
	7405.1.1 = {death="7405.1.1"}
}
4027220 = {
	name="Tezza"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7351.1.1 = {birth="7351.1.1"}
	7405.1.1 = {death="7405.1.1"}
}
1028220 = {
	name="Mezzala"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1023220
	mother=4023220

	7352.1.1 = {birth="7352.1.1"}
	7402.1.1 = {death="7402.1.1"}
}
1029220 = {
	name="Tezza"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1023220
	mother=4023220

	7355.1.1 = {birth="7355.1.1"}
	7413.1.1 = {death="7413.1.1"}
}
1030220 = {
	name="Gezzella"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1023220
	mother=4023220

	7358.1.1 = {birth="7358.1.1"}
	7416.1.1 = {death="7416.1.1"}
}
1031220 = {
	name="Hazzaara"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1023220
	mother=4023220

	7361.1.1 = {birth="7361.1.1"}
	7433.1.1 = {death="7433.1.1"}
}
1032220 = {
	name="Azzara"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1027220
	mother=4027220

	7373.1.1 = {birth="7373.1.1"}
	7440.1.1 = {death="7440.1.1"}
}
1033220 = {
	name="Hizdhar"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1027220
	mother=4027220

	7374.1.1 = {birth="7374.1.1"}
	7390.1.1 = {add_trait=trained_warrior}
	7392.1.1 = {add_spouse=4033220}
	7418.1.1 = {death="7418.1.1"}
}
4033220 = {
	name="Hazzaara"
	female=yes

	religion="harpy"
	culture="ghiscari"

	7374.1.1 = {birth="7374.1.1"}
	7418.1.1 = {death="7418.1.1"}
}
1034220 = {
	name="Kraznys"	# not a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1027220
	mother=4027220

	7377.1.1 = {birth="7377.1.1"}
	7393.1.1 = {add_trait=poor_warrior}
	7436.1.1 = {death="7436.1.1"}
}
1035220 = {
	name="Faezdhar"	# a lord
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1033220
	mother=4033220

	7397.1.1 = {birth="7397.1.1"}
	7413.1.1 = {add_trait=poor_warrior}
	7457.1.1 = {death="7457.1.1"}
}
1036220 = {
	name="Jezhane"	# not a lord
	female=yes
	dynasty=360220

	religion="harpy"
	culture="ghiscari"

	father=1033220
	mother=4033220

	7398.1.1 = {birth="7398.1.1"}
	7455.1.1 = {death="7455.1.1"}
}


