
########
#####Historic Lord Commanders#####
########
121400 = {
	name="Tristan"	# Canon, one of the worst Lord Commander's in history
	dynasty=174399 # Mudd
	
	martial = 12
	stewardship = 12
	
	religion="old_gods"
	culture="old_first_man"
	
	father=617174399
	
	
	6173.1.1 = {birth="6173.1.1"}
	6195.1.1 = { add_trait=nightswatch}
	6218.1.1 = {death=yes}
}
121406 = {
	name="Marq" #Canon, one of the worst Lord Commanders.	
	dynasty=174415 #Rankenfell
	
	martial = 12
	stewardship = 12
	
	religion="old_gods"
	culture="old_first_man"
		
	6324.1.1 = {birth="6324.1.1"}
	6340.1.1 = {add_trait=skilled_warrior add_trait=tough_soldier}
	6361.1.1 = { add_trait=nightswatch}
	6377.1.1 = { give_nickname = nick_the_mad}
	6378.1.1 = {death="6378.1.1"}
}
121404 = {
	name="Robin"	
	
	martial = 12
	stewardship = 12
	
	religion="old_gods"
	culture="old_first_man"

	add_trait="bastard"
	give_nickname = nick_hill
		
	6384.1.1 = {birth="6384.1.1"}
	6400.1.1 = {add_trait=skilled_warrior add_trait=tough_soldier}
	6405.6.1={ add_trait=nightswatch}
	6428.1.1 = {death="6428.1.1"}
}





121401 = {
	name="Runcel" # Canon, tried to pass Castle Black and the Watch to his bastard son.	
	dynasty=285 #Hightower
	
	martial = 12
	stewardship = 12
	
	religion="the_seven"
	culture="reachman"

	father=177285
		
	7649.1.1 = {birth="7649.1.1"}
	7665.1.1 = {add_trait=knight}
	7680.1.1 = { add_trait=nightswatch}
	7708.1.1 = {death = {death_reason = death_execution}}
}
121402 = {
	name="Martyn" #Canon, Runcel's bastard son.	
	dynasty=285 #Hightower
	
	martial = 12
	stewardship = 12
	
	religion="the_seven"
	culture="reachman"

	father=121401

	add_trait="bastard"
	give_nickname = nick_flowers
	
	7679.1.1 = {birth="7679.1.1"}
	7680.1.1 = { add_trait=nightswatch}
	7708.1.1 = {death = {death_reason = death_execution}}
}



1000868 = {
	name="Bass"	# a lord
	

	religion="old_gods"
	culture="northman"

	7558.1.1 = {birth="7558.1.1"}
	7574.1.1 = {add_trait=nightswatch add_trait=poor_warrior}
	7615.1.1 = {death="7615.1.1"}
}
1001868 = {
	name="Brenett"	# a lord
	

	religion="old_gods"
	culture="northman"

	7576.1.1 = {birth="7576.1.1"}
	7592.1.1 = {add_trait=nightswatch add_trait=poor_warrior}
	7628.1.1 = {death="7628.1.1"}
}
1002868 = {
	name="Dywen"	# a lord
	

	religion="old_gods"
	culture="northman"

	7588.1.1 = {birth="7588.1.1"}
	7604.1.1 = {add_trait=nightswatch add_trait=poor_warrior}
	7635.1.1 = {death="7635.1.1"}
}
1003868 = {
	name="Alaric"	# a lord
	

	religion="old_gods"
	culture="northman"

	7605.1.1 = {birth="7605.1.1"}
	7621.1.1 = {add_trait=nightswatch add_trait=poor_warrior}
	7649.1.1 = {death="7649.1.1"}
}
1004868 = {
	name="Porther"	# a lord
	

	religion="old_gods"
	culture="northman"

	7616.1.1 = {birth="7616.1.1"}
	7632.1.1 = {add_trait=nightswatch add_trait=poor_warrior}
	7662.1.1 = {death="7662.1.1"}
}
1005868 = {
	name="Rickon"	# a lord
	

	religion="old_gods"
	culture="northman"

	7637.1.1 = {birth="7637.1.1"}
	7653.1.1 = {add_trait=nightswatch add_trait=poor_warrior}
	7681.1.1 = {death="7681.1.1"}
}
1006868 = {
	name="Hareth"	# a lord
	

	religion="old_gods"
	culture="northman"

	7659.1.1 = {birth="7659.1.1"}
	7675.1.1 = {add_trait=nightswatch add_trait=poor_warrior}
	7708.1.1 = {death="7708.1.1"}
}
1007868 = {
	name="Qhorin"	# a lord
	

	religion="old_gods"
	culture="northman"

	7681.1.1 = {birth="7681.1.1"}
	7697.1.1 = {add_trait=nightswatch add_trait=poor_warrior}
	7737.1.1 = {death="7737.1.1"}
}
1008868 = {
	name="Harmond"	# a lord
	

	religion="old_gods"
	culture="northman"

	7703.1.1 = {birth="7703.1.1"}
	7719.1.1 = {add_trait=nightswatch add_trait=poor_warrior}
	7761.1.1 = {death="7761.1.1"}
}
1009868 = {
	name="Jory"	# a lord
	

	religion="old_gods"
	culture="northman"

	7733.1.1 = {birth="7733.1.1"}
	7749.1.1 = {add_trait=nightswatch add_trait=poor_warrior}
	7792.1.1 = {death="7792.1.1"}
}
1010868 = {
	name="Benfred"	# a lord
	

	religion="old_gods"
	culture="northman"

	7774.1.1 = {birth="7774.1.1"}
	7790.1.1 = {add_trait=nightswatch add_trait=poor_warrior}
	7817.1.1 = {death="7817.1.1"}
}
1011868 = {
	name="Duncan"	# a lord
	

	religion="old_gods"
	culture="northman"

	7789.1.1 = {birth="7789.1.1"}
	7805.1.1 = {add_trait=nightswatch add_trait=poor_warrior}
	7845.1.1 = {death="7845.1.1"}
}
1012868 = {
	name="Theodan"	# a lord
	

	religion="old_gods"
	culture="northman"

	7809.1.1 = {birth="7809.1.1"}
	7825.1.1 = {add_trait=nightswatch add_trait=poor_warrior}
	7858.1.1 = {death="7858.1.1"}
}
1013868 = {
	name="Kyle"	# a lord
	

	religion="old_gods"
	culture="northman"

	7812.1.1 = {birth="7812.1.1"}
	7828.1.1 = {add_trait=nightswatch add_trait=poor_warrior}
	7876.1.1 = {death="7876.1.1"}
}
1014868 = {
	name="Mallador"	# a lord
	

	religion="old_gods"
	culture="northman"

	7843.1.1 = {birth="7843.1.1"}
	7859.1.1 = {add_trait=nightswatch add_trait=poor_warrior}
	7894.1.1 = {death="7894.1.1"}
}
1015868 = {
	name="Benjen"	# a lord
	

	religion="old_gods"
	culture="northman"

	7877.1.1 = {birth="7877.1.1"}
	7893.1.1 = {add_trait=nightswatch add_trait=poor_warrior}
	7912.1.1 = {death="7912.1.1"}
}
