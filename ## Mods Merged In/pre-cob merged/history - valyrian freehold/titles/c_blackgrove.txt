### IRONRATH
5600.1.1={
	liege="d_wolfswood"
	law = succ_primogeniture
}

5600.1.1 = { holder=11443 } # Gerhard (C)
#5650.1.1 = { holder=21443 } # Line of Gerhard (nc)
5650.1.1 = { holder= 0 }
6100.1.1 = { holder=31443 } # Cedric (C)
6127.1.1 = { holder=51443 } # Karlon (nc)
#6170.1.1 = { holder=81443 } # Line of Karlon (nc)
6170.1.1 = { holder=0 }
7500.1.1 = { holder=91443 } # Medger (nc)
7552.1.1 = { holder=101443 } # Ramsay (nc)
7599.1.1 = { holder=111443 } # Torren (nc)
7611.1.1 = { holder=121443 } # Qhorin (nc)
7632.1.1 = { holder=151443 } # Leobald (nc)
7644.1.1 = { holder=161443 } # Theo (nc)
7672.1.1 = { holder=171443 } # Rickard (nc)
7732.1.1 = { holder=191443 } # Torghen (nc)
7739.1.1 = { holder=201443 } # Barth (nc)
7742.1.1 = { holder=221443 } # Galbart (nc)
7782.1.1 = { holder=261443 } # Mark (nc)
7804.1.1 = { holder=271443 } # Dywen (nc)
7847.1.1 = { holder=301443 } # Thorren (C)
7867.1.1 = { holder=311443 } # Gregor (C)
7899.8.1 = { holder=351443 } # Ethan (C)
7900.1.1 = { holder=321443 } # Rodrik (C)
7900.1.1 = { liege = k_north }
