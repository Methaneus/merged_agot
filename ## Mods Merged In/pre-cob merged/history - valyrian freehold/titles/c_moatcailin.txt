5600.1.1={
	liege="e_north"
	law = succ_appointment
	effect = {
		set_title_flag = military_command
		holder_scope = {
			if = {
				limit = { primary_title = { title = PREVPREV } }
				set_government_type = military_command_government 
				PREV = { succession = appointment }
				recalc_succession = yes
			}
		}
	}	
}
7588.1.1={
	holder = 20059 #Torrhen Stark
}	
7614.1.1 = {
	holder=20159 #Brandon Stark
}
7647.12.12 = {
	holder=552059 #Brenett Stark (NC)
}
7667.1.1 = {
	holder = 20259 #Ellard Stark
}
7706.1.1 = {
	holder = 20559 #Benjen Stark
}
7715.1.1 = {
	holder=20659 # Rickon Stark 
}
7721.1.1 = {
	holder=20859 #Cregan Stark 
}
7791.1.1 = {
	holder=22059 #Jonnel Stark 
}
7796.1.1 ={
	holder=24059 #Barthogan 'Barth' Stark 
}
7808.1.1 = {
	holder=25059 #Brandon Stark
}
7812.8.7 = {
	holder=28059 # Rodwell  Stark
}
7813.6.1 = {
	holder=21059 # Beron  Stark
}
7815.1.1 = {
	holder=20959 #Willam Stark {Beron's 2nd son}
}
7826.9.9 = {
	holder=21169 #Edwyle Stark {Willem's son}
}
7862.1.1 = {
	holder=2058 #Rickard Stark
}
7882.1.1={
	holder=59 #Eddard Stark
}
7898.11.2={
	holder = 2059 #Robb Stark
	#liege=0
}

7898.12.20 = {
	liege = "e_north"
}
7899.11.5={
	holder = 87 #roose bolton
	liege="k_north"
}
