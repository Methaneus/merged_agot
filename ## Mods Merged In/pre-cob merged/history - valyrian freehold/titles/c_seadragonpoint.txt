1.1.1={
    liege="e_north"
	law = succ_primogeniture
	holder=821059
	#Brandon the Builder
}
1.1.1 = {
    holder = 826059 # Brandon, son of Bran the builder
}
1.1.1 = {
    holder = 827059 # Jon
}	    
1.1.1 = { 
    holder = 0 
}

6050.1.1={
	holder = 8115059 #Brandon
}

6060.1.1={
	holder = 815059 #Jon
}
6103.1.1={
	holder = 814059 #Rickard
}
6131.1.1 = {
	holder = 0
}
6197.1.1 = {
	holder = 820059 #Theon the Hungry Wolf
}
6238.1.1 = {
	holder = 0
}

6827.1.1= {
    holder = 1120159 # Brandon the Daughterless
}
6867.12.12= {
    holder = 1120459 # Jon, Bael's son
}
6898.9.9= {
    holder = 8311059 # Harlon
}
6924.9.9= {
    holder = 81321059 # Ellard
}
6952.1.1= {
    holder = 8331059 # Barth
}

6991.1.1 = {
	holder = 0
}

7052.1.1 = {
	holder = 819059 #Brandon the Shipwright
}
7061.1.1={
	holder = 818059 #Brandon the Burner
}
7127.1.1={
	holder = 817059 #Jonnel
}
7132.1.1={
	holder = 816059 #Dorren
}
7182.1.1 = {
	holder = 850059 #Brandon
}

7190.1.1 = {
	holder = 8150059 #Brandon
}
7209.1.1 = {
	holder = 813059 #Rodrik
}
7220.1.1 = {
	holder = 812059 #Edrick
}
7309.1.1 = {
	holder = 811059 #Brandon
}
7315.1.1 = {
	holder = 810059 #Rodrik
}
7322.1.1={
	holder = 809059 #Brandon ice Eyes
}
7342.1.1={
	holder = 808059 #Benjen
}
7365.1.1={
	holder = 807059 #Benjen
}
7379.1.1={
	holder = 806059 #Eyron
}
7412.1.1 ={
	holder = 805059 #Edderion
}
7435.1.1 ={
	holder = 804059 #Walton
}
7456.1.1 ={
	holder = 803059 #Brandon the Bad
}
7492.1.1  ={
	holder = 802059 #Jorah
}
7519.1.1 ={
	holder = 801059 #Jonos
}
7551.1.1 ={
	holder = 800059 #Edwyn
}
7588.1.1={
	holder = 20059 #Torrhen Stark
}
7614.1.1 = {
	holder=20159 #Brandon Stark
}
7647.12.12 = {
	holder=552059 #Brenett Stark (NC)
}
7667.1.1 = {
	holder = 20259 #Ellard Stark
}
7706.1.1 = {
	holder = 20559 #Benjen Stark
}
7715.1.1 = {
	holder=20659 # Rickon Stark 
}
7721.1.1 = {
	holder=20859 #Cregan Stark 
}
7791.1.1 = {
	holder=22059 #Jonnel Stark 
}
7796.1.1 ={
	holder=24059 #Barthogan 'Barth' Stark 
}
7808.1.1 = {
	holder=25059 #Brandon Stark
}
7812.8.7 = {
	holder=28059 # Rodwell  Stark
}
7813.6.1 = {
	holder=21059 # Beron  Stark
}
7815.1.1 = {
	holder=26059 #Donnor Stark {Beron's 1st son}
}
7819.1.1 = {
	holder=20959 #Willam Stark {Beron's 2nd son}
}
7826.9.9 = {
	holder=21169 #Edwyle Stark {Willem's son}
}
7862.1.1 = {
	holder=2058 #Rickard Stark
}
7882.1.1={
	holder=59 #Eddard Stark
}
7898.11.2={
	holder = 2059 #Robb Stark
}
7899.11.5={
	holder = 2087 #Ramsay Bolton
}


