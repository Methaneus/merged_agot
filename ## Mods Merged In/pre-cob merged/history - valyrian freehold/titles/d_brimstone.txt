7240.1.1={
	liege="k_brimstoneTK"
	law = succ_primogeniture
	law = true_cognatic_succession
}
7274.1.1 = {
	holder = 175013 #Lucifer Dryland
}
7303.1.1 = { # conquered by the Martell's.
	liege="e_dorne"
	holder = 150013 #Lord Uller
	effect = {
		b_hellholt = {
			ROOT = {
				holder_scope = {
					if = {
						limit = { has_landed_title = b_hellholt }
						capital = PREVPREV
					}
				}
			}	
		}	
	}
}
7316.1.1 = { holder = 0 }

7528.1.1 = { holder=152013}
7571.1.1 = { holder=60013 } # Symon (nc)
7596.1.1 = { holder=60113 } # Yandry (nc)
7604.1.1 = { holder=60313 } # Elyana (nc)
7612.1.1 = { holder=60713 } # Mariya (nc)
7651.1.1 = { holder=60813 } # Elyana (nc)
7690.1.1 = { holder=61213 } # Melessa (nc)
7729.1.1 = { holder=61313 } # Archibald (nc)
7756.1.1 = { holder=61513 } # Arthur (nc)

7759.4.7 = { holder=62013 } # Gerris (nc)

7760.7.3 = { holder=62113 } # Boran (nc)
7787.1.1 = { holder=62513 } # Symon (nc)



7796.4.18 = { holder=62913 } # Theodan (nc)
7811.2.10 = { holder=63113 } # Doran (nc)
7843.1.1 = { holder=63613 } # Daemon (nc)

