500.1.1={
    liege="e_riverlands"
	law = succ_primogeniture
	law = cognatic_succession
	holder=0
}

###House Justman
6539.1.1 = { holder=600174432 } # Benedict (C) # House Jusman
6562.1.1 = { holder=602174432 } # Benedict (C) 
6622.1.1 = { holder=606174432 } # Bernarr (C)
6629.1.1 = { holder=607174432 } # Edwyn (nc)
6638.1.1 = { holder=609174432 } # Brynden (nc)
6686.1.1 = { holder=615174432 } # Bernarr (C)

6692.1.1 = { holder=0 } # Justman royal line extinct

###House Teague
6800.1.1 = { holder=600174435 } # Torrence (C) # House Teague
6850.1.1 = { holder=601174435 } # Brynden (nc)
6851.1.1 = { holder=602174435 } # Petyr (nc)
6875.1.1 = { holder=604174435 } # Theo (C)
6890.1.1 = { holder=0 }
7260.1.1 = { holder=607174435 } # Humfrey (C)
7280.1.1 = { holder=608174435 } # Humfrey  II( C)
7280.1.2 = { holder=609174435 } # Hollis  ( C)
7280.1.3 = { holder=611174435 } # Tyler C)
7280.1.4 = { holder=612174435 } # Damon( C)
7280.1.5 = { holder=0 }

