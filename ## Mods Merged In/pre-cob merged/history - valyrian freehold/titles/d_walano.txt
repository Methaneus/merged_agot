#House Xhaxos
7560.1.1 = { 
	holder=100304008 
	law = succ_seniority
	law = cognatic_succession
} # Adeyemu (nc)
7603.1.1 = { holder=101304008 } # Odingaxho (nc)
7620.1.1 = { holder=103304008 } # Bapato (nc)
7642.1.1 = { holder=107304008 } # Doshuru (nc)
7702.1.1 = { holder=108304008 } # Rudo (nc)
7717.1.1 = { holder=112304008 } # Odingaxho (nc)
7744.1.1 = { holder=114304008 } # Quhura (nc)

#House Qo Unifies the Islands
7758.1.1 = { holder=114304005 } # Xanda (C)
7778.1.1 = { holder=118304005 } # Isingodo (nc)
7803.1.1 = { holder=121304005 } # Thabo (nc)
7821.1.1 = { holder=122304005 } # Xhelimo (nc)
7835.1.1 = { holder=124304005 } # Jalahuru (nc)

#House Xho usurps the Islands
7858.1.1 = { holder=122304000 } # Udo (nc)
7887.1.1 = { holder=126304000 } # Qubhar (nc)

7889.6.1 = { holder=127304005 } #Usurped by House Ko
