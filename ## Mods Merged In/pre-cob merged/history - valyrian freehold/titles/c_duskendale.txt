400.1.1={
	liege="d_duskendale"
}
7556.1.1 = { holder=6000364 } # Jarmen (nc)
7580.1.1 = { holder=6002364 } # Triston (nc)
7598.2.1={ holder=6007364 } # Steffon (nc)
7609.1.1 = {  #kings landing expansion
	effect = {
		b_duskendale = {
			remove_building = ct_asoiaf_crown_basevalue_6
		}
	}
}
7618.1.1 = {  #kings landing expansion
	effect = {
		b_highley = {
			remove_building = ct_asoiaf_crown_basevalue_3
		}
	}
}
7621.1.1 = { holder=6009364 } # Robin (nc)
7627.1.1 = { #kings landing expansion
	effect = {
		b_highley = {
			remove_building = ct_asoiaf_crown_basevalue_2
			remove_building = ct_asoiaf_crown_basevalue_1
		}
	}
}
7635.1.1 = { holder=6012364 } # Florian (nc)
7636.1.1 = { #kings landing expansion
	effect = {
		b_duskendale = {
			remove_building = ct_asoiaf_crown_basevalue_5
		}
	}
}
7657.1.1 = { holder=6019364 } # Ulmer (nc)
7700.1.1 = { holder=6022364 } # Dale (nc)
7729.10.1 = { holder=6025364 } # Jaremy (nc)
7754.1.1 = { holder=6029364 } # Monford (nc)
7789.1.1 = { holder=6035364 } # Allar (nc)
7799.1.1 = { holder=6038364 } # Will (nc)
7843.1.1 = { holder=6045364 } # Alyn
7872.1.1 = { holder=6049364 } # Denys, Lord at the Defiance
7876.10.1={
	holder = 36 	#Dylan Rykker
}
7877.1.1={
	holder = 2036 	#Renfred Rykker
}
