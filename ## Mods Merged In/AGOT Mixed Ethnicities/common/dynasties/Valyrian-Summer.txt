999304000 = {
	name="Xhonis"
	culture = valyrian_summer_islander
}
999304001 = {
	name="Bonae"
	culture = valyrian_summer_islander
}
999304002 = {
	name="Mova"
	culture = valyrian_summer_islander
}
999304003 = {
	name="Rhaeqho"
	culture = valyrian_summer_islander
}
999304004 = {
	name="Xhoana"
	culture = valyrian_summer_islander
}
999304005 = {
	name="Qonarys" #canon
	culture = valyrian_summer_islander
}
999304006 = {
	name="Xhaneon" #nc
	culture = valyrian_summer_islander
}
999304007 = {
	name="Luxhoa" #nc
	culture = valyrian_summer_islander
}
999304008 = {
	name="Xhaxaes" #nc
	culture = valyrian_summer_islander
}
999304010 = {
	name="Uxaela" #nc
	culture = valyrian_summer_islander
}
999304011 = {
	name="Agbealu" #nc
	culture = valyrian_summer_islander
}
999304012 = {
	name="Ajakarios" #nc
	culture = valyrian_summer_islander
}