7706541 = {
	name = Swiggity
	culture = islander

	used_for_random = no
	can_appear = no
}

7706542 = {
	name = Bomahmah
	culture = islander

	used_for_random = no
	can_appear = no
}

7706543 = {
	name = Vut
	culture = islander

	used_for_random = yes
	can_appear = yes
}

7706544 = {
	name = Koodj
	culture = islander

	used_for_random = yes
	can_appear = yes
}

7706545 = {
	name = Obomamana
	culture = islander

	used_for_random = yes
	can_appear = yes
}

7706546 = {
	name = Guh
	culture = islander

	used_for_random = yes
	can_appear = yes
}

7706547 = {
	name = Gug
	culture = islander

	used_for_random = yes
	can_appear = yes
}

7706548 = {
	name = Luv
	culture = islander

	used_for_random = yes
	can_appear = yes
}

7706549 = {
	name = Kraff
	culture = islander

	used_for_random = yes
	can_appear = yes
}

7706550 = {
	name = Lum
	culture = islander

	used_for_random = yes
	can_appear = yes
}

7706551 = {
	name = Tuttle

	used_for_random = yes
	can_appear = yes
}

7706552 = {
	name = Louz
	culture = islander

	used_for_random = yes
	can_appear = yes
}

7706553 = {
	name = Rl'yeah
	culture = islander

	used_for_random = yes
	can_appear = yes
}

7706554 = {
	name = Green
	culture = islander

	used_for_random = yes
	can_appear = yes
}

7706555 = {
	name = Greenie
	culture = islander

	used_for_random = yes
	can_appear = yes
}

7706556 = {
	name = Relyeh
	culture = islander

	used_for_random = yes
	can_appear = yes
}

7706557 = {
	name = Howard
	culture = islander

	used_for_random = yes
	can_appear = yes
}

7706558 = {
	name = Phillips
	culture = islander

	used_for_random = yes
	can_appear = yes
}

7706559 = {
	name = Fesh
	culture = islander

	used_for_random = yes
	can_appear = yes
}

7706560 = {
	name = Fishchep
	culture = islander

	used_for_random = yes
	can_appear = yes
}

7706561 = {
	name = Smeet
	culture = islander

	used_for_random = yes
	can_appear = yes
}

7706562 = {
	name = Squamos
	culture = islander

	used_for_random = yes
	can_appear = yes
}

7706563 = {
	name = Squamm
	culture = islander

	used_for_random = yes
	can_appear = yes
}

7706564 = {
	name = Squish
	culture = islander

	used_for_random = yes
	can_appear = yes
}

7706565 = {
	name = Squisher
	culture = islander

	used_for_random = yes
	can_appear = yes
}

7706566 = {
	name = Squash
	culture = islander

	used_for_random = yes
	can_appear = yes
}

7706567 = {
	name = Squick
	culture = islander

	used_for_random = yes
	can_appear = yes
}

7706568 = {
	name = Watersdeep
	culture = islander

	used_for_random = yes
	can_appear = yes
}

7706569 = {
	name = Watersmeet
	culture = islander

	used_for_random = yes
	can_appear = yes
}

7706570 = {
	name = Billyfesh
	culture = islander

	used_for_random = yes
	can_appear = yes
}

7706571 = {
	name = Nemos
	culture = islander

	used_for_random = yes
	can_appear = yes
}

7706572 = {
	name = Marlinos
	culture = islander

	used_for_random = yes
	can_appear = yes
}

7706573 = {
	name = Doryos
	culture = islander

	used_for_random = yes
	can_appear = yes
}

7706574 = {
	name = K'rushos
	culture = islander

	used_for_random = yes
	can_appear = yes
}

7706575 = {
	name = "the Chad"

	used_for_random = yes
	can_appear = yes
}

7706576 = {
	name = Chad

	used_for_random = yes
	can_appear = yes
}