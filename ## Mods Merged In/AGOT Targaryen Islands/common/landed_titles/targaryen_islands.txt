	d_targaryen_islands = {
		color={ 82 239 85 }
		color2={ 255 255 255 }
		
		allow = {
			OR = {				
				custom_tooltip = {
					text = TOOLTIPhl_creation_requirement
					hidden_tooltip = { any_demesne_province = { duchy = { title = FROM } } }
				}	
				has_game_rule = { name = hl_creation_requirement value = off }
			}	
		}
		
		c_aegon = {
			color={ 84 15 15 }
			color2={ 255 255 255 }
			
			b_aegon_1 = {
			}
			b_aegon_2 = {
			}
			b_aegon_3 = {
			}
			b_aegon_4 = {
			}
			b_aegon_5 = {
			}
			b_aegon_6 = {
			}
			b_aegon_7 = {
			}
		}
		
		c_rhaenys = {
			color={ 115 32 32 }
			color2={ 255 255 255 }
			
			b_rhaenys_1 = {
			}
			b_rhaenys_2 = {
			}
			b_rhaenys_3 = {
			}
			b_rhaenys_4 = {
			}
			b_rhaenys_5 = {
			}
			b_rhaenys_6 = {
			}
			b_rhaenys_7 = {
			}
		}
		
		c_visenya = {
			color={ 143 62 62 }
			color2={ 255 255 255 }
			
			b_visenya_1 = {
			}
			b_visenya_2 = {
			}
			b_visenya_3 = {
			}
			b_visenya_4 = {
			}
			b_visenya_5 = {
			}
			b_visenya_6 = {
			}
			b_visenya_7 = {
			}
		}
		
	}