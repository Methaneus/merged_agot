# 500 - Tall trees Town

# County Title
title = c_aegon

# Settlements
max_settlements = 4
b_aegon_1 = castle
b_aegon_2 = city
b_aegon_3 = city

# Misc
culture = summer_islander
religion = summer_rel

terrain = jungle

#History
1.1.1 = {
	b_aegon_1 = ca_asoiaf_summerisland_basevalue_1
	
	b_aegon_2 = ct_asoiaf_summerisland_basevalue_1
	b_aegon_2 = ct_asoiaf_summerisland_basevalue_2
	
	b_aegon_3 = ct_asoiaf_summerisland_basevalue_1
	
}