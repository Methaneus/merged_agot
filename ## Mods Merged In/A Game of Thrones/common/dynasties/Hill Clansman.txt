##Hill Clansman##
#non-canon
178900 = {
	name="Rugger"
	culture = hill_clansman
}
178901 = {
	name="Hardstaff"
	culture = hill_clansman
}
178902 = {
	name="Troter"
	culture = hill_clansman
}
178903 = {
	name="Skinner"
	culture = hill_clansman
}
178904 = {
	name="Vallie"
	culture = hill_clansman
}
178905 = {
	name="Narder"
	culture = hill_clansman
}
178906 = {
	name="Wars"
	culture = hill_clansman
}
178907 = {
	name="Lash"
	culture = hill_clansman
}
178908 = {
	name="Harker"
	culture = hill_clansman
}
178909 = {
	name="Greatsword"
	culture = hill_clansman
}
178910 = {
	name="Sling"
	culture = hill_clansman
}
178911 = {
	name="Whitehill"
	culture = hill_clansman
}
178912 = {
	name = Nolt
	culture = hill_clansman
}
178913 = {
	name = Hough
	culture = hill_clansman
}
178914 = {
	name = Tofty
	culture = hill_clansman
}
178915 = {
	name = Hallow
	culture = hill_clansman
}
178916 = {
	name = Raye
	culture = hill_clansman
}
178917 = {
	name = Tarn
	culture = hill_clansman
}
178918 = {
	name = Burney
	culture = hill_clansman
}
178919 = {
	name = Foy
	culture = hill_clansman
}
178920 = {
	name = Dorr
	culture = hill_clansman
}
178921 = {
	name = Reiver
	culture = hill_clansman
}
178922 = {
	name = Grayne
	culture = hill_clansman
}
178923 = {
	name = Chiftan
	culture = hill_clansman
}
178924 = {
	name = Sleat
	culture = hill_clansman
}
178925 = {
	name = Manrent
	culture = hill_clansman
}
178926 = {
	name = Laird
	culture = hill_clansman
}
178927 = {
	name = Wullson
	culture = hill_clansman
}
178928 = {
	name = Ellott
	culture = hill_clansman
}
178929 = {
	name = Skott
	culture = hill_clansman
}
178930 = {
	name = Blackhill
	culture = hill_clansman
}
178931 = {
	name = Trotter
	culture = hill_clansman
}
178932 = {
	name = Craw
	culture = hill_clansman
}
178933 = {
	name = Hurley
	culture = hill_clansman
}
178934 = {
	name = Lyddal
	culture = hill_clansman
}
178935 = {
	name = Knottwoode
	culture = hill_clansman
}
178936 = {
	name = Ker
	culture = hill_clansman
}

#canon
92 = {
	name="Burley"
	culture = hill_clansman
	coat_of_arms = {
		template = 0
		layer = {
			texture = 5
			texture_internal = 1
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
96 = {
	name="Harclay"
	culture = hill_clansman
	coat_of_arms = {
		template = 0
		layer = {
			texture = 5
			texture_internal = 11
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
97 = {
	name="Liddle"
	culture = hill_clansman
	coat_of_arms = {
		template = 0
		layer = {
			texture = 5
			texture_internal = 17
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
98 = {
	name="Knott"
	culture = hill_clansman
	coat_of_arms = {
		template = 0
		layer = {
			texture = 5
			texture_internal = 15
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
99 = {
	name="Wull"
	culture = hill_clansman
	coat_of_arms = {
		template = 0
		layer = {
			texture = 5
			texture_internal = 40
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}
100 = {
	name="Norrey"
	culture = hill_clansman
	coat_of_arms = {
		template = 0
		layer = {
			texture = 5
			texture_internal = 26
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
}

