decisions = {

	extort_subjects = {
		is_high_prio = yes
		only_playable = yes
		
		potential = {
			has_ambition = obj_build_a_war_chest
			is_tribal = no
			is_merchant_republic = no
		}
		allow = {
			prisoner = no
			NOR = { 
				trait = incapable
				is_inaccessible_trigger = yes
			}
			#war = no
			custom_tooltip = {
				text = extort_cooldown_custom_tooltip
				hidden_tooltip = {
					NOT = { has_character_modifier = extort_timer }
				}
			}
			custom_tooltip = { #something is available to be extorted
				text = extort_subjects_custom_tooltip_A
				hidden_tooltip = {
					OR = {
						any_demesne_province = {
							NOR = {
								has_province_modifier = extra_tax 
								has_province_modifier = extra_tax_kind
								has_province_modifier = colony
								has_province_modifier = partial_colony
							}
							any_province_holding = {
								OR = {
									holding_type = city
									holding_type = temple
									holding_type = castle
									holding_type = tribal
								}
							}
						}
						any_demesne_province = {
							any_province_holding = {
								holding_type = temple
								num_of_buildings = 4
								holder_scope = {
									NOT = { character = ROOT } 
									tier = baron
									liege = { character = ROOT }
								}
							}
						}
						any_courtier = {
							liege = { character = ROOT }
							NOT = { is_close_relative = ROOT }
							is_adult = yes
							prisoner = no
							is_landed = no
							wealth = 100
							NOR = {
								has_character_modifier = extort_timer
								is_married = ROOT 
								dynasty = ROOT
								is_friend = ROOT
								holds_favor_on = ROOT
							}
						}
					}
				}
			}
		}
		effect = {
			custom_tooltip = {
				text = extort_consequences_custom_tooltip
				hidden_tooltip = {
					add_character_modifier = {
					   modifier = extort_timer
					   months = 6
					   hidden = yes
					}
					character_event = { id = ZE.21000 }
				}
			}
		}
		ai_check_interval = 12
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 0.12
			}
			
			modifier = {
				factor = 0
				calc_true_if = {
					amount = 2
					trait = kind
					trait = charitable
				}
			}
			
			modifier = {
				factor = 0.2
				any_liege = { ai = no }
			}
			
			modifier = {
				factor = 0.2
				OR = {
					trait = kind
					trait = charitable
				}
			}
			modifier = {
				factor = 5
				NOT = { wealth = 0 }
			}
			
			modifier = {
				factor = 2
				NOT = { wealth = 100 }
				independent = yes
			}
			
			modifier = {
				factor = 0.1
				trait = patient
			}
			
			modifier = {
				factor = 3
				trait = greedy
			}
			
			modifier = {
				factor = 2
				trait = cruel
			}
			
			modifier = {
				factor = 2
				trait = arbitrary
			}
			
			modifier = {
				factor = 2
				trait = stressed
			}
				
			modifier = {
				factor = 0.5
				trait = content
			}
		}
	}
	
	donate_to_liege = {
		is_high_prio = yes
		only_rulers = yes
		
		potential = {
			is_ruler = yes
			liege = { has_ambition = obj_build_a_war_chest }			
			NOT = { tier = BARON }
			NOT = { liege = { character = ROOT } }
		}
		allow = {
			prisoner = no
			NOR = { 
				trait = incapable
				is_inaccessible_trigger = yes
			}
			war = no
			#liege = { war = no }
			wealth = 200
			NOT = { liege = { owes_favor_to = ROOT } }
			NOT = { has_ambition = obj_build_a_war_chest }
			custom_tooltip = {
				text = donation_cooldown_custom_tooltip
				hidden_tooltip = {
					NOT = { has_character_modifier = donation_offer_timer }
				}
			}
		}
		effect = {
			custom_tooltip = {
				text = offer_donation_custom_tooltip
				hidden_tooltip = {
					add_character_modifier = {
					   modifier = donation_offer_timer
					   years = 5
					   hidden = yes
					}
					liege = { character_event = { id = ZE.22000 } }
				}
			}
			wealth = -200
		}
		ai_check_interval = 10
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 0.1
			}
			
			modifier = {
				factor = 0
				trait = greedy
			}
			
			modifier = {
				factor = 0
				NOT = { opinion = { who = LIEGE value = 50 } }
			}
			
			modifier = {
				factor = 0
				is_rival = LIEGE
			}
			
			modifier = {
				factor = 0
				NOT = { wealth = 400 }
			}
			
			modifier = {
				factor = 2
				OR = {
					is_councillor = yes
					is_voter = yes
				}	
			}
			
			modifier = {
				factor = 2
				trait = charitable
			}
			
			modifier = {
				factor = 2
				wealth = 600
			}
			
			modifier = {
				factor = 2
				wealth = 800
			}
			
			modifier = {
				factor = 2
				wealth = 1000
			}
			
			modifier = {
				factor = 10
				liege = { is_friend = ROOT }
			}
		}
	}
	
}