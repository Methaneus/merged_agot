# 162 - Pyke

# County Title
title = c_pyke

# Settlements
max_settlements = 3
b_pyke = castle
b_seagrave = temple

#b_brodsworth = castle


# Misc
culture = old_ironborn
religion = drowned_god

#History

1.1.1 = {
	b_pyke = ca_asoiaf_ironislands_basevalue_1
	b_pyke = ca_asoiaf_ironislands_basevalue_2
	b_pyke = ca_asoiaf_ironislands_basevalue_3
	b_pyke = ca_asoiaf_ironislands_basevalue_4
	b_pyke = ca_asoiaf_ironislands_basevalue_5
	b_pyke = ca_asoiaf_ironislands_basevalue_6

	b_pyke = ca_asoiaf_ironshipyard
}

6700.1.1 = {
	culture = ironborn
}
