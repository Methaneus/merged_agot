# 93 - Red Fork

# County Title
title = c_redfork

# Settlements
max_settlements = 4
b_hartshead = castle
b_swamplake = city
b_waterford = temple

# Misc
culture = old_first_man
religion = old_gods

# History


1.1.1 = {

b_hartshead = ca_asoiaf_river_basevalue_1

b_swamplake = ct_asoiaf_river_basevalue_1
b_swamplake = ct_asoiaf_river_basevalue_2
b_swamplake = ct_asoiaf_river_basevalue_3



}

6700.1.1 = {
	culture = riverlander
	religion = the_seven
}





