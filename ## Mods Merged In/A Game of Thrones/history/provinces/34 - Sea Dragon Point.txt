# 34 - Sea Dragon Point

# County Title
title = c_seadragonpoint

# Settlements
max_settlements = 2
b_seadragonwatch = castle
#b_dragonport = city

# Misc
culture = old_first_man
religion = old_gods

# History

1.1.1 = {
	b_seadragonwatch = ca_asoiaf_north_basevalue_1
}
6700.1.1 = {
	culture = northman
}