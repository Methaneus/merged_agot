# 1198 - Nettlemont

# County Title
title = c_nettlemont

# Settlements
max_settlements = 3
b_nettlebank = castle
b_blackblood = castle

#b_pinehole


# Misc
culture = old_ironborn
religion = drowned_god

#History

1.1.1 = {
	b_nettlebank = ca_asoiaf_ironislands_basevalue_1
	b_nettlebank = ca_asoiaf_ironislands_basevalue_2
	b_nettlebank = ca_asoiaf_ironislands_basevalue_3

	b_nettlebank = ca_asoiaf_ironshipyard

	b_blackblood = ca_asoiaf_ironislands_basevalue_1

	capital = b_blackblood 
}

6700.1.1 = {
	culture = ironborn
}

7998.2.1 = {
	capital = b_nettlebank 
}