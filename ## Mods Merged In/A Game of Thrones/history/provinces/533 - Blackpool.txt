# 533 - Blackpool

# County Title
title = c_blackpool

# Settlements
max_settlements = 3
b_blackpool = castle
b_longhouse = castle

# Misc
culture = old_first_man
religion = old_gods

# History

1.1.1 = {
	b_blackpool = ca_asoiaf_north_basevalue_1
	b_blackpool = ca_asoiaf_north_basevalue_2
	
	b_longhouse = ca_asoiaf_north_basevalue_1
}
6700.1.1 = {
	culture = northman
}