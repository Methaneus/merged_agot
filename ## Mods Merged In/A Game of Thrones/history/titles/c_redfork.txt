900.1.1={
    liege="e_riverlands"
	law = succ_primogeniture
	law = cognatic_succession
	holder=0
}

###House Justman
6939.1.1 = { holder=600174432 } # Benedict (C) # House Jusman
6962.1.1 = { holder=602174432 } # Benedict (C) 
7022.1.1 = { holder=606174432 } # Bernarr (C)
7029.1.1 = { holder=607174432 } # Edwyn (nc)
7038.1.1 = { holder=609174432 } # Brynden (nc)
7086.1.1 = { holder=615174432 } # Bernarr (C)

7092.1.1 = { holder=0 } # Justman royal line extinct

###House Teague
7200.1.1 = { holder=600174435 } # Torrence (C) # House Teague
7250.1.1 = { holder=601174435 } # Brynden (nc)
7251.1.1 = { holder=602174435 } # Petyr (nc)
7275.1.1 = { holder=604174435 } # Theo (C)
7290.1.1 = { holder=0 }
7660.1.1 = { holder=607174435 } # Humfrey (C)
7680.1.1 = { holder=608174435 } # Humfrey  II( C)
7680.1.2 = { holder=609174435 } # Hollis  ( C)
7680.1.3 = { holder=611174435 } # Tyler C)
7680.1.4 = { holder=612174435 } # Damon( C)
7680.1.5 = { holder=0 }

#House Strong
7830.1.1={
	holder=164356
}
7884.1.1={
	holder=163356
}
7905.1.1={
	holder=162356
}
7926.1.1={
	holder=161356
}
7970.1.1={
	holder=160356
}
7976.1.1 = { holder=148356 } # Harwin (nc)
7998.2.1={
	liege="k_riverlands"
}
8005.1.1 = { holder=149356 } # Osmund (C)
8017.1.1 = { liege=e_iron_throne }
8034.1.1 = { holder=152356 liege=k_riverlands } # Simon (nc)
8049.1.1 = { holder=6356 } # Bywin (C)
8076.1.1 = {
	holder=157356
}
8086.1.1 = {
	holder=4356 #Lyonel
}
8120.6.1 = {
	holder=300356 #Larys
}

#Reverts to the tullys post dance
8131.1.8={
	holder=13159 # Kermit Tully
}
8170.1.1={
	holder=19159 # Edmure Tully
}
8182.1.1={
    holder=2280159 # Edmund Tully
}
8195.12.10={
	holder=1159 # Medgar Tully
}
8210.1.1={
	holder=10159
}
8258.1.1={
	holder=175159
}
8265.1.1 = {
	holder=159
}
#Brotherhood temp title so they are playable
8298.10.28={
	liege = d_brotherhood
	holder = 1303 #Beric Dondarrion
}
8299.11.5 = {
	holder = 250159 #Lady Stoneheart
}

# 8299.10.15={
	# holder=4159
# }
# 8300.4.15 = {
	# holder=27 #Walder Frey
# }