decisions = {
	fix_valyrian_steel = {
		ai = no
		potential = {
			ai = no
			OR = {
				trait = ashbringer
				trait = avarice
				trait = avenger
				trait = calibur
				trait = dusk
				trait = dustcloud
				trait = emancipate
				trait = fornicater
				trait = harbinger
				trait = misers_folly
				trait = red_menace
				trait = stormblade
				trait = unbound
				trait = vainglory
				trait = vendetta
				trait = whiplash
				trait = white_menace
				trait = bloodbloom
				trait = cataclysm
				trait = desolation
				trait = dread
				trait = exile
				trait = extinction
				trait = futility
				trait = headsmans_herald
				trait = houndsbreath
				trait = judgment
				trait = lionheart
				trait = raging_storm
				trait = ravenwing
				trait = redeemer
				trait = remorse
				trait = ruinsword
				trait = shadows_fall
				trait = sovereign
				trait = spiderbite
				trait = valor
				trait = destiny
				trait = eternity
				trait = fate
			}
		}
		allow = {
			OR = {
				trait = ashbringer
				trait = avarice
				trait = avenger
				trait = calibur
				trait = dusk
				trait = dustcloud
				trait = emancipate
				trait = fornicater
				trait = harbinger
				trait = misers_folly
				trait = red_menace
				trait = stormblade
				trait = unbound
				trait = vainglory
				trait = vendetta
				trait = whiplash
				trait = white_menace
				trait = bloodbloom
				trait = cataclysm
				trait = desolation
				trait = dread
				trait = exile
				trait = extinction
				trait = futility
				trait = headsmans_herald
				trait = houndsbreath
				trait = judgment
				trait = lionheart
				trait = raging_storm
				trait = ravenwing
				trait = redeemer
				trait = remorse
				trait = ruinsword
				trait = shadows_fall
				trait = sovereign
				trait = spiderbite
				trait = valor
				trait = destiny
				trait = eternity
				trait = fate
			}
		}
		effect = {
			character_event = { id = valyrian_steel.998 }
		}
	}
}