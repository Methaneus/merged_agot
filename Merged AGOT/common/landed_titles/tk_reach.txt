k_the_arborTK = {
	color = { 40 46 140 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_the_arbor }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 292 # The Arbor
	creation_requires_capital = yes
	culture = reachman
}

k_blueburnTK = {
	color = { 122 172 201 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_blueburn }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 259 # Grassy Vale
	creation_requires_capital = no
	culture = reachman
}

k_brightwaterTK = {
	color = { 189 67 58 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_brightwater }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 274 # Brightwater
	creation_requires_capital = yes
	culture = reachman
}

k_cockleswhentTK = {
	color = { 63 167 149 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_cockleswhent }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 270 # Ashford
	creation_requires_capital = no
	culture = reachman
}

k_goldengroveTK = {
	color = { 241 172 26 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_goldengrove }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 243 # Goldengrove
	creation_requires_capital = yes
	culture = reachman
}

k_highgardenTK = {
	color = { 13 96 0 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_highgarden }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 267 # Highgarden
	creation_requires_capital = yes
	culture = reachman
}

k_mandervaleTK = {
	color = { 12 37 131 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_mandervale }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 268 # Cider Hall
	creation_requires_capital = no
	culture = reachman
}

k_northmarchTK = {
	color = { 15 116 219 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_northmarch }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 238 # Coldmoat
	creation_requires_capital = no
	culture = reachman
}

k_ocean_roadTK = {
	color = { 117 169 179 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_ocean_road }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 235 # Old Oak
	creation_requires_capital = no
	culture = reachman
}

k_oldtownTK = {
	color = { 83 83 83 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_oldtown }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 286 # Oldtown
	creation_requires_capital = yes
	culture = reachman
}

k_red_lakeTK = {
	color = { 193 70 70 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_red_lake }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 236 # Red Water Keep
	creation_requires_capital = yes
	culture = reachman
}

k_roseroadTK = {
	color = { 254 62 100 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_roseroad }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 253 # Bitterbridge
	creation_requires_capital = no
	culture = reachman
}

k_the_shield_islesTK = {
	color = { 127 220 230 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_the_shield_isles }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 263 # Oak Castle
	creation_requires_capital = no
	culture = reachman
}

k_torrentpeakTK = {
	color = { 13 58 33 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_torrentpeak }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 250 # Torrentpeak
	creation_requires_capital = yes
	culture = reachman
}

k_tumbletonTK = {
	color = { 87 189 190 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_tumbleton }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 249 # Tumbleton
	creation_requires_capital = yes
	culture = reachman
}

k_west_marchesTK = {
	color = { 118 151 198 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_west_marches }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 280 # Horn Hill
	creation_requires_capital = no
	culture = reachman
}