#### Titular Kingdoms - North ####
d_widowswatch = {
	color={ 100 99 100 }
	color2={ 255 255 255 }
	
	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { duchy = { title = FROM } } }
			}
			has_game_rule = { name = hl_creation_requirement value = off }
		}
	}
}
