#THE WALL
e_winter = {
	color = { 140 200 255 }
	color2 = { 255 255 255 }
}

#THE NORTH
d_deepwood_motte = {
	color = { 142 142 142 }
	color2 = { 255 255 255 }

	allow = {
		OR = {
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { duchy = { title = FROM } } }
			}
			has_game_rule = { name = hl_creation_requirement value = off }
		}
	}
}

d_torrhens_square = {
	color={ 156 156 156 }
	color2={ 255 255 255 }

	capital = 44

	allow = {
		OR = {
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { is_capital = FROM } }
			}
			has_game_rule = { name = hl_creation_requirement value = off }
		}
		NOT = { has_landed_title = d_winterfell }
	}
}

d_cerwyn = {
	color={ 168 168 168 }
	color2={ 255 255 255 }

	capital = 49

	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { is_capital = FROM } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}
		NOT = { has_landed_title = d_winterfell }
	}
}

#d_oldcastle = {
#	color = { 130 130 130 }
#	color2 = { 255 255 255 }
#
#	allow = {
#		OR = {
#			custom_tooltip = {
#				text = TOOLTIPhl_creation_requirement
#				hidden_tooltip = { any_demesne_province = { duchy = { title = FROM } } }
#			}
#			has_game_rule = { name = hl_creation_requirement value = off }
#		}
#	}
#}

d_long_lake = {
	color={ 162 162 162 }
	color2={ 255 255 255 }
	
	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { duchy = { title = FROM } } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}	
	}
}

#d_hornwoodcastle = {
#	color={ 202 203 179 }
#	color2={ 255 255 255 }
#	
#	allow = {
#		OR = {				
#			custom_tooltip = {
#				text = TOOLTIPhl_creation_requirement
#				hidden_tooltip = { any_demesne_province = { duchy = { title = FROM } } }
#			}	
#			has_game_rule = { name = hl_creation_requirement value = off }
#		}	
#	}
#}

#d_widowswatch = {
#	color={ 156 156 156 }
#	color2={ 255 255 255 }
#	
#	allow = {
#		OR = {				
#			custom_tooltip = {
#				text = TOOLTIPhl_creation_requirement
#				hidden_tooltip = { any_demesne_province = { duchy = { title = FROM } } }
#			}	
#			has_game_rule = { name = hl_creation_requirement value = off }
#		}
#	}
#}

d_fever = {
	color={ 141 141 141 }
	color2={ 255 255 255 }
	
	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { duchy = { title = FROM } } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}	
	}
}

#IRON ISLANDS
#d_old_wyk = {
#	color={ 128 85 0 }
#	color2={ 255 255 255 }
#	
#	allow = {
#		OR = {				
#			custom_tooltip = {
#				text = TOOLTIPhl_creation_requirement
#				hidden_tooltip = { any_demesne_province = { duchy = { title = FROM } } }
#			}	
#			has_game_rule = { name = hl_creation_requirement value = off }
#		}	
#	}
#}

#d_saltcliffe = {
#	color={ 184 135 0 }
#	color2={ 255 255 255 }
#	
#	allow = {
#		OR = {				
#			custom_tooltip = {
#				text = TOOLTIPhl_creation_requirement
#				hidden_tooltip = { any_demesne_province = { duchy = { title = FROM } } }
#			}	
#			has_game_rule = { name = hl_creation_requirement value = off }
#		}	
#	}
#}

d_blacktyde = {
	color={ 140 113 3 }
	color2={ 255 255 255 }
	
	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { duchy = { title = FROM } } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}	
	}
}

#d_orkmont = {
#	color={ 179 113 3 }
#	color2={ 255 255 255 }
#	
#	allow = {
#		OR = {				
#			custom_tooltip = {
#				text = TOOLTIPhl_creation_requirement
#				hidden_tooltip = { any_demesne_province = { duchy = { title = FROM } } }
#			}	
#			has_game_rule = { name = hl_creation_requirement value = off }
#		}	
#	}
#}

d_lonely_light = {
	color={ 128 95 0 }
	color2={ 255 255 255 }
	
	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { duchy = { title = FROM } } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}	
	}
}

d_sealskin_point = {
	color={ 153 77 0 }
	color2={ 255 255 255 }
	
	capital = 151 # Sealskin Point
	
	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { is_capital = FROM } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}
		NOT = { has_landed_title = d_great_wyk }
	}
}

d_hammerhorn = {
	color={ 107 79 0 }
	color2={ 255 255 255 }
	
	capital = 152 # Hammerhorn
	
	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { is_capital = FROM } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}
		NOT = { has_landed_title = d_great_wyk }
	}
}

d_pebbleton = {
	color={ 168 123 0 }
	color2={ 255 255 255 }
	
	capital = 153 # Pebbleton
	
	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { is_capital = FROM } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}
		NOT = { has_landed_title = d_great_wyk }
	}
}

d_orkwood = {
	color={ 179 119 2 }
	color2={ 255 255 255 }
	
	capital = 156 # Orkmont
	
	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { is_capital = FROM } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}
		NOT = { has_landed_title = d_orkmont }
	}
}

d_nettlemont = {
	color={ 200 131 2 }
	color2={ 255 255 255 }
	
	capital = 1198 # Nettlemont
	
	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { is_capital = FROM } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}
		NOT = { has_landed_title = d_orkmont }
	}
}

#VALE
d_longbow_hall = {
	color={ 53 139 164 }
	color2={ 255 255 255 }
	
	capital = 139

	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { is_capital = FROM } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}
		NOT = { has_landed_title = d_eastweald }
	}
}

d_old_anchor = {
	color={ 38 203 203 }
	color2={ 255 255 255 }
	
	capital = 143

	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { is_capital = FROM } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}
		NOT = { has_landed_title = d_eastweald }
	}
}

d_hearts_home = {
	color={ 60 190 190 }
	color2={ 255 255 255 }
	
	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { duchy = { title = FROM } } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}	
	}
}

d_newkeep = {
	color={ 100 228 220 }
	color2={ 255 255 255 }
	
	capital = 130

	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { is_capital = FROM } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}
		NOT = { has_landed_title = d_the_fingers }
	}
}

#RIVERLANDS
d_pinkmaiden = {
	color={ 47 53 191 }
	color2={ 255 255 255 }
	
	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { duchy = { title = FROM } } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}	
	}
}

d_greenfork = {
	color={ 35 65 207 }
	color2={ 255 255 255 }
	
	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { duchy = { title = FROM } } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}	
	}
}

d_oldstones_ruin = {
	color={ 0 0 65 }
	color2={ 255 255 255 }
	
	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { duchy = { title = FROM } } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}	
	}
}

#WESTERLANDS
d_banefort = {
	color={ 241 35 15 }
	color2={ 255 255 255 }
	
	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { duchy = { title = FROM } } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}	
	}
}

d_river_road = {
	color={ 241 47 15 }
	color2={ 255 255 255 }
	
	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { duchy = { title = FROM } } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}	
	}
}

d_tarbeck = {
	color={  241 45 15 }
	color2={ 255 255 255 }
	
	capital = 171

	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { is_capital = FROM } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}
		NOT = { has_landed_title = d_goldencoast }
	}
}

d_goldencoast = {
	color={ 255 93 66 }
	color2={ 255 255 255 }
	
	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { duchy = { title = FROM } } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}	
	}
}

d_deep_den = {
	color={ 215 35 15 }
	color2={ 255 255 255 }
	
	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { duchy = { title = FROM } } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}	
	}
}

#CROWNLANDS
e_crownlands = {
	color={ 190 30 120 }
	color2={ 255 255 255 }

	capital = 226 # King's Landing
	
	allow = {
		e_iron_throne = { is_titular = yes }
		NOT = { has_landed_title = e_iron_throne }
		k_crownlands = { is_titular = no }
		has_landed_title = k_crownlands
	}
}

d_rooks_rest = {
	color={ 161 67 95 }
	color2={ 255 255 255 }
	
	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { duchy = { title = FROM } } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}	
	}
}

d_antlers = {
	color={ 234 85 149 }
	color2={ 255 255 255 }
	
	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { duchy = { title = FROM } } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}	
	}
}

#d_hayford = {
#	color={ 216 101 153 }
#	color2={ 255 255 255 }
#	
#	allow = {
#		OR = {				
#			custom_tooltip = {
#				text = TOOLTIPhl_creation_requirement
#				hidden_tooltip = { any_demesne_province = { duchy = { title = FROM } } }
#			}	
#			has_game_rule = { name = hl_creation_requirement value = off }
#		}	
#	}
#}

#d_claw_isle = {
#	color={ 157 46 109 }
#	color2={ 255 255 255 }
#	
#	allow = {
#		OR = {				
#			custom_tooltip = {
#				text = TOOLTIPhl_creation_requirement
#				hidden_tooltip = { any_demesne_province = { duchy = { title = FROM } } }
#			}	
#			has_game_rule = { name = hl_creation_requirement value = off }
#		}	
#	}
#}

#d_driftmark = {
#	color={ 245 71 173 }
#	color2={ 255 255 255 }
#	
#	allow = {
#		OR = {				
#			custom_tooltip = {
#				text = TOOLTIPhl_creation_requirement
#				hidden_tooltip = { any_demesne_province = { duchy = { title = FROM } } }
#			}	
#			has_game_rule = { name = hl_creation_requirement value = off }
#		}	
#	}
#}

#d_sweetport_sound = {
#	color={ 213 81 173 }
#	color2={ 255 255 255 }
#	
#	allow = {
#		OR = {				
#			custom_tooltip = {
#				text = TOOLTIPhl_creation_requirement
#				hidden_tooltip = { any_demesne_province = { duchy = { title = FROM } } }
#			}	
#			has_game_rule = { name = hl_creation_requirement value = off }
#		}	
#	}
#}

#d_sharp_point = {
#	color={ 245 71 173 }
#	color2={ 255 255 255 }
#	
#	allow = {
#		OR = {				
#			custom_tooltip = {
#				text = TOOLTIPhl_creation_requirement
#				hidden_tooltip = { any_demesne_province = { duchy = { title = FROM } } }
#			}	
#			has_game_rule = { name = hl_creation_requirement value = off }
#		}	
#	}
#}

#STORMLANDS
k_summerhall = {
	color={ 245 245 120 }
	color2={ 255 255 255 }

	capital = 306

	title = "DORNEPRINCE"
	title_female = "DORNEPRINCE_female"
	foa = "DORNEPRINCE_FOA"
	title_prefix = "PRINCEDOM_OF"

	allow = {
		k_summerhall = { is_titular = no }
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { is_capital = FROM } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}
		NOT = { has_landed_title = k_stormlands }
	}
}

d_parchments = {
	color={ 220 219 97 }
	color2={ 255 255 255 }
	
	capital = 299

	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { is_capital = FROM } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}
		NOT = { has_landed_title = d_durranvale }
	}
}

d_rain_house = {
	color={ 225 215 30 }
	color2={ 255 255 255 }
	
	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { duchy = { title = FROM } } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}	
	}
}

d_estermont = {
	color={ 180 200 80 }
	color2={ 255 255 255 }
	
	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { duchy = { title = FROM } } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}	
	}
}

d_blackhaven = {
	color={ 255 190 60 }
	color2={ 255 255 255 }
	
	capital = 311

	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { is_capital = FROM } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}
	}
}

d_harvest_hall = {
	color={ 220 225 75 }
	color2={ 255 255 255 }
	
	capital = 308

	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { is_capital = FROM } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}
		NOT = { has_landed_title = d_dornish_marches }
	}
}

d_gallowsgrey = {
	color={ 245 225 120 }
	color2={ 255 255 255 }
	
	capital = 310

	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { is_capital = FROM } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}
		NOT = { has_landed_title = d_durranvale }
	}
}

d_nightsong = {
	color={ 235 255 0 }
	color2={ 255 255 255 }
	
	capital = 307

	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { is_capital = FROM } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}
		NOT = { has_landed_title = d_dornish_marches }
	}
}

d_haystack_hall = {
	color={ 215 200 60 }
	color2={ 255 255 255 }
	
	capital = 298

	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { is_capital = FROM } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}
		NOT = { has_landed_title = d_shipbreaker }
	}
}

d_durranvale = {
	color={ 222 220 95 }
	color2={ 255 255 255 }
	
	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { duchy = { title = FROM } } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}	
	}
}

#REACH
d_three_towers = {
	color={ 110 230 140 }
	color2={ 255 255 255 }
	
	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { duchy = { title = FROM } } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}	
	}
}

d_blackcrown = {
	color={ 85 160 55 }
	color2={ 255 255 255 }
	
	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { duchy = { title = FROM } } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}	
	}
}

d_uplands = {
	color={ 45 130 45 }
	color2={ 255 255 255 }
	
	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { duchy = { title = FROM } } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}	
	}
}

d_starpike = {
	color={ 30 110 25 }
	color2={ 255 255 255 }
	
	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { duchy = { title = FROM } } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}	
	}
}

#DORNE
d_starfall = {
	color={ 200 80 0 }
	color2={ 255 255 255 }
	
	capital = 335

	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { is_capital = FROM } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}
		NOT = { has_landed_title = d_redmountains }
	}
}

d_wyl = {
	color={ 200 140 30 }
	color2={ 255 255 255 }
	
	capital = 330

	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { is_capital = FROM } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}
		NOT = { has_landed_title = d_stoneway }
	}
}

d_yronwood = {
	color={ 220 120 30 }
	color2={ 255 255 255 }
	
	capital = 334

	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { is_capital = FROM } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}
		NOT = { has_landed_title = d_stoneway }
	}
}

#d_blackmont = {
#	color={ 150 40 0 }
#	color2={ 255 255 255 }
#	
#	capital = 327
#
#	allow = {
#		OR = {				
#			custom_tooltip = {
#				text = TOOLTIPhl_creation_requirement
#				hidden_tooltip = { any_demesne_province = { is_capital = FROM } }
#			}	
#			has_game_rule = { name = hl_creation_requirement value = off }
#		}
#		NOT = { has_landed_title = d_redmountains }
#	}
#}

d_sandstone = {
	color={ 240 80 30 }
	color2={ 255 255 255 }
	
	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { duchy = { title = FROM } } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}	
	}
}

d_wells = {
	color={ 240 50 0 }
	color2={ 255 255 255 }
	
	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { duchy = { title = FROM } } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}	
	}
}

d_the_tor = {
	color={ 210 130 0 }
	color2={ 255 255 255 }
	
	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { duchy = { title = FROM } } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}	
	}
}

d_ghaston_grey = {
	color={ 200 150 80 }
	color2={ 255 255 255 }
	
	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { duchy = { title = FROM } } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}	
	}
}

d_greenblood = {
	color={ 220 120 30 }
	color2={ 255 255 255 }
	
	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { duchy = { title = FROM } } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}	
	}
}

d_godsgrace = {
	color={ 220 170 0 }
	color2={ 255 255 255 }
	
	capital = 349

	allow = {
		OR = {				
			custom_tooltip = {
				text = TOOLTIPhl_creation_requirement
				hidden_tooltip = { any_demesne_province = { is_capital = FROM } }
			}	
			has_game_rule = { name = hl_creation_requirement value = off }
		}
		NOT = { has_landed_title = d_greenblood }
	}
}

#ESSOS
e_bones = {
	color={ 150 100 50 }
	color2={ 255 255 255 }
}

e_jade_sea = {
	color={ 0 168 107 }
	color2={ 6 128 0 }
	
	lengi="Leng"
	asshai="Asshai"
	asabhadi="Asabhad"
	moraqi="Moraq"
	farosi="Moraq"
	zabhadi="Moraq"
	vahari="Moraq"
	
	dignity = 25
}