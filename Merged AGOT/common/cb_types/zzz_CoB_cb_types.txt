claim_riverlands_CoB = { #Arlan III invasion
	name = CB_NAME_CLAIM
	war_name = WAR_NAME_claim_stormlands_ACOK
	sprite = 1
	truce_days = 3650
	hostile_against_others = yes
	can_ask_to_join_war = no
	attacker_can_call_allies = no
	press_claim = yes
	allow_whitepeace = yes
	infamy_modifier = 0

	can_use = { always = no }
	can_use_title = { always = no }

	is_valid_title = {
		OR = {
			ROOT = {
				tier = EMPEROR
				is_adult = yes
				is_female = no
				is_incapable = no
				prisoner = no
			}
			d_blackwood = {
				holder_scope = {
					has_non_aggression_pact_with = ROOT
					has_claim = PREVPREV
					is_adult = yes
					is_female = no
					is_incapable = no
					prisoner = no
				}
			}
		}
	}

	on_add = { }

	on_success = {
		hidden_tooltip = {
			any_defender = {
				character_event = { id = mega_wars.86 days = 1 } #rebel leader resolution
			}
			FROM = { any_courtier = { set_character_flag = former_royal_courtier } }
		}
		ROOT = { prestige = 400 }
		hidden_tooltip = {
			any_attacker = {
				limit = { NOT = { character = ROOT } }
				participation_scaled_prestige = 300
			}
		}
		FROM = { prestige = -200 }
	}

	on_success_title = {
		hidden_tooltip = { ROOT = { add_character_modifier = { name = victory_timer duration = 3 } } }
		holder_scope = { save_event_target_as = old_usurped_title_holder }
		if = { #if blackwood is not available, take lands
			limit = {
				NOT = {
					d_blackwood = {
						holder_scope = {
							has_non_aggression_pact_with = ROOT
							has_claim = PREVPREV
							is_adult = yes
							is_female = no
							is_incapable = no
							prisoner = no
						}
					}
				}
			}
			usurp_title = { target = ROOT type = invasion }
			FROM = { 
				set_defacto_liege = ROOT 
			}
			hidden_tooltip = {			
				ROOT = { 				
					character_event = { id = maintenance.24 } #Mega war flag maintenance
					character_event = { id = maintenance.21 } #Take dejure vassals (AGOT)
				} 
				FROM = {
					if = {
						limit = { is_liege_or_above = ROOT }
						character_event = { id = maintenance.25 } #Subjugation flag maintenance
					}	
				}
				FROM = { character_event = { id = diplomatic.51 days = 2 } } #destroy redundant kingdoms
			}
		}	
		if = { #if blackwood is available, give him the riverlands
			limit = {
				d_blackwood = {
					holder_scope = {
						has_non_aggression_pact_with = ROOT
						has_claim = PREVPREV
						is_adult = yes
						is_female = no
						is_incapable = no
						prisoner = no
					}
				}
			}
			d_blackwood = {
				holder_scope = {
					e_riverlands = { usurp_title = { target = PREV type = invasion } }
					hidden_tooltip = {
						opinion = {
							modifier = pressed_my_claim 
							who = ROOT
						}
						if = {
							limit = {
								has_nickname = no
							}
							random = {
								chance = 20
								give_nickname = nick_the_usurper
							}
						}
						character_event = { id = maintenance.26 } #Vanilla bug resets laws, so fix empire tracker law and primogeniture
						character_event = { id = maintenance.23 } #Other Claim Mega War flag maintenance
						character_event = { id = maintenance.21 } #Take dejure vassals (AGOT)
					}
				}
			}
		}
	}

	on_fail = {
		ROOT = {
			prestige = -200
		}
		any_defender = {
			limit = { character = FROM }
			participation_scaled_prestige = 100
		}
		any_defender = {
			limit = { NOT = { character = FROM } }
			hidden_tooltip = { participation_scaled_prestige = 100 }
		}
	}

	on_reverse_demand = {
		hidden_tooltip = { FROM = { add_character_modifier = { name = victory_timer duration = 3 } } } 
		hidden_tooltip = {
			any_attacker = {
				limit = { NOT = { character = ROOT } }
				character_event = { id = mega_wars.74 days = 1 } #king can revoke titles of LPs and non direct vassals
			}
		}
		ROOT = {
			prestige = -200
			transfer_scaled_wealth = {
				to = FROM
				value = 4.0
			} 
			set_character_flag = abort_obj
		}
		any_defender = {
			limit = { character = FROM }
			character_event = { id = maintenance.2199 tooltip = TOOLTIPmaintenance.2199 } #Take back dejure vassals (AGOT)
			participation_scaled_prestige = 200
		}
		any_defender = {
			limit = { NOT = { character = FROM } }
			hidden_tooltip = { participation_scaled_prestige = 200 }
		}	
	}

	on_reverse_demand_title = { }
	on_defender_leader_death = { }
	attacker_ai_victory_worth = { factor = -1 } # always accept
	attacker_ai_defeat_worth = { factor = 100 }
	defender_ai_victory_worth = { factor = -1 } # always accept
	defender_ai_defeat_worth = { factor = 100 }
}