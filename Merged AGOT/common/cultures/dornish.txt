dornish = {
	graphical_cultures = { dornishgfx } # units

	stone_dornish = {
		graphical_cultures = { stonedornishgfx westerngfx } # units
		color = { 0.95 0.6 0 }

		alternate_start = {
			year >= 7600
			NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } }
		}

		male_names = {
			Addam Aeron Aethan Aidan Alaric Albin Alekyne Allard Alyn Ambrose Anders Andrey Anton Archibald Arnolf Arran Arrant Arthur Aryard
			Baylon Benedict Beric Branden
			Cael Casimir Casper Cassiel Caswyn Cedric Clarien Cleon Cletus Connor Conran Coran Corbyn Corentyn Cugel Cullen
			Dagos Darien Daven Davos Dickon Domeric Dunstan
			Edgar Edmund Edmyn Edric
			Farien Farrell Ferris Finnian Franklyn
			Galwyn Garmond Garrison Gedry Gerold Gerris Guerin Gyles
			Halbert Hugh
			Jarmond Jarreth Jasper Jerome Joffrey Jordan Joris
			Lambert Lancelyn Landon Lorrent Lucimore Lyonel
			Michael Morgan Morris Mors Morys Myles
			Olyvar Olyver Orivel Ormond
			Perrin Perros
			Raynald Robin Roryn
			Quentyn Quincy
			Samwell
			Ulrick Urros
			Vorian
			Walter Warryn Willam Wyland
			Yoren Yorick Yorwin
		}
		female_names = {
			Adeline Ailys Alanna Alays Alia Alienor Allyria Alma Alyna Arabella Arenne Arianne Arlette Ashara Aslia Avelyn Azalais
			Barbrey Bryanna
			Caitrin Clarisse Cordelia Cyrissa
			Delicia Denisa Denyse Dinah Doncella Dyanna
			Elianne Elicia Elysa Erin
			Falyce Fionalla
			Galenna Giselle Gwyneth
			Helen
			Inis Ivalla Ivanna
			Jaryse Jasmylla Jennalyn Jennelyn Jessa Jeyne Jyalla Jynessa
			Katrin
			Lanei Larra Leyne Liera Linnet Loreza Lorinda Lyarra Lyra
			Marcia Marella Margaid Marietta Marise Mariya Martina Maryssa Melanna Moryssa
			Nicolette
			Rayne Rhona Robyna Rosarya Rossalyn
			Serena Serene
			Tamara Tavia Tereza
			Ursula
			Valeria
			Wylla
			Ynessa Ynys
		}

		from_dynasty_prefix = "of "

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 15
		mat_grf_name_chance = 10
		father_name_chance = 0

		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 15
		mat_grm_name_chance = 10
		mother_name_chance = 0

		feminist = yes

		modifier = default_culture_modifier
	}

	sand_dornish = {
		graphical_cultures = { sanddornishgfx } # units
		color = { 0.75 0.4 0 }
		
		alternate_start = {
			year >= 7600
			NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } }
		}

		male_names = {
			Addam Aderyn Aimery Archibald Arron Arros Aubry
			Balan Ballard Bayard
			Caston Castor Caswald Clayton Conary Conlan Cullian
			Daemon Daeron Darien Darius Davyd Derryn Dylan
			Galayn Galbert Ganlos Garibald Garyn Gascoyne Gavin Gelyon Godric Gulian
			Harmen
			Jarvis Jeffory Joreth Josland Jossart Justin
			Kedry
			Lewyn Lyron
			Mael Manfryd Mathan Mavros
			Oriel Orrell
			Quentyn Quinlan
			Raylon Ryon
			Sargon Sebaston Selwyn Serion Symon
			Tarion Theodan
			Utheryn Uthor Ulwyck Ulwyn
			Warryn Willam
		}
		female_names = {
			Alenei Aleona Alerra Alis Allanys Allyria Alyse Alyx Amrya Arabelle Arbella Azilia
			Cassella Charissa Cyrene
			Damarya Dancy Danelle Darce Delonne Diane
			Elinora Ellaria Emmeline Enora Evain Eveline
			Gavella Geleria Gisela Gwenlyn
			Hallina
			Islene Ismerei
			Jarianne Jayne Jehana Jehane Jeynora Jezarra Jillian Jinny Josiane Joslena
			Kerenza
			Larissa Leonella Liane Lyralla
			Madina Margareta Margery Maryse Merra Moriah Morwen
			Nymeria
			Rachelle Ricassa Rosilla
			Samara Sarianne Serena Serra
			Talia
			Verona
			Ysilla
		}
		from_dynasty_prefix = "of "

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 15
		mat_grf_name_chance = 10
		father_name_chance = 0

		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 15
		mat_grm_name_chance = 10
		mother_name_chance = 0

		feminist = yes

		modifier = default_culture_modifier
	}

	salt_dornish = {
		graphical_cultures = { saltdornishgfx } # units
		color = { 0.85 0.5 0 }

		alternate_start = {
			year >= 7600
			NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } }
		}

		male_names = {
			Ademar Alric Andrey Angran Arden Armand Aron
			Baduin Barrett Bastian Bedard Blaise Branston Brys
			Cadan Conayn Corrent
			Danton Danyll Darren Davit Denzel Derrett Derrik Derry Desmond Devin Devlin Devon Deziel Digory Dinias Doran Duran Dylan
			Edgar Elfred Elric Elyas Emrys
			Galeran Galwell Gareth Garin Gavyn Gerris Gerys Gesmund Gethan Gideon Gilbert Godry Gorian Greyson Griffin Griffith Gyren
			Jaran Jaspin Josua
			Landon Laryn Laurent Leoran Lewyn
			Madyn Malor Manfrey Marden Marence Marius Maron Matrim Meribald Merrick Milo Morgan Moribald Morion Mors Myron
			Neilyn Nigel Nymor
			Oberyn Olymer Olyvar Orlyn Orrys Owen
			Qoren Quentyn Qyle
			Raymun Reynold Rhodry Rickard Ronan Ronyn Royce Rufus Russell Rustyn
			Sargon Sherman Simon Sylas Symon
			Talfryn Tamlyn Tomas Toryn Trebor Tremond Trystane
			Waldon Warrin Wendel Willam
		}
		female_names = {
			Adrienne Adrya Alara Alba Aleona Alesia Aliandra Alyse Amaria Ameline Ariana Arianne Arlene
			Belessa
			Claire Coryanne
			Dacella Dacey Daleria Danella Darcia Deneza Deria Deridre Dora Dorea
			Elda Eleria Elia Eloise Elyce Emilia Emphyria Eselle Essylt Evelyn
			Gemma Gilda Gileta Glynis Gwyn
			Halera Helia
			Idella Iona Izabel
			Jayne Jeanne Jocasta Jolene Joleta Julianna Jyalla
			Leonette Leyla Lilah Liliane Lilya Linette Lisette Loreza Lucinda Lynette Lyrella
			Maerra Maia Mairona Malia Mara Marion Maryce Meria Milona Mirielis Monielle Morna Muriel Mylene Myria Myriah
			Nella Nera Nerissa Nicola Nymella Nymeria Nyrene
			Obara Obella Olivia Ormane
			Ravenna Rhialta Richelle Roslyn Roxana
			Sarella Sarilla Sarra Selara Senara Serene Sharyn Silva Sybella Sybelle Synella Sylva Sylvenna
			Tamsyn Tanyth Tara Teora Tristana Tyene Tyssa
			Valena Vanora
			Wylla
		}

		from_dynasty_prefix = "of "

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 15
		mat_grf_name_chance = 10
		father_name_chance = 0

		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 15
		mat_grm_name_chance = 10
		mother_name_chance = 0

		feminist = yes

		modifier = default_culture_modifier
	}

	rhoynar = {
		graphical_cultures = { saltdornishgfx } # units
		color = { 0.8 0.25 0.0 }

		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }

		male_names = {
			Allar Anders Aryen Aron Alleras
			Belyar Bors
			Caleyen Casyos
			Daemyn Daeron Dygos Dezyel Doran
			Garyn Gasoyne Gerrys Gulyan
			Lewyn
			Mallor Manfrey Maron Mors
			Nymor
			Oberyn
			Peyos
			Quentyn Qoren
			Rycas Ryon
			Symon
			Timyon Trebor Trystane
			Yandry
		}
		female_names = {
			Allyria Alysanne Alyse Amary Aryanne Ashara Aliandra
			Beyandra
			Caroley Cassella Cedra Cerenya Corenna Cyrenna
			Delonne Donyse Dorea Dyanna
			Eleanor Elia Ellaria Elyana Emberlia
			Jynessa
			Laria Leyla Loreza Lynesse
			Marei Mariya Marya Melessa Mellei Morra Myria
			Nymella Nymeria Nysterica
			Obara Obella
			Sarelya Senelle Sylva
			Tanselia
			Tyene
			Wylia
			Ysilla
		}

		from_dynasty_prefix = "of "

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 15
		mat_grf_name_chance = 10
		father_name_chance = 0

		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 15
		mat_grm_name_chance = 10
		mother_name_chance = 0

		feminist = yes

		modifier = default_culture_modifier
	}
}