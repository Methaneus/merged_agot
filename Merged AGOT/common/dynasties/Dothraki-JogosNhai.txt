600200 = {
	name="Bharbo" #Canon
	culture = dothraki
	used_for_random = no
	can_appear = no
}
600201 = {
	name="Drogo" #Canon
	culture = dothraki
	used_for_random = no
	can_appear = no
}

600202 = {
	name="Jhaquo"
	culture = dothraki
	used_for_random = no
	can_appear = no
}
600203 = {
	name="Temmo"
	culture = dothraki
	used_for_random = no
	can_appear = no
}
600204 = {
	name="Pono" #Canon
	culture = dothraki
	used_for_random = no
	can_appear = no
}
600205 = {
	name="Jhaqo" #Canon
	culture = dothraki
	used_for_random = no
	can_appear = no
}
600206 = {
	name="Zekko" #Canon
	culture = dothraki
	used_for_random = no
	can_appear = no
}
600207 = {
	name="Mengo" #Canon
	culture = dothraki
	used_for_random = no
	can_appear = no
}
600208 = {
	name="Horro" #Canon
	culture = dothraki
	used_for_random = no
	can_appear = no
}
600209 = {
	name="Onqo" #Canon
	culture = dothraki
	used_for_random = no
	can_appear = no
}
600210 = { 
	name="Temmo" #Canon
	culture = dothraki
	used_for_random = no
	can_appear = no
}
600211 = {
	name="Zirqo"
	culture = dothraki
	used_for_random = no
	can_appear = no
}
600212 = {
	name="Onqo"
	culture = dothraki
	used_for_random = no
	can_appear = no
}
600213 = {
	name="Remekko"
	culture = dothraki
	used_for_random = no
	can_appear = no
}
600214 = {
	name="Haggo"
	culture = dothraki
	used_for_random = no
	can_appear = no
}
600215 = {
	name="Kovarro"
	culture = dothraki
	used_for_random = no
	can_appear = no
}
600216 = {
	name="Marro"
	culture = dothraki
	used_for_random = no
	can_appear = no
}
600217 = {
	name="Rogo"
	culture = dothraki
	used_for_random = no
	can_appear = no
}
600218 = {
	name="Rovo"
	culture = dothraki
	used_for_random = no
	can_appear = no
}
600219 = {
	name="Harro"
	culture = dothraki
	used_for_random = no
	can_appear = no
}
600220 = {
	name="Cohollo" #Canon
	culture = dothraki
	used_for_random = no
	can_appear = no
}
600221 = {
	name="Loso" #Canon
	culture = dothraki
	used_for_random = no
	can_appear = no
}
600222 = {
	name="Zhako" #Canon
	culture = dothraki
	used_for_random = no
	can_appear = no
}
600223 = {
	name="Qano" #Canon
	culture = dothraki
	used_for_random = no
	can_appear = no
}
600224 = {
	name="Haro" #Canon
	culture = dothraki
	used_for_random = no
	can_appear = no
}
600225 = {
	name="Zeggo" #Canon
	culture = dothraki
	used_for_random = no
	can_appear = no
}
600226 = {
	name="Scoro" #Canon
	culture = dothraki
	used_for_random = no
	can_appear = no
}
600227 = {
	name="Rogo" #Canon
	culture = dothraki
	used_for_random = no
	can_appear = no
}
600228 = {
	name="Moro" #Canon
	culture = dothraki
	used_for_random = no
	can_appear = no
}
600229 = {
	name="Jommo" #Canon
	culture = dothraki
	used_for_random = no
	can_appear = no
}
600230 = {
	name="Ogo" #Canon
	culture = dothraki
	used_for_random = no
	can_appear = no
}
600231 = {
	name="Motho" #Canon
	culture = dothraki
	used_for_random = no
	can_appear = no
}
600232 = {
	name="Rovo"
	culture = dothraki
	used_for_random = no
	can_appear = no
}
600233 = {
	name="Remekko"
	culture = dothraki
	used_for_random = no
	can_appear = no
}
600234 = {
	name="Dosko"
	culture = dothraki
	used_for_random = no
	can_appear = no
}
600235 = {
	name="Virgo"
	culture = dothraki
	used_for_random = no
	can_appear = no
}


#Jogos Nhai
607300 = {
	name="Fardeen"
	culture = jogos_nhai
	used_for_random = no
	can_appear = no
}
607301 = {
	name="Barialy"
	culture = jogos_nhai
	used_for_random = no
	can_appear = no
}
607302 = {
	name="Jawak"
	culture = jogos_nhai
	used_for_random = no
	can_appear = no
}
607303 = {
	name="Zaid"
	culture = jogos_nhai
	used_for_random = no
	can_appear = no
}
607304 = {
	name="Ehsan"
	culture = jogos_nhai
	used_for_random = no
	can_appear = no
}
607305 = {
	name="Zawak"
	culture = jogos_nhai
	used_for_random = no
	can_appear = no
}



