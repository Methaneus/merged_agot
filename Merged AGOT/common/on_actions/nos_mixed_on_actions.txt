# character
on_birth = {
	events = {
		valyrian_culture_mix.1
	}
}
on_decade_pulse = {
	events = {
		royconv.1
		royconv.2
		royconv.5
	}
	random_events = {
		500 = royconv.6
	}
}