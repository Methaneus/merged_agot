##################################################################################################
# scriptable and usable for event-system below.
##################################################################################################
#
# ICONS:
#
# Good 			<=> 		Bad
#
#  1 - Martial 				- 19
#  2 - Learning 			- 20
#  3 - Diplomacy 			- 21
#  4 - Stewardship 			- 22
#  5 - Intrigue 			- 23
#  6 - Money 				- 24
#  7 - Prestige 			- 25
#  8 - Piety 				- 26
#  9 - Titles 				- 27
# 10 - Council 				- 28 
# 11 - Laws 				- 29
# 12 - Tech 				- 30 
# 13 - Military 			- 31
# 14 - Plots 				- 32
# 15 - Messages 			- 33
# 16 - Diplomatic actions 	- 34
# 17 - Church 				- 35
# 18 - Characters 			- 36
# 37 - Prison 				- 38
# 39 - Love 				- 40
# 41 - Death 				- 42
# 43 - Indian religion 		- 44
# 45 - Dog					- 65
# 46 - Cat					- 66
# 47 - Owl					- 67
# 48 - Pagan religion       - 49
# 50 - Staff of Asclepius   - 51
# 52 - Mystic               - 53
# 54 - Bonesaw              - 55
# 56 - Horseshoe            - 57
# 58 - Parrot				- 68
# 59 - Ham					- 60
# 61 - Anchor				- 62
# 63 - Jewish religion		- 64
# 69 - Bed					- 70
# game\gfx\interface\modifier_icons.dds

###Sex Related###
tt_good_sex = {
	icon = 39
	health = 0.1 # You feel revitalized after some good sex.
	general_opinion = 5 # You've the after good sex glow, and you generally get along better with others since you're satisfied.
}
tt_rape_trauma = {
	icon = 40
	health = -0.5
	general_opinion = -5
	sex_appeal_opinion = -10
	diplomacy = -1
	intrigue = -5
	martial = -1
	combat_rating = -3
	monthly_character_prestige = -5
}
tt_still_wanting_more = { # Just didn't get the itch properly scratched
	icon = 40
	diplomacy = -1 # A little quick to anger from frustration
	intrigue = -1 # Frustration is hard to conceal not only itself, but other motives
	martial = 1 # Much more willing to lash out
	combat_rating = 1
	learning = -1 # Too distracted to properly focus intellectually
}


### End Sex related ###