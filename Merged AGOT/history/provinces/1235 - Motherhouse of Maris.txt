
# County Title
title = c_mbs_maris_motherhouse

# Settlements
max_settlements = 2
b_mbs_maris_motherhouse = temple
b_mbs_maris_motherhouse2 = city

# Misc
culture = old_first_man
religion = old_gods


# History

1.1.1 = {

	b_mbs_maris_motherhouse = tp_monastery_1
}
6700.1.1 = {
	culture = valeman
	religion = the_seven
}