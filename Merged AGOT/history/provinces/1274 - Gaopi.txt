# 1177  - Gaopi

# County Title
title = c_gaopi

# Settlements
max_settlements = 4

b_gaopi_castle = castle
b_gaopi_city1 = city
b_gaopi_temple = temple
b_gaopi_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_gaopi_castle = ca_asoiaf_yiti_basevalue_1
	b_gaopi_castle = ca_asoiaf_yiti_basevalue_2

	b_gaopi_city1 = ct_asoiaf_yiti_basevalue_1
	b_gaopi_city1 = ct_asoiaf_yiti_basevalue_2

}
	