# 1247 - Codd Hall

# County Title
title = c_codd_hall

# Settlements
max_settlements = 2

b_codd_hall = castle
#b_whitefish

# Misc
culture = old_ironborn
religion = drowned_god

#History
1.1.1 = {
	b_codd_hall = ca_asoiaf_ironislands_basevalue_1
	b_codd_hall = ca_asoiaf_ironislands_basevalue_2
}
6700.1.1 = { culture = ironborn }