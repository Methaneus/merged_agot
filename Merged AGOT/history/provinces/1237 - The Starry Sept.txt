

# County Title
title = c_mbs_starry_sept

# Settlements
max_settlements = 2
b_starry_sept = temple
b_mbs_oldtown_motherhouse = temple

# Misc
culture = old_first_man
religion = old_gods

# History
1.1.1 = {
	
	b_starry_sept = tp_monastery_1
	b_starry_sept = tp_monastery_2

	b_mbs_oldtown_motherhouse = tp_monastery_1
}
6700.1.1 = {
	culture = reachman
	religion = the_seven
}