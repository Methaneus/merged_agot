# 1199 - Skane

# County Title
title = c_skane

# Settlements
max_settlements = 2
b_skane = castle

# Misc
culture = skagosi
religion = beyond_wall_old_gods

# History
1.1.1 = { b_skane = ca_asoiaf_north_basevalue_1 }