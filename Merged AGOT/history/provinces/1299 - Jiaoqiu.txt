# 1202  - Jiaoqiu

# County Title
title = c_jiaoqiu

# Settlements
max_settlements = 4

b_jiaoqiu_castle = castle
b_jiaoqiu_city1 = city
b_jiaoqiu_temple = temple
b_jiaoqiu_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_jiaoqiu_castle = ca_asoiaf_yiti_basevalue_1
	b_jiaoqiu_castle = ca_asoiaf_yiti_basevalue_2

	b_jiaoqiu_city1 = ct_asoiaf_yiti_basevalue_1
	b_jiaoqiu_city1 = ct_asoiaf_yiti_basevalue_2

}
	