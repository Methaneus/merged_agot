

# County Title
title = c_mbs_highgarden_sept

# Settlements
max_settlements = 1
b_mbs_highgarden_sept = temple

# Misc
culture = old_first_man
religion = old_gods

# History
1.1.1 = {
	
	b_mbs_highgarden_sept = tp_monastery_1
	b_mbs_highgarden_sept = tp_monastery_2

}
6700.1.1 = {
	culture = reachman
	religion = the_seven
}