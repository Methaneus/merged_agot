# 1232  - Ningyang

# County Title
title = c_ningyang

# Settlements
max_settlements = 4

b_ningyang_castle = castle
b_ningyang_city1 = city
b_ningyang_temple = temple
b_ningyang_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_ningyang_castle = ca_asoiaf_yiti_basevalue_1
	b_ningyang_castle = ca_asoiaf_yiti_basevalue_2

	b_ningyang_city1 = ct_asoiaf_yiti_basevalue_1
	b_ningyang_city1 = ct_asoiaf_yiti_basevalue_2

}
	