# 23 - Pinesend

# County Title
title = c_pinesend

# Settlements
max_settlements = 3
b_firmont = castle
b_harclay = castle
#b_treesedge = castle
#b_highwoodhall = castle

# Misc
culture = hill_clansman
religion = old_gods

#History
1.1.1 = {
	b_firmont = ca_asoiaf_north_basevalue_1
	b_firmont = ca_asoiaf_north_basevalue_2
}
1.1.1 = {
	b_harclay = ca_asoiaf_north_basevalue_1
	b_harclay = ca_asoiaf_north_basevalue_2
}
