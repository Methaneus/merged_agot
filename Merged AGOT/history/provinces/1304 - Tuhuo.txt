# 1207  - Tuhuo

# County Title
title = c_tuhuo

# Settlements
max_settlements = 4

b_tuhuo_castle = castle
b_tuhuo_city1 = city
b_tuhuo_temple = temple
b_tuhuo_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_tuhuo_castle = ca_asoiaf_yiti_basevalue_1
	b_tuhuo_castle = ca_asoiaf_yiti_basevalue_2

	b_tuhuo_city1 = ct_asoiaf_yiti_basevalue_1
	b_tuhuo_city1 = ct_asoiaf_yiti_basevalue_2

}
	