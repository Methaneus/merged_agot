# 1211  - Tudian

# County Title
title = c_tudian

# Settlements
max_settlements = 4

b_tudian_castle = castle
b_tudian_city1 = city
b_tudian_temple = temple
b_tudian_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_tudian_castle = ca_asoiaf_yiti_basevalue_1
	b_tudian_castle = ca_asoiaf_yiti_basevalue_2

	b_tudian_city1 = ct_asoiaf_yiti_basevalue_1
	b_tudian_city1 = ct_asoiaf_yiti_basevalue_2

}
	