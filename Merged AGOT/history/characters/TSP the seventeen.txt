# TURTLESHROOM PRODUCTIONS PROUDLY PRESENTS #
# THE ILLEGITIMATE CHILDREN OF GREEN SWIGGITY #

# 770691  - The man, the myth, the legend, the father

# Year 8185 - The year he begins

#!-------------------------------------!#

# THE THREE ILLEGITIMATE CHILDREN OF GREEN AND ILHAN #

770054 = {
	name = "Green"
	mother = 770017
	father = 770691
	dynasty = "7706541"

	culture = islander
	religion = fish_gods
	
	add_trait = bastard

	8188.2.14 = {
		birth = "8188.2.14"
		add_trait = the_mark_of_the_deep_ones
		effect = {
			give_nickname = nick_fish
			set_graphical_culture = nghai
		}
	}

	8270.3.15 = {
		death = "8270.3.15"
	}
}

770027 = {
	name = "Glucka"
	mother = 770017
	father = 770691
	dynasty = "7706541"

	culture = islander
	religion = fish_gods
	
	add_trait = bastard

	female = yes

	8189.11.14 = {
		birth = "8189.11.14"
		add_trait = the_mark_of_the_deep_ones
		effect = {
			give_nickname = nick_fish
			set_graphical_culture = nghai
		}
	}

	8267.3.15 = {
		death = "8267.3.15"
	}
}

770028 = {
	name = "Omar"
	mother = "770017"
	father = "770691"
	dynasty = "7706541"

	culture = islander
	religion = fish_gods

	add_trait = sympathy_far_east_group
	add_trait = bastard

	8191.7.11 = {
		birth = "8191.7.11"
		add_trait = the_mark_of_the_deep_ones
		effect = {
			give_nickname = nick_fish
			set_graphical_culture = nghai
		}
	}

	8270.3.15 = {
		death = "8270.3.15"
	}
}

# - - - - - #

# THE THREE ILLEGITIMATE CHILDREN OF TLAIB AND GREEN #

770029 = {
	name = "Rashida"
	father = 770691
	mother = 770018
	dynasty = "7706541"

	culture = islander
	religion = fish_gods
	add_trait = bastard

	female = yes

	8187.10.5 = {
		birth = "8187.10.5"
		add_trait = the_mark_of_the_deep_ones
		effect = {
			give_nickname = nick_fish
			set_graphical_culture = nghai
		}
	}

	8260.6.6 = {
		death = "8260.6.6"
	}
}

770030 = {
	name = "Hussein"
	father = 770691
	mother = 770018
	dynasty = "7706541"

	culture = islander
	religion = fish_gods
	add_trait = bastard

	add_trait = fashionable
	add_trait = groomed

	8186.1.9 = {
		birth = "8186.1.9"
		add_trait = the_mark_of_the_deep_ones
		effect = { give_nickname = nick_fish }
	}

	8261.6.4 = {
		death = "8261.6.4"
	}
}

770053 = { # Accidentally listed as the same number as above, there is too much code to change back
	name = "Grog"
	father = 770691
	mother = 770018
	dynasty = "7706541"

	culture = islander
	religion = fish_gods
	add_trait = bastard

	8192.1.9 = {
		birth = "8192.1.9"
		add_trait = the_mark_of_the_deep_ones
		effect = {
			give_nickname = nick_fish
			set_graphical_culture = nghai
		}
	}

	8261.6.4 = {
		death = "8261.6.4"
	}
}

# - - - - - - #

# THE THREE ILLEGITIMATE CHILDREN OF OSCAIO AND GREEN #

770031 = {
	name = "Iskandar"
	father = 770691
	mother = 770020
	dynasty = "7706541"

	culture = islander
	religion = fish_gods

	add_trait = imbecile
	add_trait = fair
	add_trait = fashionable
	add_trait = groomed
	add_trait = indulgent_wastrel
	add_trait = bastard

	8197.1.1 = {
		birth = "8197.1.1"
		add_trait = the_mark_of_the_deep_ones
		effect = {
			give_nickname = nick_fish
			set_graphical_culture = nghai
		}
	}

	8300.1.2 = {
		death = "8300.1.2"
	}
}

770032 = {
	name = "Tek"
	father = 770691
	mother = 770020
	dynasty = "7706541"

	culture = islander
	religion = fish_gods

	female = yes

	add_trait = fair
	add_trait = wroth
	add_trait = poet
	add_trait = selfish
	add_trait = ambitious
	add_trait = aggressive_leader
	add_trait = schemer
	add_trait = twin # FRATERNAL twin
	add_trait = bastard

	disallow_random_traits = yes

	8198.3.30 = {
		birth = "8198.3.30"
		add_trait = the_mark_of_the_deep_ones
		effect = { give_nickname = nick_fish }
	}

	8218.6.27 = {
		effect = { add_rival = 770031 }
	}

	8299.8.19 = {
		death = "8299.8.19"
	}
}

770033 = {
	name = "Zed"
	father = 770691
	mother = 770020
	dynasty = "7706541"

	culture = islander
	religion = fish_gods

	add_trait = lunatic
	add_trait = dull
	add_trait = wroth
	add_trait = gluttonous
	add_trait = selfish
	add_trait = misguided_warrior
	add_trait = uncouth
	add_trait = trickster
	add_trait = paranoid
	add_trait = zealous
	add_trait = arbitrary
	add_trait = schemer
	add_trait = twin # FRATERNAL twin
	add_trait = bastard
	
	disallow_random_traits = yes

	8198.3.30 = {
		birth = "8198.3.30"
		add_trait = the_mark_of_the_deep_ones
		effect = { give_nickname = nick_fish }
	}

	8319.8.19 = {
		death = "8319.8.19"
	}
}

# - - - - - - #

# THE THREE ILLEGITIMATE CHILDREN OF GREEN AND SANGER #

770034 = {
	name = "Margaret"
	mother = 770021
	father = 770691
	dynasty = "7706541"

	religion = fish_gods
	culture = islander

	female = yes

	add_trait = ambitious
	add_trait = selfish
	add_trait = schemer
	add_trait = bastard

	intrigue = 8

	8201.1.1 = {
		birth = "8201.1.1"
		add_trait = bastard
		add_trait = the_mark_of_the_deep_ones
		effect = {
			give_nickname = nick_fish
			set_graphical_culture = nghai
		}
	}

	8300.11.30 = {
		death = "8300.11.30"
	}
}

770035 = {
	name = "Skoodge"
	mother = 770021
	father = 770691
	dynasty = "7706541"

	religion = fish_gods
	culture = islander

	add_trait = dwarf
	add_trait = ambitious
	add_trait = strategist
	add_trait = bastard

	8202.5.17 = {
		birth = "8202.5.17"
		add_trait = the_mark_of_the_deep_ones
		effect = { give_nickname = nick_fish }
	}

	8277.8.30 = {
		death = "8277.8.30"
	}
}

770036 = {
	name = "Vogg"
	mother = 770021
	father = 770691
	dynasty = "7706541"

	religion = fish_gods
	culture = islander
	
	add_trait = bastard

	8205.9.1 = {
		birth = "8205.9.1"
		add_trait = the_mark_of_the_deep_ones
		effect = { give_nickname = nick_fish }
	}

	8281.8.30 = {
		death = "8281.8.30"
	}
}

# - - - - - - #

# THE THREE ILLEGITIMATE CHILDREN OF GREEN AND KAMALA

770037 = {
	name = "Harris"
	mother = 770022
	father = 770691
	dynasty = "7706541"

	religion = fish_gods
	culture = islander

	add_trait = selfish
	add_trait = ruthless
	add_trait = schemer
	add_trait = bastard

	8190.2.3 = {
		birth = "8190.2.3"
		add_trait = the_mark_of_the_deep_ones
		effect = { give_nickname = nick_fish }
	}

	8256.1.1 = {
		death = "8256.1.1"
	}
}

770038 = {
	name = "Malik"
	mother = 770022
	father = 770691
	dynasty = "7706541"

	religion = fish_gods
	culture = islander

	add_trait = just
	add_trait = honest
	add_trait = sympathy_far_east_group
	add_trait = sympathy_summer_rel_group
	add_trait = gregarious
	add_trait = twin
	add_trait = dull
	add_trait = bastard

	8193.6.6 = {
		birth = "8193.6.6"
		add_trait = the_mark_of_the_deep_ones
		effect = { give_nickname = nick_fish }
	}

	8300.8.11 = {
		death = 8300.8.11
	}
}

770039 = {
	name = "Barakk"
	mother = 770022
	father = 770691
	dynasty = "7706541"

	religion = fish_gods
	culture = islander

	add_trait = socializer
	add_trait = genius
	add_trait = ruthless
	add_trait = arbitrary
	add_trait = wroth
	add_trait = ambitious
	add_trait = trusting
	add_trait = proud
	add_trait = deceitful
	add_trait = schemer
	add_trait = stutter
	add_trait = selfish
	add_trait = groomed
	add_trait = inspiring_leader
	add_trait = envious
	add_trait = fashionable
	add_trait = feeble
	add_trait = sympathy_summer_rel_group
	add_trait = twin
	add_trait = bastard

	health = 20

	disallow_random_traits = yes

	8193.6.6 = {
		birth = "8193.6.6"
		add_trait = the_mark_of_the_deep_ones
		effect = { give_nickname = nick_fish }
	}

	8209.6.7 = {
		add_trait = chaste
		add_trait = craven
		add_trait = grey_eminence
	}
	
	8226.8.4 = {
		add_spouse=770040
		effect = {
			add_rival = 770038
			add_friend = 770040
		}
	}
	
	8245.9.11 = {
		dynasty=7706542 # Vanilla rendition mechanics change an illegitimate child's dynasty when he has kids
		effect = {
			dynasty=7706542 # Vanilla rendition mechanics change an illegitimate child's dynasty when he has kids
			add_friend = 770700
		}
	}
	
	8245.12.30 = {
		add_trait = eunuch
	}

	8300.6.27 = {
		add_trait = widowed
		add_trait = depressed
	}

	8306.6.7 = {
		death = {
			death_reason = death_execution_hanging
		}
	}
}

770040 = { # Barakk's wife
	name = "Michelle" # Also known as "Big Mike"
	
	culture = summer_islander
	religion = old_ones

	add_trait = strong
	add_trait = tall
	add_trait = proud
	add_trait = quick
	add_trait = ugly
	add_trait = fashionable
	add_trait = groomed
	add_trait = one_eyed
	add_trait = ambitious
	add_trait = gregarious
	add_trait = socializer
	add_trait = schemer
	add_trait = cynical
	add_trait = inspiring_leader
	add_trait = arbitrary
	add_trait = authoritative
	add_trait = sympathy_summer_rel_group

	female = yes

	intrigue = 10
	diplomacy = 10
	martial = 7
	
	disallow_random_traits = yes

	health = 20

	8195.6.6 = {
		birth = "8195.6.6"
		add_trait = the_mark_of_the_deep_ones
		effect = {
			set_graphical_culture = summer_islander
		}
	}

	8200.6.27 = {
		effect = { give_nickname = nick_big_mike }
	}

	8226.8.4 = {
		add_trait = chaste
		add_spouse=770039
		effect = {
			add_friend = 770039
		}
	}
	
	8245.9.11 = {
		effect = {
			add_friend = 770700
		}
	}

	8317.6.27 = {
		death = {
			death_reason = death_execution_hanging
		}
	}
}

# - - - - - - - #

# THE THREE ILLEGITIMATE CHILDREN OF GREEN AND ALRED #

770041 = {
	name = "Gloria"
	father = 770691
	mother = 770023
	dynasty = "7706541"

	religion = fish_gods
	culture = islander

	add_trait = proud
	add_trait = deceitful
	add_trait = schemer
	add_trait = bastard

	female = yes

	8199.9.4 = {
		birth = "8199.9.4"
		add_trait = the_mark_of_the_deep_ones
		effect = {
			give_nickname = nick_fish
			set_graphical_culture = nghai
		}
	}

	8261.10.6 = {
		death = "8261.10.6"
	}
}

770042 = {
	name = "Will"
	father = 770691
	mother = 770023
	dynasty = "7706541"

	religion = fish_gods
	culture = islander

	add_trait = quick
	add_trait = administrator
	add_trait = gregarious
	add_trait = socializer
	add_trait = elusive_shadow
	add_trait = ambitious
	add_trait = twin # FRATERNAL twin
	add_trait = bastard

	disallow_random_traits = yes

	stewardship=10
	diplomacy=12

	8200.9.4 = {
		birth = "8200.9.4"
		add_trait = the_mark_of_the_deep_ones
		effect = { give_nickname = nick_fish }
	}

	8218.3.15 = {
		add_trait = hedonist
		add_trait = lustful
		add_trait = inspiring_leader
		add_trait = seducer
		effect = {
			add_ambition = obj_make_the_eight
			add_friend = 770043
		}
	}
	
	8222.6.6 = {
		add_trait = lovers_pox
	}

	8280.6.27 = {
		death = {
			death_reason = death_execution_snu_snu
		}
	}
}

770043 = {
	name = "Hillary"
	father = 770691
	mother = 770023
	dynasty = "7706541"

	add_trait = twin # FRATERNAL twins
	add_trait = ruthless
	add_trait = wroth
	add_trait = arbitrary
	add_trait = gregarious
	add_trait = genius
	add_trait = hunchback
	add_trait = schemer
	add_trait = ugly
	add_trait = elusive_shadow
	add_trait = socializer
	add_trait = bastard

	female = yes

	disallow_random_traits = yes

	culture = islander
	religion = fish_gods

	stewardship = 6
	intrigue = 10
	diplomacy = 7
	
	8200.9.4 = {
		birth = "8200.9.4"
		add_trait = the_mark_of_the_deep_ones
		effect = { give_nickname = nick_fish }
	}

	8218.3.15 = {
		add_trait = inspiring_leader
		effect = { add_friend = 770042 }
	}

	8227.9.11 = { # She gets legitimatized
		remove_trait = bastard
		add_trait = legit_bastard
		add_trait = envious
		effect = { remove_nickname = nick_fish }
	}

	8300.9.12 = {
		death = "8300.9.12"
	}
}

# - - - - - - #

# THE THREE ILLEGITIMATE CHILDREN OF GREEN AND BLASEY #

770044 = {
	name = "Ford"
	mother = 770024
	father = 770691
	dynasty = "7706541"

	culture = islander
	religion = fish_gods

	add_trait = cruel
	add_trait = ruthless
	add_trait = deceitful
	add_trait = bastard

	8226.9.4 = {
		birth = "8226.9.4"
		add_trait = the_mark_of_the_deep_ones
		effect = { give_nickname = nick_fish}
	}

	8314.9.4 = {
		death = {
			death_reason = death_execution_strangle
		}
	}
}

770045 = {
	name = "Kisteen"
	mother = 770024
	father = 770691
	dynasty = "7706541"
	
	culture = islander
	religion = fish_gods

	add_trait = cruel
	add_trait = ruthless
	add_trait = deceitful
	add_trait = bastard

	8228.7.16 = {
		birth = "8228.7.16"
		add_trait = the_mark_of_the_deep_ones
		effect = { give_nickname = nick_fish}
	}

	8240.1.1 = {
		add_trait = hedonist
		add_trait = lustful
	}

	8257.3.15 = {
		death = {
			death_reason = death_execution_snu_snu
		}
	}
}

770046 = {
	name = "Ugg"
	mother = 770024
	father = 770691
	dynasty = "7706541"
	
	culture = islander
	religion = fish_gods

	add_trait = bastard
	add_trait = sympathy_far_east_group

	8231.1.17 = {
		birth = "8231.1.17"
		add_trait = the_mark_of_the_deep_ones
		effect = {
			give_nickname = nick_fish
			set_graphical_culture = nghai
		}
	}

	8284.9.6 = {
		death = "8284.9.6"
	}
}

# - - - - - - #

# THE THREE ILLEGITIMATE CHILDREN OF GREEN AND DAVIES #

770047 = {
	name = "Marylynne"
	mother = 770025
	father = 770691
	dynasty = "7706541"

	culture = islander
	religion = fish_gods

	female = yes

	add_trait = cruel
	add_trait = ruthless
	add_trait = deceitful
	add_trait = schemer
	add_trait = ambitious
	add_trait = shrewd
	add_trait = wroth
	add_trait = bastard

	8237.1.1 = {
		birth = "8237.1.1"
		add_trait = bastard
		add_trait = the_mark_of_the_deep_ones
		effect = { give_nickname = nick_fish}
	}

	8245.6.6 = {
		add_trait = lustful
		add_trait = hedonist
		add_trait = flamboyant_schemer
	}

	8259.3.15 = {
		death = {
			death_reason = death_execution_strangle
		}
	}
}

770048 = {
	name = "Davis"
	mother = 770025
	father = 770691
	dynasty = "7706541"

	culture = islander
	religion = fish_gods

	add_trait = twin
	add_trait = bastard

	8240.6.21 = {
		birth = "8240.6.21"
		add_trait = the_mark_of_the_deep_ones
		effect = {
			give_nickname = nick_fish
			set_graphical_culture = nghai
		}
	}

	8300.1.30 = {
		death = "8300.1.30"
	}
}

770049 = {
	name = "Grok"
	mother = 770025
	father = 770691
	dynasty = "7706541"

	culture = islander
	religion = fish_gods

	add_trait = twin
	add_trait = bastard

	8240.6.21 = {
		birth = "8240.6.21"
		add_trait = the_mark_of_the_deep_ones
		effect = {
			give_nickname = nick_fish
			set_graphical_culture = nghai
		}
	}

	8330.9.4 = {
		death = "8330.9.4"
	}
}

# - - - - - - - - #

# THE THREE ILLEGITIMATE CHILDREN OF HARLEY AND GREEN #

770050 = {
	name = "Hannah"
	mother = 770026
	father = 770691
	dynasty = "7706541"

	culture = islander
	religion = fish_gods

	add_trait = cruel
	add_trait = ruthless
	add_trait = twin
	add_trait = bastard

	8241.9.9 = {
		birth = "8241.9.9"
		add_trait = the_mark_of_the_deep_ones
		effect = { give_nickname = nick_fish}
	}

	8251.6.6 = {
		add_trait = hedonist
		add_trait = lustful
	}

	8262.2.1 = {
		death = "8262.2.1"
	}
}

770051 = {
	name = "Corfmana"
	mother = 770026
	father = 770691
	dynasty = "7706541"

	culture = islander
	religion = fish_gods

	add_trait = cruel
	add_trait = ruthless
	add_trait = twin
	add_trait = bastard

	8241.9.9 = {
		birth = "8241.9.9"
		add_trait = the_mark_of_the_deep_ones
		effect = { give_nickname = nick_fish}
	}

	8260.1.1 = {
		add_trait = hedonist
		add_trait = gluttonous
		add_trait = is_fat
	}

	8313.3.15 = {
		death = "8313.3.15"
	}
}

770052 = {
	name = "Leigh"
	mother = 770026
	father = 770691
	dynasty = "7706541"

	culture = islander
	religion = fish_gods

	add_trait = cruel
	add_trait = ruthless
	add_trait = deceitful
	add_trait = bastard

	8247.3.15 = {
		birth = "8247.3.15"
		add_trait = the_mark_of_the_deep_ones
		effect = { give_nickname = nick_fish}
	}

	8260.1.1 = {
		death = "8260.1.1"
	}
}

### 770053 IS ALREADY TAKEN BY GROG, THE SON OF TLAIB, BECAUSE GROGG ACCIDENTLY GOT A DUPLICATE ID AND I CANNOT INCREMENT THAT MANY CHARACTERS WITHOUT LOSING MY PLACE ###

### THE SAME APPLIES TO 770054, FOR GREEN, THE SON OF ILHAN, FOR THE SAME REASON ###