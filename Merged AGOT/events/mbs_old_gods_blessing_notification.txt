namespace = mbs_old_gods_blessing_notification

#mbs_old_gods_blessing_notification.1 - Mormont Snowbear
narrative_event = {
	id = mbs_old_gods_blessing_notification.1
	title = "EVTtitlembs_old_gods_blessing_notification.1"
	desc = "EVTDESCmbs_old_gods_blessing_notification.1"
	picture = "GFX_snowbear"
	
	is_triggered_only = yes
	
	option = {
		name = "EVTOPTAmbs_old_gods_blessing_notification.1" #
		
		ai_chance = {
			factor = 2
		}		
	}
}
#mbs_old_gods_blessing_notification.2 - Mormont Brownbear
narrative_event = {
	id = mbs_old_gods_blessing_notification.2
	title = "EVTtitlembs_old_gods_blessing_notification.2"
	desc = "EVTDESCmbs_old_gods_blessing_notification.2"
	picture = "GFX_brownbear"
	
	is_triggered_only = yes
	
	option = {
		name = "EVTOPTAmbs_old_gods_blessing_notification.2" #
		
		ai_chance = {
			factor = 2
		}		
	}
}
#mbs_old_gods_blessing_notification.3 - Blackwood Raven
narrative_event = {
	id = mbs_old_gods_blessing_notification.3
	title = "EVTtitlembs_old_gods_blessing_notification.3"
	desc = "EVTDESCmbs_old_gods_blessing_notification.3"
	picture = "GFX_crow"
	
	is_triggered_only = yes
	
	option = {
		name = "EVTOPTAmbs_old_gods_blessing_notification.3" #
		
		ai_chance = {
			factor = 2
		}		
	}
}
#mbs_old_gods_blessing_notification.4 - Stark Wolf
narrative_event = {
	id = mbs_old_gods_blessing_notification.4
	title = "EVTtitlembs_old_gods_blessing_notification.4"
	desc = "EVTDESCmbs_old_gods_blessing_notification.4"
	picture = "GFX_wolf"
	
	is_triggered_only = yes
	
	option = {
		name = "EVTOPTAmbs_old_gods_blessing_notification.4" #
		
		ai_chance = {
			factor = 2
		}		
	}
}
#mbs_old_gods_blessing_notification.5 - Marsh King Lizard-lion
narrative_event = {
	id = mbs_old_gods_blessing_notification.5
	title = "EVTtitlembs_old_gods_blessing_notification.5"
	desc = "EVTDESCmbs_old_gods_blessing_notification.5"
	picture = "GFX_evt_hunting_scene"
	
	is_triggered_only = yes
	
	option = {
		name = "EVTOPTAmbs_old_gods_blessing_notification.5" #
		
		ai_chance = {
			factor = 2
		}		
	}
}
#mbs_old_gods_blessing_notification.6 - Banefort lion
narrative_event = {
	id = mbs_old_gods_blessing_notification.6
	title = "EVTtitlembs_old_gods_blessing_notification.6"
	desc = "EVTDESCmbs_old_gods_blessing_notification.6"
	picture = "GFX_evt_hunting_scene"
	
	is_triggered_only = yes
	
	option = {
		name = "EVTOPTAmbs_old_gods_blessing_notification.6" #
		
		ai_chance = {
			factor = 2
		}		
	}
}