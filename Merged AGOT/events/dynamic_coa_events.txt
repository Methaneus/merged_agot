namespace = dynamic_coa

#on game load/start, update CoAs
character_event = {
	id = dynamic_coa.0
	
	is_triggered_only = yes
	only_rulers = yes
	hide_window = yes
	
	trigger = { 
		NOT = { has_game_rule = { name = dynamic_coa value = off } }
		OR = {
			has_landed_title = e_rebels
			has_character_flag = alt_start_coa_check
		}	
	}
	
	immediate = {
		if = {
			limit = { NOT = { has_global_flag = dynamic_coa_initialised } }
			any_character = {
				limit = {
					OR = {
						dynasty = 496 #Targaryen
						dynasty = 497 #Targaryen (Aegon VI)
						dynasty = 498 #Baratheon (Old)
						dynasty = 57 #Blackfyre
						dynasty = 7000 #Bittersteel
						dynasty = 501 #Truefyre
						dynasty = 502 #Hammer
						dynasty = 503 #White
						dynasty = 52 #Velaryon
						dynasty = 364 #Darklyn
						dynasty = 38 #Rosby
						dynasty = 35 #Stokeworth
						dynasty = 32 #Hayford
						dynasty = 30 #Thorne
						dynasty = 1440 #Blackwater 
						dynasty = 36 #Rykker
						dynasty = 50 #Celtigar
						dynasty = 41 #Staunton
						dynasty = 174411 #Cave
						dynasty = 42 #Brune
						dynasty = 1197 #Crabb
						dynasty = 174410 #Boggs
						dynasty = 51 #Sunglass
						dynasty = 45 #Bywater
						dynasty = 46 #Massey
						dynasty = 21 #Follard
						dynasty = 22 #Blount
						dynasty = 23 #Gaunt
						dynasty = 24 #Farring
						dynasty = 25 #Chelsted
						dynasty = 26 #Mallery
						dynasty = 27 #Cressey
						dynasty = 28 #Chyttering
						dynasty = 29 #Rollingford
						dynasty = 33 #Harte
						dynasty = 34 #Buckwell
						dynasty = 37 #Hollard
						dynasty = 39 #Byrch
						dynasty = 40 #Edgerton
						dynasty = 48 #Langward
						dynasty = 55 #Pyle
						dynasty = 352 #Kettleblack
						dynasty = 353 #Slynt
						dynasty = 43 #Manning
						dynasty = 1523 #Suggs
						dynasty = 1521 #Cargyll
						dynasty = 49 #Bar Emmon
						dynasty = 47 #Hardy
						dynasty = 1524 #Tall (Tallad)
						dynasty = 1509 #Tall (Duncan)
						dynasty = 1520 #Deem
						dynasty = 1522 #Blackberry
						dynasty = 1442 #Tallyhill
						dynasty = 1543 #Winters
						dynasty = 1525 #Straw
						dynasty = 1526 #Tanner
						dynasty = 1527 #Foxglove
						dynasty = 1528 #Darke
						dynasty = 1529 #Darkwood
						dynasty = 1530 #Dargood
						dynasty = 1531 #Quince
						dynasty = 1532 #Largent
						dynasty = 1533 #Correy
						dynasty = 1534 #Goode
						dynasty = 1535 #Belgrave
						dynasty = 1514 #Bush
						dynasty = 1513 #Bean
						dynasty = 1512 #Blackhull
						dynasty = 504 #Farseed
						dynasty = 1472 #Bourney
						dynasty = 1473 #Corne
						dynasty = 1873 #Branfield
						dynasty = 1479 #Scales
						dynasty = 44 #Wendwater
						dynasty = 174412 #Pyne
						dynasty = 1875 #Longwaters
						
						dynasty = 59 #Stark
						dynasty = 1559 #Stark-Targaryen
						dynasty = 63859 #Greystark
						dynasty = 89 #Karstark
						dynasty = 87 #Bolton
						dynasty = 93 #Umber
						dynasty = 80 #Manderly
						dynasty = 62 #Glover
						dynasty = 68 #Ryswell
						dynasty = 79 #Dustin
						dynasty = 84 #Flint
						dynasty = 126 #Reed
						dynasty = 102 #Mormont
						dynasty = 1443 #Forrester
						dynasty = 70 #Whitehill
						dynasty = 83 #Hornwood
						dynasty = 101 #Tallhart
						dynasty = 60 #Cassel
						dynasty = 61 #Cerwyn
						dynasty = 88 #Locke
						dynasty = 104 #Magnar 
						dynasty = 105 #Crowl
						dynasty = 106 #Stane
						dynasty = 174831 #Warrick
						
						dynasty = 800867 #Silverfield
						dynasty = 65 #Fisher
						dynasty = 69 #Stout
						dynasty = 71 #Slate
						dynasty = 76 #Hayes
						dynasty = 81 #Wells
						dynasty = 82 #Woolfield
						dynasty = 85 #Overton
						dynasty = 86 #Mollen
						dynasty = 90 #Waterman
						dynasty = 91 #Lightfoot
						dynasty = 94 #Lake
						dynasty = 95 #Poole
						dynasty = 350 #Ironsmith
						dynasty = 351 #Condon
						dynasty = 1444 #Woods 
						dynasty = 1445 #Branch 
						dynasty = 1446 #Bole
						dynasty = 800833 #Ryder
						dynasty = 800839 #Ashwood
						dynasty = 800840 #Holt
						dynasty = 800841 #Long
						dynasty = 1872 #Tuttle
						dynasty = 1874 #Glenmore
						dynasty = 1876 #Brownbarrow
						dynasty = 1877 #Elliver
						dynasty = 1878 #Grayson
						dynasty = 800838 #Degore
						dynasty = 800834 #Greenwood
						dynasty = 800835 #Towers
						dynasty = 800836 #Amber
						dynasty = 800837 #Frost
						dynasty = 882175 #Woodfoot
						dynasty = 800842 #Goodmen
						dynasty = 1880 #Dormund
						dynasty = 1452 #Boggs
						dynasty = 72 #Moss
						dynasty = 73 #Fenn
						dynasty = 74 #Blackmyre
						dynasty = 78 #Marsh
						dynasty = 1448 #Cray 
						dynasty = 1449 #Greengood
						dynasty = 1450 #Peat
						dynasty = 1451 #Quagg
						dynasty = 92 #Burley
						dynasty = 96 #Harclay
						dynasty = 97 #Liddle
						dynasty = 98 #Knott
						dynasty = 99 #Wull
						dynasty = 100 #Norrey	
						dynasty = 1174345 #Redbeard
						dynasty = 1174344 #Thenn					
						
						dynasty = 159 #Tully 
						dynasty = 127 #Frey
						dynasty = 150 #Blackwood
						dynasty = 149 #Bracken
						dynasty = 128 #Mallister
						dynasty = 153 #Vance of Wayfarer's Rest
						dynasty = 154 #Vance of Atranta
						dynasty = 157 #Piper
						dynasty = 137 #Darry
						dynasty = 138 #Mooton
						dynasty = 152 #Ryger
						dynasty = 125 #Charlton
						dynasty = 129 #Erenford
						dynasty = 132 #Haigh
						dynasty = 133 #Grell
						dynasty = 134 #Paege
						dynasty = 139 #Hawick
						dynasty = 145 #Roote
						dynasty = 147 #Lychester
						dynasty = 151 #Shawney
						dynasty = 148 #Goodbrook
						dynasty = 135 #Lolliston
						dynasty = 2160 #Harlton (Telltale AGOT)
						dynasty = 155 #Blanetree
						dynasty = 156 #Smallwood
						dynasty = 141 #Terrick
						dynasty = 140 #Wayn
						dynasty = 142 #Keath
						dynasty = 136 #Wode
						dynasty = 131 #Vypren
						dynasty = 174435 #Teague
						dynasty = 174430 #Fisher
						dynasty = 174432 #Justman
						dynasty = 174399 #Mudd
						dynasty = 355 #Lothston
						dynasty = 146 #Whent
						dynasty = 356 #Strong
						dynasty = 357 #Harroway
						dynasty = 358 #Towers
						dynasty = 143 #Butterwell
						dynasty = 359 #Qoherys						
						dynasty = 178450 #Heddle
						dynasty = 178452 #Bigglestone
						dynasty = 178453 #Chambers
						dynasty = 178454 #Grey
						dynasty = 178455 #Hook
						dynasty = 178456 #Nutt
						dynasty = 178457 #Perryn
						dynasty = 174846 #Pemford
						dynasty = 211 #Deddings
						#dynasty = 1438 #Mandrake
						#dynasty = 1439 #Duckfield
						#dynasty = 1441 #Pennytree
						dynasty = 69113 #Rushmoor
						dynasty = 39114 #Kanet
						#dynasty = 174369 #Woodsman
						#dynasty = 174415 #Rankenfell
						dynasty = 174429 #Cox
						dynasty = 174431 #Bromley
						dynasty = 174433 #Longbough
						dynasty = 174434 #Nayland
						dynasty = 174829 #Groves
						#dynasty = 174834 #Of the Crossing
						
						dynasty = 178 #Arryn  
						dynasty = 601 #Baelish 
						dynasty = 161 #Belmore
						dynasty = 174 #Lynderly
						dynasty = 187 #Royce
						dynasty = 177 #Hunter
						dynasty = 182 #Waynwood
						dynasty = 186 #Grafton
						dynasty = 176 #Corbray
						dynasty = 189 #Waxley
						dynasty = 1428 #Strickland
						dynasty = 184 #Hardyng
						dynasty = 183 #Redfort
						dynasty = 169 #Coldwater						
						dynasty = 162 #Tollett
						dynasty = 163 #Hersy
						dynasty = 167 #Pryor
						dynasty = 168 #Elesham
						dynasty = 170 #Upcliff
						dynasty = 171 #Baelish (Old)
						dynasty = 172 #Templeton
						dynasty = 173 #Donniger
						dynasty = 175 #Egen
						dynasty = 45072 #Arryn of Gulltown
						dynasty = 179 #Moore
						dynasty = 180 #Wydman
						dynasty = 181 #Melcolm
						dynasty = 185 #Shett
						dynasty = 188 #Ruthermont
						dynasty = 174357 #Long
						dynasty = 174359 #Hungerford
						dynasty = 174437 #Shady Glen
						dynasty = 174823 #Crayne
						dynasty = 174835 #Dutton
						dynasty = 174836 #Woodhull
						dynasty = 174839 #of the Vale
						dynasty = 174851 #Underleaf
						dynasty = 801047 #Breakstone
						dynasty = 881800 #Shell
						dynasty = 881801 #Brightstone
						dynasty = 801048 #Lipps	
						dynasty = 363 #Sunderland
						dynasty = 164 #Longthorpe
						dynasty = 165 #Borrell
						dynasty = 166 #Torrent
						dynasty = 10001 #Saul
						dynasty = 10000 #Badics
						
						dynasty = 190 #Lannister 
						dynasty = 1575 #Baratheon-Lannister 
						dynasty = 197 #Reyne
						dynasty = 196 #Prester
						dynasty = 227 #Crakehall
						dynasty = 228 #Swyft
						dynasty = 221 #Serrett
						dynasty = 208 #Payne
						dynasty = 216 #Brax
						dynasty = 233 #Sarsfield
						dynasty = 199 #Westerling
						dynasty = 207 #Lorch
						dynasty = 213 #Lefford
						dynasty = 230 #Clegane
						dynasty = 217 #Lydden
						dynasty = 225 #Tarbeck
						dynasty = 200 #Banefort
						dynasty = 212 #Marbrand
						dynasty = 203 #Vikary						
						dynasty = 192 #Spicer
						dynasty = 193 #Hamell
						dynasty = 194 #Farman
						dynasty = 195 #Kenning
						dynasty = 198 #Drox
						dynasty = 201 #Estren
						dynasty = 202 #Stackspear
						dynasty = 204 #Foote
						dynasty = 205 #Yarwyck
						dynasty = 206 #Bettley
						dynasty = 209 #Moreland
						dynasty = 210 #Ruttiger
						dynasty = 214 #Garner
						dynasty = 215 #Myatt
						dynasty = 218 #Ferren
						dynasty = 219 #Peckledon
						dynasty = 220 #Hawthorne
						dynasty = 222 #Yew
						dynasty = 223 #Jast
						dynasty = 224 #Algood
						dynasty = 226 #Plumm
						dynasty = 229 #Falwell
						dynasty = 231 #Turnberry
						dynasty = 232 #Greenfield
						dynasty = 2158 #Westford
						dynasty = 2159 #Sarwyck
						dynasty = 2161 #Parren
						dynasty = 174358 #Lanster
						dynasty = 174362 #Doggett
						dynasty = 174406 #Casterly
						dynasty = 174409 #Clifton
						dynasty = 174414 #Hetherspoon
						dynasty = 802043 #Lanny
						dynasty = 802044 #Lannett
						dynasty = 802045 #Lantell
						dynasty = 174830 #Ruskyn
						dynasty = 174832 #Whitfield
						dynasty = 802042 #Kyndall
						dynasty = 174842 #Stilwood	
						dynasty = 45000156 #Graymarch						
						
						dynasty = 253 #Tyrell
						dynasty = 1254 #Gardener
						dynasty = 296 #Tarly
						dynasty = 285 #Hightower
						dynasty = 281 #Florent
						dynasty = 235 #Osgrey
						dynasty = 250 #Rowan
						dynasty = 298 #Peake
						dynasty = 270 #Meadows
						dynasty = 288 #Redwyne
						dynasty = 261 #Red Apple Fossoway 
						dynasty = 262 #Green Apple Fossoway
						dynasty = 237 #Oakheart
						dynasty = 234 #Crane
						dynasty = 248 #Footly
						dynasty = 297 #Ambrose
						dynasty = 263 #Merryweather
						dynasty = 282 #Beesbury
						dynasty = 264 #Caswell
						dynasty = 247 #Roxton
						dynasty = 258 #Varner
						dynasty = 265 #Ashford
						dynasty = 260 #Ball
						dynasty = 293 #Vyrwel
						dynasty = 238 #Webber
						dynasty = 252 #Kidwell
						dynasty = 246 #Shermer
						dynasty = 1333 #Lowther
						dynasty = 244 #Wythers
						dynasty = 245 #Bushy
						dynasty = 284 #Bulwer
						dynasty = 287 #Costayne
						dynasty = 289 #Cuy
						dynasty = 283 #Blackbar
						dynasty = 290 #Mullendore
						dynasty = 279 #Norcross
						dynasty = 254 #Dunn
						dynasty = 278 #Hewett
						dynasty = 277 #Chester
						dynasty = 276 #Serry
						dynasty = 275 #Grimm
						dynasty = 272 #Willum
						dynasty = 271 #Cockshaw
						dynasty = 240 #Westbrook
						dynasty = 267 #Hastwyck
						dynasty = 266 #Appleton
						dynasty = 259 #Sloane
						dynasty = 255 #Middlebury
						dynasty = 249 #Inchfield
						dynasty = 292 #Hunt
						dynasty = 295 #Oldflowers
						dynasty = 257 #Yelshire
						dynasty = 242 #Graceford
						dynasty = 256 #Orme
						dynasty = 294 #Cordwayner
						dynasty = 299 #Leygood
						dynasty = 268 #Bridges
						dynasty = 269 #Norridge
						dynasty = 251 #Lyberr
						dynasty = 291 #Hutcheson
						dynasty = 280 #Graves
						dynasty = 241 #Uffering
						dynasty = 274 #Pommingham
						dynasty = 243 #Redding
						dynasty = 1425 #Risley
						dynasty = 1423 #Rhysling
						dynasty = 239 #Woodwright
						dynasty = 236 #Durwell
						dynasty = 286 #Conklyn
						dynasty = 1201 #Cupps
						dynasty = 1427 #Goldwyne
						dynasty = 1471 #Stackhouse
						dynasty = 174425 #Farrow
						dynasty = 174426 #Penny
						dynasty = 174427 #Potter
						dynasty = 174428 #Sawyer
						dynasty = 174833 #Cobb
						dynasty = 1476 #Rodden
						dynasty = 1477 #Weatherwax
						dynasty = 1478 #Whitewater
						dynasty = 1489 #Summerchild	
						dynasty = 450001 #Knuckey
						dynasty = 45000170 #Pandred
						dynasty = 4500001 #Clohessy
						dynasty = 4500002 #Foxheart
						dynasty = 982383 #Loch
						dynasty = 206017 #Greenhill
						# dynasty = 1426 #Stonecrab					
						
						dynasty = 317 #Baratheon
						dynasty = 1544 #Durrandon
						dynasty = 316 #Connington
						dynasty = 330 #Estermont
						dynasty = 329 #Wylde
						dynasty = 306 #Swann
						dynasty = 307 #Toyne 
						dynasty = 319 #Buckler
						dynasty = 54 #Tarth
						dynasty = 320 #Penrose
						dynasty = 303 #Dondarrion
						dynasty = 301 #Caron
						dynasty = 9110 #Seaworth
						dynasty = 302 #Selmy
						dynasty = 304 #Trant
						dynasty = 321 #Morrigen
						dynasty = 327 #Mertyns
						dynasty = 322 #Rogers
						dynasty = 311 #Horpe
						dynasty = 1007 #Cole
						dynasty = 362 #Musgood
						dynasty = 361 #Swygert
						dynasty = 354 #Wagstaff
						dynasty = 333 #Wensington
						dynasty = 332 #Bolling
						dynasty = 331 #Rambton
						dynasty = 328 #Kellington
						dynasty = 326 #Gower
						dynasty = 325 #Tudbury
						dynasty = 324 #Herston
						dynasty = 323 #Staedmon
						dynasty = 318 #Errol
						dynasty = 313 #Fell
						dynasty = 312 #Hasty
						dynasty = 310 #Cafferen
						dynasty = 308 #Grandison
						dynasty = 305 #Lonmouth
						dynasty = 273 #Peasebury
						dynasty = 881808 #Brownhill
						dynasty = 805046 #Whitehead
						dynasty = 65100 #King's Mountain
						dynasty = 174436 #Greenpools

						dynasty = 1 #Martell
						dynasty = 16 #Dayne
						dynasty = 14 #Fowler
						dynasty = 12 #Yronwood
						dynasty = 13 #Uller
						dynasty = 10 #Jordayne 
						dynasty = 9 #Allyrion
						dynasty = 15 #Qorgyle
						dynasty = 18 #Blackmont
						dynasty = 20 #Manwoody
						dynasty = 4 #Toland (post-conquest)
						dynasty = 499 #Toland (pre-conquest)
						dynasty = 2 #Santagar
						dynasty = 6 #Dalt
						dynasty = 7 #Gargalen
						dynasty = 8 #Vaith
						dynasty = 19 #Wyl
						
						dynasty = 107 #Greyjoy
						dynasty = 66 #Hoare
						dynasty = 1508 #Greyiron
						dynasty = 1507 #Codd
						dynasty = 110 #Merlyn
						dynasty = 120 #Stonetree
						dynasty = 116 #Orkwood
						dynasty = 118 #Blacktyde
						dynasty = 1506 #Humble
						dynasty = 122 #Harlaw
						dynasty = 112 #Goodbrother
						dynasty = 121 #Drumm
						dynasty = 114 #Farwynd
						dynasty = 119 #Volmark
						dynasty = 109 #Saltcliffe
						dynasty = 64 #Botley
						dynasty = 67 #Wynch
						dynasty = 108 #Stonehouse
						dynasty = 111 #Myre
						dynasty = 113 #Sparr
						dynasty = 115 #Kenning
						dynasty = 117 #Sunderly
						dynasty = 123 #Tawney
						dynasty = 124 #Shepherd
						dynasty = 1503 #Netley
						dynasty = 1504 #Sharp
						dynasty = 1505 #Weaver
						dynasty = 1500 #Ironmaker
						dynasty = 1490 #Salt
						dynasty = 1501 #Stern
							
						dynasty = 982375 #Vunatis
						dynasty = 10002 #Back
						
							#Cadets
						dynasty = 450204  
						dynasty = 450205  
						dynasty = 450206  
						dynasty = 450207  
						dynasty = 450228  
						dynasty = 450229  
						dynasty = 450230  
						dynasty = 450231  
						dynasty = 450200  
						dynasty = 450201  
						dynasty = 450202  
						dynasty = 450203  
						dynasty = 450209  
						dynasty = 450210  
						dynasty = 450208  
						dynasty = 450214  
						dynasty = 450215  
						dynasty = 450216  
						dynasty = 450217  
						dynasty = 450218  
						dynasty = 450219  
						dynasty = 450236  
						dynasty = 450237  
						dynasty = 450238  
						dynasty = 450239  
						dynasty = 450220  
						dynasty = 450211  
						dynasty = 450212  
						dynasty = 450213  
						dynasty = 450224  
						dynasty = 450225  
						dynasty = 450226  
						dynasty = 450227  
						dynasty = 450232  
						dynasty = 450233  
						dynasty = 450234  
						dynasty = 450235  
						dynasty = 496029
						dynasty = 450221
						dynasty = 450222
						dynasty = 450223
						dynasty = 174812
						dynasty = 174347
						dynasty = 174348
						dynasty = 174344
						dynasty = 1062
						dynasty = 174345
						dynasty = 1771
						dynasty = 1772
						dynasty = 1773
						dynasty = 800839
						dynasty = 1874
						dynasty = 1071
						dynasty = 1072
						dynasty = 1073
						dynasty = 1074
						dynasty = 1075
						dynasty = 1076
						dynasty = 1077
						dynasty = 1078
						dynasty = 1079
						dynasty = 1080
						dynasty = 1081
						dynasty = 1082
						dynasty = 1083
						dynasty = 1084
						dynasty = 1085
						dynasty = 1086
						dynasty = 1087
						dynasty = 1088
						dynasty = 174800
						dynasty = 174801
						dynasty = 19993
						dynasty = 18872
						dynasty = 18873
						dynasty = 18874
						dynasty = 18875
						dynasty = 18876
						dynasty = 18877
						dynasty = 18878
						dynasty = 18879
						
						#CO Drace
						dynasty = 5700145
					}
				}
				set_dynasty_flag = has_dynamic_coa
			}
			set_global_flag = dynamic_coa_initialised
		}
		any_title = {
			limit = {
				NOT = { tier = BARON }
				has_holder = yes		
			}
			reset_coa = THIS
			clr_title_flag = has_dynamic_coa
		}
		any_playable_ruler = {
			limit = {
				OR = {
					higher_tier_than = COUNT
					has_game_rule = { name = dynamic_coa value = full }
				}
				has_dynasty_flag = has_dynamic_coa
			}
			character_event = { id = dynamic_coa.1 days = 1 }
		}
		clr_character_flag = alt_start_coa_check
	}
}	
character_event = { #also called from events when a ruler changes dynasty
	id = dynamic_coa.1
	
	is_triggered_only = yes
	only_rulers = yes
	hide_window = yes
	
	trigger = { 
		NOT = { tier = BARON }	
		OR = {
			has_game_rule = { name = dynamic_coa value = full }
			has_game_rule = { name = dynamic_coa value = default }
			AND = {
				has_game_rule = { name = dynamic_coa value = dynasty_head }
				dynasty_head = { character = ROOT }
			}
		}		
	}
	
	immediate = {
		if = {
			limit = {
				NAND = {
					OR = {
						higher_tier_than = COUNT
						has_game_rule = { name = dynamic_coa value = full }
					}
					has_dynasty_flag = has_dynamic_coa
					can_press_claims_trigger = yes
					is_republic = no
					is_theocracy = no
					NOT = { is_in_interregnum_trigger = yes }
				}	
			}
			any_demesne_title = {
				limit = {
					NOT = { tier = BARON }
					has_title_flag = has_dynamic_coa
				}
				reset_coa = THIS
				clr_title_flag = has_dynamic_coa
			}			
			break = yes
		}
		any_demesne_title = {
			limit = {
				NOT = { tier = BARON }
				has_title_flag = has_dynamic_coa
				NOR = {
					title = e_iron_throne
					title = e_new_valyria
					is_primary_holder_title = yes
					ROOT = {
						capital_scope = {
							OR = {
								county = { title = PREVPREVPREV }
								duchy = { title = PREVPREVPREV }
								kingdom = { title = PREVPREVPREV }
								empire = { title = PREVPREVPREV }
							}
						}
					}
				}
			}
			reset_coa = THIS
			clr_title_flag = has_dynamic_coa
		}
		any_demesne_title = {
			limit = {
				NOT = { tier = BARON }
				is_landless_type_title = no
				controls_religion = no
				OR = {
					title = e_iron_throne
					title = e_new_valyria
					is_primary_holder_title = yes
					ROOT = {
						capital_scope = {
							OR = {
								county = { title = PREVPREVPREV }
								duchy = { title = PREVPREVPREV }
								kingdom = { title = PREVPREVPREV }
								empire = { title = PREVPREVPREV }
							}
						}
					}
				}
			}
			set_title_flag = has_dynamic_coa
			set_dynamic_coa_effect = yes
		}
	}
	
	option = {
		name = OK
	}
}
#Check CoAs on new holder
character_event = {
	id = dynamic_coa.2
	
	is_triggered_only = yes
	hide_window = yes
	
	trigger = {	
		FROM = { NOT = { tier = BARON } }
		OR = {
			higher_tier_than = COUNT
			has_game_rule = { name = dynamic_coa value = full }
		}
		has_dynasty_flag = has_dynamic_coa
		OR = {
			has_game_rule = { name = dynamic_coa value = full }
			has_game_rule = { name = dynamic_coa value = default }
			AND = {
				has_game_rule = { name = dynamic_coa value = dynasty_head }
				dynasty_head = { character = ROOT }
			}
		}
		is_republic = no
		is_theocracy = no
		NOT = { is_in_interregnum_trigger = yes }
		NOR = {
			trait = kingsguard
			trait = nightswatch
			trait = dosh_khaleen
			trait = maester
			trait = archmaester
			trait = red_priest
			trait = grace
			trait = warlock
			trait = shadowbinder
			trait = bearded_priest
			trait = septa
			trait = septon
			trait = love_priest
		}
	}
	
	immediate = {	
		FROM = {
			reset_coa = THIS
			clr_title_flag = has_dynamic_coa
			if = {
				limit = {
					is_landless_type_title = no
					controls_religion = no
					OR = {
						title = e_iron_throne
						title = e_new_valyria
						is_primary_holder_title = yes
						ROOT = {
							capital_scope = {
								OR = {
									county = { title = ROOT_FROM }
									duchy = { title = ROOT_FROM }
									kingdom = { title = ROOT_FROM }
									empire = { title = ROOT_FROM }
								}
							}
						}
					}
				}
				set_title_flag = has_dynamic_coa
				set_dynamic_coa_effect = yes		
			}	
		}
	}
	
	option = {
		name = OK
	}
}
character_event = {
	id = dynamic_coa.3
	
	is_triggered_only = yes
	hide_window = yes
	
	trigger = { 
		NOT = { has_game_rule = { name = dynamic_coa value = off } }
		FROM = { 
			NOT = { tier = BARON } 
			has_title_flag = has_dynamic_coa
		}
		OR = {
			NOT = { has_dynasty_flag = has_dynamic_coa }
			is_republic = yes
			is_theocracy = yes
			is_in_interregnum_trigger = yes
		}	
	}
	
	immediate = {
		FROM = {
			reset_coa = THIS
			clr_title_flag = has_dynamic_coa
		}
	}
	
	option = {
		name = OK
	}
}
